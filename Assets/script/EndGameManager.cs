using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameManager : MonoBehaviour
{
    private static EndGameManager instance;
    public static EndGameManager Instance
    { 

        get {
            instance = FindObjectOfType<EndGameManager>();
            if (instance == null)
            {
                GameObject obj = new GameObject("EndGameManager");
                instance = obj.AddComponent<EndGameManager>();
            }
            return instance;
        }
    }
    public bool Doortouchfirst,finalwarrning;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time.Instance.EndGame == true)
        {
            if (Doortouchfirst)
            {
                finalwarrning = true;
            }
        }
    }
}
