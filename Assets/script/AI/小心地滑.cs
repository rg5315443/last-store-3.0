 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using StarterAssets;
using UnityEngine.UIElements;

public class 小心地滑 : MonoBehaviour
{
    //public int walkableArea;
    public GameObject 小心地滑實物,小心地滑鏡頭,playercam;
    public Vector3 raycastboxSize;
    public LayerMask whatisPlayer,ground,vision, wall;
    public GameObject playerc,playerdect,camROOT;
    public bool playerintoxic;
    public float max,min,hitrange;
    public float ToxicTime,Debufftime,spawntime;
    public bool playerLimitemin,playerLimitemax,playervision;
    public bool isPlace=false;
    //bool istox=false;
    public Vector3 P4setpoint;
    Coroutine coroutine;
    public GameObject lightoffgam;
    Lightoff[] lightoff;
    NavMeshHit hit;
    RaycastHit rayhit;
    CharacterController Playercha;
    public FirstPersonController playerfps;
    public stamina stamina;
    public bool musicego;
    public P4Manager P4Manager;
    public int P4Touched;
    public bool P4TouchedGranted;
    // Start is called before the first frame update
    void Start()
    {
         lightoff = lightoffgam.GetComponentsInChildren<Lightoff>();
         Playercha = playerc.GetComponent<CharacterController>();
        小心地滑鏡頭.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (P4Touched >= 5 && !P4TouchedGranted)
        {
            FinalScorecaculator.CaculatingScore += 1000;
            P4TouchedGranted = true;
        }
        if (Physics.BoxCast(transform.position, raycastboxSize, -Vector3.up, Quaternion.identity, 5, wall))
        {
            Debug.Log("LDDLDL");
        }
        playerLimitemax = Physics.CheckSphere(P4setpoint, max, whatisPlayer);
        playerLimitemin = Physics.CheckSphere(P4setpoint, min, whatisPlayer);
        if (isPlace == true)
        {
            spawntime += Time.deltaTime;
            if (spawntime >= 25&& !playerLimitemin)
            { 
                isPlace = false;
            }
        }
        if(isPlace==false)
        {
            spawntime = 0;
            float randomZ = Random.Range(-max, max);
            float randomX = Random.Range(-max, max);
            P4setpoint=new Vector3(playerdect.transform.position.x+randomX,transform.position.y, playerdect.transform.position.z+randomZ);

            playervision = Physics.CheckSphere(P4setpoint, 1, vision);
            /* if(!playerLimitemin&&playerLimitemax&&!playervision)
             {
              if(NavMesh.SamplePosition(P4setpoint,out hit,hitrange,NavMesh.AllAreas))
                 {
                     Debug.Log("sace");
                     小心地滑實物.transform.position=P4setpoint;
                     isPlace=true;
                 }
             }*/
            Debug.DrawRay(new Vector3(P4setpoint.x, P4setpoint.y  , P4setpoint.z), Vector3.down* hitrange, Color.blue);
            if (Physics.Raycast(new Vector3(P4setpoint.x, P4setpoint.y, P4setpoint.z), Vector3.down,out rayhit, hitrange, ground))
            {
                if (NavMesh.SamplePosition(new Vector3(P4setpoint.x, P4setpoint.y-0.01f, P4setpoint.z), out hit, 0.5f, NavMesh.AllAreas) && !playerLimitemin && !playervision)
                {
                    小心地滑實物.transform.position = P4setpoint;
                    isPlace = true;

                }
                else isPlace = false;
            }
            else isPlace = false;

        }
        /*if(Input.GetKeyDown(KeyCode.C))
        {
            小心地滑實物.transform.position=new Vector3(playerdect.transform.position.x,playerdect.transform.position.y,playerdect.transform.position.z-5);
        }*/
        /*if (musicego)
        {
            Debufftime += Time.deltaTime;
        }
        if (Debufftime >= ToxicTime)
        {
            musicego = false;
            stamina.P4Hit = false;
            foreach (Lightoff item in lightoff)
            {
                item.toxic = false;
                item.entry = false;
                item.over = true;
                item.backtime = true;
            }
            Debufftime = 0;
        }*/
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag==("Player"))
        {
            // Debug.Log("place");
            /*if (musicego)
            {
                StopCoroutine(coroutine);
                Debug.Log("stoped");
            }*/
            /*  if (coroutine != null)
              {
                  StopCoroutine(coroutine);
              }

              coroutine = StartCoroutine(ddd());*/
            //StartCoroutine(ddd());
            //istox=true;
            Debuff();
            P4Touched++;
            P4Manager.Debuffing = true;
            P4Manager.Debufftime = 0;
            StartCoroutine(小心地滑倒數());
            isPlace=false;

           
            
        }
    }

    void Debuff()
    {
        /*Debufftime = 0;
        Debufftime += Time.deltaTime;*/
        foreach (Lightoff item in lightoff)
        {
            item.toxic = true;
        }
        stamina.P4Hit = true;
        /*if (musicego == false)
            musicego = true;*/
       
    }
    IEnumerator ddd()
    {
        #region Abandon
        /*for(int i=0;i<Lightoff.LongLength;i++)
            {
             Lightoff[i].toxic=true;
             //Debug.Log(i);
            }
             stamina.P4Hit=true;
        yield return new WaitForSeconds(ToxicTime); 
        stamina.P4Hit=false;
        Debug.Log("Changed");
        
        for(int i=0;i<Lightoff.LongLength;i++)
        {
        Lightoff[i].toxic=false;
        Lightoff[i].entry=false;
        //Lightoff[i].over=true;

        Lightoff[i].backtime=true;
        //Lightoff[i].over=false;
        
        }*/
        #endregion

        foreach (Lightoff item in lightoff)
        {
            item.toxic=true;
        }
        stamina.P4Hit=true;
        if(musicego==false)
        musicego=true;
        yield return new WaitForSeconds(ToxicTime); 
        musicego=false;
        stamina.P4Hit=false;
        foreach (Lightoff item in lightoff)
        {
            item.toxic=false;
            item.entry=false;
            item.over=true;
            item.backtime=true;
        }        
    }
    IEnumerator 小心地滑倒數()
    {
        playercam.SetActive(false);
        Playercha.enabled=false;
        小心地滑鏡頭.SetActive(true);
        playerfps.enabled=(false);
       
        yield return new WaitForSeconds(1.6f);
        playerfps.enabled=(true);
        playercam.SetActive(true);
         Playercha.enabled=true;
         
        小心地滑鏡頭.SetActive(false);
        
        
    }
}
