using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class P3watch : MonoBehaviour
{
    public p3ai p3;
    public Transform player, P3;
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public Animator P3animator;
    public Camera pcamera;
    bool bewatch;
    RaycastHit hit;
    public LayerMask layer;
    public bool detected;
    // Start is called before the first frame update
    void Start()
    {

    }


    void Update()
    {
        Vector3 dir = P3.transform.position - player.transform.position;
        float distance = Vector3.Distance(P3.position, player.position);
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(pcamera);

       
        if (GeometryUtility.TestPlanesAABB(planes, skinnedMeshRenderer.bounds))
        {
            if (!Physics.Raycast(player.position, dir, out hit, distance, layer) && NewBehaviourScript.Instance.hiding == false)
            {
                detected = true;
                if (bewatch == false)
                {
                    p3.watched = true;
                    int Num = UnityEngine.Random.Range(0, 2);
                    P3animator.SetInteger("Idelytpe", Num);
                    P3animator.SetBool("bewatch", true);
                    bewatch = true;
                }

            }else detected = false;


        }
        if (!GeometryUtility.TestPlanesAABB(planes, skinnedMeshRenderer.bounds)|| detected == false|| NewBehaviourScript.Instance.hiding == true)
        {
            if (bewatch == true)
            {
                p3.onwalkspot = false;
                P3animator.SetBool("bewatch", false);
                p3.watched = false;
                bewatch = false;
            }

        }
        
        
        /*private void OnBecameInvisible()
        {
            p3.watched = false;
            P3animator.SetBool("bewatch", false);
        }
        private void OnBecameVisible()
        {
            p3.watched = true;
            int Num = Random.Range(0, 2);
            P3animator.SetInteger("Idelytpe", Num);
            P3animator.SetBool("bewatch", true);
        }*/
    }
}