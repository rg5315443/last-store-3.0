using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P3STEP : MonoBehaviour
{

    public AudioClip[] walkclip, runclip;
    public AudioClip roar;
    public AudioSource Audio, RunAudio;

    // Start is called before the first frame update
    void Start()
    {
        //Audio=this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void Step()
    {

        Audio.PlayOneShot(walkclip[Random.Range(0, walkclip.Length)]);
    }
    private void Run()
    {

        RunAudio.PlayOneShot(runclip[0]);
    }
    private void Roar()
    {

        Audio.PlayOneShot(roar);
    }

}
