using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

public class P3apostle : MonoBehaviour
{
    public Transform Player;
    public float targetangle;
    Vector3 dir;
     float speed;
     public float time=0;
     public bool candoit;
      public p3ai p3Ai;
      public AudioClip[] clips;
      AudioSource audioSource;
      bool playing=false;
      public P3使徒總控 P3script;
    // Start is called before the first frame update
    void Start()
    {
        audioSource=GetComponent<AudioSource>();
        P3script=GetComponentInParent<P3使徒總控>();
        try { p3Ai = GameObject.FindGameObjectWithTag("P3").GetComponent<p3ai>(); } catch { };
        StartCoroutine(StartGetPlayer());
    }

    // Update is called once per frame
    void Update()
    {

        if (Player != null)
        {
            dir = Player.transform.position - transform.position;
            targetangle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
        }else
        { try
            {
                Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            }
            catch { }         
        }



        if (candoit==true)
    {           
           if(time<0.5f)
        {
                time += Time.deltaTime * 1;
                transform.rotation=Quaternion.Slerp(transform.rotation,Quaternion.Euler(0,targetangle,0),time*0.025f);
        }
    }
        if (time >= 0.5f && time < 1f)
        {
            time += Time.deltaTime * 1;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, targetangle, 0),1);
            if (playing == false)
            {
                audioSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
                playing = true;
            }


        }
        else if (time >= 1f)
        {
            p3Ai.saerchteleport();
            p3Ai.ischessing = true;
            time = 0;
            playing = false;
            candoit = false;
            //P3使徒總控.Instance.DetectedAndSpawn();
           // this.gameObject.transform.parent = null;
            P3使徒總控.Instance.FindSpawnSpace(this.gameObject);
            gameObject.SetActive(false);
            //Destroy(this.gameObject);
            // P3使徒總控.Instance.P3change(this.gameObject.transform.parent.gameObject.name);
            //this.gameObject.transform.parent.gameObject.SetActive(false);



        }

    }

    IEnumerator StartGetPlayer()
    {
        
        yield return new WaitForSeconds(6f);
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    private void OnTriggerEnter(Collider other) {
       if(other.tag=="FLESH")
        {
            
           candoit=true;
        }
    }
private void OnTriggerExit(Collider other) {
       if(other.tag=="FLESH")
        {
            if(time<=0.5f)
            {
                time=0;
                candoit=false;
            }

        }
    }


            
            
        
        
        
    
}
