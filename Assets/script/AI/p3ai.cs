using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class p3ai : MonoBehaviour, IDatapersistence
{
    public float dis;
    public bool playerinLocker, SeeuInLocker, youcantseeme;
    public Transform Lockergps;
    public NewBehaviourScript NewBehaviourScript;
    public LayerMask obstruct, Rollingdoor,DOOR, obs,othermask;
    public bool deskdead;
    // public int warrning;
    NavMeshAgent navMeshAgent;
    public float distant;
    public Transform Player, seplayer;
    public float range;
    public bool watched = false;
    public float ramdonXZ;
    public float limitXZ;
    public bool inlimit, inarea;
    public float angle;
    public float lostdistant, Rate;
    public bool ischessing, onwalkspot, chessbool;
    public Animator P3animator;
    NavMeshHit hitInfo;
    public Vector3 walkpoint;
    [SerializeField]
    Vector3 teleportpoint;
    Vector3 walkdistant;
    public GameObject deadcam, deletcam, P4cam;
    public GameObject DeletP4;
    public CharacterController charactercon;
    public IAMDEAD over;
    public bool playerinsafehouse;
    public GameObject palyer;
    public LayerMask whatissavehouse;
    public bool stuckrotation,LockTarget;
    [SerializeField]
    FirstPersonController firstPersonController;
    NavMeshAgent navMesh;
    public time timescript;
    
    RaycastHit hit,doorhitter;
    private Coroutine coroutine;
    Vector3 savedwalkpoint;
    bool HaventFinishedOpen;
    bool traceanimeproceed;
    void Start()
    {
        TraitRrefrsh();
        firstPersonController = FindAnyObjectByType<FirstPersonController>();
        stuckrotation = false;
        deadcam.SetActive(false);
        P3animator.GetComponentInChildren<Animator>();
        navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        distant = Vector3.Distance(transform.position, Player.position);
        Vector3 Ndir = Player.transform.position - transform.position;
        Ray ray = new Ray(transform.position, (Player.transform.position - transform.position).normalized);

        if (Physics.Raycast(ray, out hit, distant, Rollingdoor, QueryTriggerInteraction.Ignore))
        {
            Debug.DrawLine(transform.position, Player.position, Color.green);
            //Debug.Log(hit.collider.name);
            youcantseeme = true;
        }
        if (!Physics.Raycast(ray, out hit, distant, Rollingdoor, QueryTriggerInteraction.Ignore))
        {
            /*RaycastHit rayh;
            if (Physics.Raycast(ray, out rayh, distant))
            {
                Debug.Log(rayh.collider.name);
            }*/

            youcantseeme = false;
            Debug.DrawLine(transform.position, Player.position, Color.red);
        }
        if (!HaventFinishedOpen && !ischessing && !watched)
        {
            if (Physics.Raycast(transform.position, transform.forward, out doorhitter, 3, ~othermask, QueryTriggerInteraction.Collide))
            {
                if (doorhitter.transform.GetComponent<DOOR>()!=null &&!doorhitter.transform.GetComponent<DOOR>().isopen)
                {
                    Debug.Log("Opening");
                    savedwalkpoint = walkpoint;
                    navMeshAgent.SetDestination(transform.position);
                    HaventFinishedOpen = true;
                    navMeshAgent.isStopped = true;
                    Invoke("DoorOpeningFun", 5f);
                }
            }
        }

        if (NewBehaviourScript.hiding == false)
        {
            playerinLocker = false;
        }
        else playerinLocker = true;
        if (chessbool == true)
        {
            chessing();
        }
        playerinsafehouse = Physics.CheckSphere(palyer.transform.position, 1, whatissavehouse);
        inlimit = Physics.CheckSphere(Player.position, limitXZ);

        if (NewBehaviourScript.Instance.endwarning == false)
        {
            switch (NewBehaviourScript.Instance.warrning)
            {
                case 0:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 16 * Rate;
                    else lostdistant = 11 * Rate;
                    range = 0;
                    LockTarget = false;
                    break;
                case 1:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 16 * Rate;
                    else lostdistant = 14 * Rate;
                    range = 10 * Rate;
                    LockTarget = false;
                    break;
                case 2:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 18 * Rate;
                    else lostdistant = 14.5f * Rate;
                    range = 12 * Rate;
                    LockTarget = true;
                    break;
                case 3:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 20 * Rate;
                    else lostdistant = 16.5f * Rate;
                    range = 14 * Rate;
                    LockTarget = true;
                    break;
                case 4:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 23 * Rate;
                    else lostdistant = 19 * Rate;
                    range = 16 * Rate;
                    LockTarget = true;
                    break;
                case 5:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 30 * Rate;
                    else lostdistant = 23 * Rate;
                    range = 20 * Rate;
                    LockTarget = true;
                    break;
            }
        }
        else
        {
            if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                lostdistant = 20 * Rate;
            else lostdistant = 16 * Rate;
            range = 15 * Rate;
            LockTarget = true;
        }

        if (ischessing == false && watched == false)
        {

            saerchwalkpoint();
        }
        if (distant <= range)
        {
            if (!playerinsafehouse && playerinLocker == false)
            {

                if (youcantseeme == false)
                {
                    ischessing = true;
                }


            }
        }
        if (playerinsafehouse && ischessing)
        {
            navMeshAgent.isStopped = true;
            ischessing = false;
            chessbool = false;
            P3animator.SetBool("aware", false);

        }
            
        if (ischessing == true && !chessbool)
        {



            StartCoroutine(aware());

        }
        /* else if (ischessing == true && chessbool == true)
         {
             chessing();
         }*/
        if ((distant > lostdistant || playerinsafehouse) && ischessing == true)
        {
           
            //StopAllCoroutines();
            Debug.Log("Lost Target");
            if (watched)
            {
                navMeshAgent.isStopped = true;
            }

            ischessing = false;
            chessbool = false;
            HaventFinishedOpen = false;
            P3animator.SetBool("aware", false);

        }
        
        if (((NewBehaviourScript.Instance.warrning == 0 && ischessing == false) || playerinsafehouse) && watched == true)
        {
            navMeshAgent.SetDestination(transform.position);
            navMeshAgent.isStopped = true;
            Vector3 dir = Player.transform.position - transform.position;
            float targetangle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;

            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, targetangle, 0), 0.5f);
            if (stuckrotation == false)
            {
                Debug.Log(targetangle);
                transform.rotation = Quaternion.Euler(0, targetangle, 0);
                stuckrotation = true;
            }

        }
        else if ((NewBehaviourScript.Instance.warrning == 1 || NewBehaviourScript.Instance.warrning == 2 || NewBehaviourScript.Instance.warrning == 3 || NewBehaviourScript.Instance.warrning == 4 || NewBehaviourScript.Instance.warrning == 5) && watched == true && ischessing == false && distant > range)
        {
            navMeshAgent.SetDestination(transform.position);
            navMeshAgent.isStopped = true;
            Vector3 dir = Player.transform.position - transform.position;
            float targetangle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, targetangle, 0), 0.5f);
            if (stuckrotation == false)
            {
                transform.rotation = Quaternion.Euler(0, targetangle, 0);
                stuckrotation = true;
            }
        }
        else
        {
            stuckrotation = false;

        }
        /*else if (warrning == 2 && watched == true && ischessing == false && distant > range)
        {
            navMeshAgent.isStopped = true;
            Vector3 dir = Player.transform.position - transform.position;
            float targetangle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, targetangle, 0), 0.5f);
            transform.rotation = Quaternion.Euler(0, targetangle, 0);
        }*/
    }


    public void saerchwalkpoint()
    {

        if (onwalkspot == false)
        {
            navMeshAgent.isStopped = false;
            if (!LockTarget||(LockTarget && (playerinsafehouse || playerinLocker)))
            {
                float randomX = Random.Range(Random.Range(-limitXZ, -ramdonXZ), Random.Range(limitXZ, ramdonXZ));
                float randomY = Random.Range(Random.Range(-limitXZ, -ramdonXZ), Random.Range(limitXZ, ramdonXZ));
                walkpoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomY);
            }
            else if(LockTarget&&!playerinsafehouse&&!playerinLocker)
            {

                float randomX = Random.Range(Random.Range(-limitXZ * 4, -ramdonXZ * 3), Random.Range(limitXZ * 4, ramdonXZ * 3));
                float randomY = Random.Range(Random.Range(-limitXZ * 4, -ramdonXZ * 3), Random.Range(limitXZ * 4, ramdonXZ * 3));
                walkpoint = new Vector3(Player.position.x + randomX, transform.position.y, Player.position.z + randomY);
                float diswalk = Vector3.Distance(Player.transform.position, walkpoint);
                if (diswalk <= 3.5f) saerchwalkpoint();
            }
            
            if (!NavMesh.SamplePosition(walkpoint, out hitInfo, 1, NavMesh.AllAreas))
            {
                saerchwalkpoint();
            }
            else
            {
                navMeshAgent.speed = (6.0f);
                navMeshAgent.isStopped = false;
                navMeshAgent.SetDestination(walkpoint);
                onwalkspot = true;
            }
        }
        Vector3 walkdistant = walkpoint - transform.position;
        if (walkdistant.magnitude <= 1f)
        {
            onwalkspot = false;
        }
    }


    public void saerchteleport()
    {
        float randomX = Random.Range(Random.Range(-limitXZ, -ramdonXZ), Random.Range(limitXZ, ramdonXZ));
        float randomY = Random.Range(Random.Range(-limitXZ, -ramdonXZ), Random.Range(limitXZ, ramdonXZ));
        teleportpoint = new Vector3(Player.position.x + randomX, transform.position.y, Player.position.z + randomY);

        Vector3 dir = teleportpoint - Player.position;
        float teledis = Vector3.Distance(teleportpoint, Player.position);
        if (teledis > 8)
        {
            if (Vector3.Angle(Player.forward, dir) >= (angle / 2) && NavMesh.SamplePosition(teleportpoint, out hitInfo, 1, NavMesh.AllAreas))
            {

                navMeshAgent.enabled = false;
                transform.position = teleportpoint;
                navMeshAgent.enabled = true;

            }
            else saerchteleport();
        }

        else
        {
            saerchteleport();
            Debug.Log("in8M");
        }

    }
    IEnumerator aware()
    {
        if (!navMeshAgent.isStopped)
        {
            navMeshAgent.isStopped = true;
            transform.LookAt(Player.transform.position);
        }
        chessbool = true;
        P3animator.SetBool("aware", true);
        yield return null;
        if (!HaventFinishedOpen)
        {
            navMeshAgent.isStopped = false;
        }
        
    }
    public void chessing()
    {
        if (ischessing == true)
        {
            if (distant >= 15)
            {
                navMeshAgent.speed = (7.0f);
            }
            else if (distant < 15)
            {
                navMeshAgent.speed = (6.5f);
            }
            if (!HaventFinishedOpen)
            {
                if (Physics.Raycast(transform.position, transform.forward, out doorhitter, 3, ~othermask, QueryTriggerInteraction.Collide))
                {
                    Debug.LogError(doorhitter.transform.gameObject);
                    if (doorhitter.transform.GetComponent<DOOR>() != null && !doorhitter.transform.GetComponent<DOOR>().isopen)
                    {
                        HaventFinishedOpen = true;
                        Debug.LogWarning("I can't open~");
                        savedwalkpoint = walkpoint;
                        navMeshAgent.SetDestination(transform.position);
                        navMeshAgent.isStopped = true;
                        Invoke("DoorOpeningFun", 5f);
                    }
                }
                else
                {
                    navMeshAgent.isStopped = false;
                }
            }
            if (NewBehaviourScript.hiding == false)
            {
                navMeshAgent.SetDestination(Player.position);
                Vector3 dir = Player.transform.position - transform.position;
                if (!Physics.Raycast(transform.position, dir, distant, obstruct) || distant <= 2.5f)
                {
                    SeeuInLocker = true;
                }
                else if (Physics.Raycast(transform.position, dir, distant, obstruct))
                {
                    SeeuInLocker = false;
                }
            }
            else if (NewBehaviourScript.hiding == true)
            {
                if (SeeuInLocker)
                {
                    NewBehaviourScript.p3cratekill = true;
                }
                else NewBehaviourScript.p3cratekill = false;


                navMeshAgent.SetDestination(new Vector3(Lockergps.position.x, transform.position.y, Lockergps.position.z));
                // Vector3 dir = Lockergps.position - transform.position;
                dis = Vector3.Distance(Lockergps.position, transform.position);

                if (dis <= 1f)
                {
                    coroutine = StartCoroutine("Llockergps");
                    navMeshAgent.isStopped = true;

                }
            }




        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player") && NewBehaviourScript.Cankill == true && GOODENDTRIGGER.instance.Isend == false && !playerinsafehouse)
        {
            Vector3 Direction;
            Direction = Player.position - transform.position;
            if (!Physics.Raycast(transform.position, Direction, out hit,distant, obs))
            {
                deadcam.SetActive(true);
                deletcam.SetActive(false);
                DeletP4.SetActive(false);
                P4cam.SetActive(false);
                charactercon.enabled = false;
                firstPersonController = seplayer.GetComponent<FirstPersonController>();
                firstPersonController.enabled = false;

                over.getover(3f);
            }
            if (Physics.Raycast(transform.position, Direction, out hit, distant))
            {
                Debug.Log(hit.transform.gameObject);
            }
        }
    }
    IEnumerator Llockergps()
    {

        if (traceanimeproceed != true)
        {
            P3animator.speed = 0f;
            traceanimeproceed = true;
            yield return new WaitForSeconds(2);
            P3animator.speed = 1f;
            navMeshAgent.isStopped = false;
            ischessing = false;
            chessbool = false;
            P3animator.SetBool("aware", false);
            onwalkspot = false;
            playerinLocker = true;
        }
        else
        {
            if (navMeshAgent.isStopped != true)
            {
                traceanimeproceed = false;
            }
            Debug.Log("���u���Ұʤ�");
        }



    }

    void TraitRrefrsh()
    {
        if (TraitManager.Instance.AttractDown)
        {
            Rate = 1 - TraitManager.Instance.AttractDownPower / 100;

        }
        else if (TraitManager.Instance.AttractUp)
        {
            Rate = 1 + TraitManager.Instance.AttractUpPower / 100;
        }
        else Rate = 1;
    }
    void DoorOpeningFun()
    {
        try
        {
            Debug.Log("Open Door Success");
            DOOR doorscript;
            Debug.Log(doorhitter.transform.gameObject);
            doorscript = doorhitter.transform.gameObject.GetComponent<DOOR>();
            doorscript.door();
            Invoke("FinishDoorFun", 2f);
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(savedwalkpoint);
        }
        catch {
            Debug.Log("None Door Object");
            navMeshAgent.isStopped = false;
            saerchwalkpoint();
        }
    }
    void FinishDoorFun() {
        HaventFinishedOpen = false;
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            this.transform.position = data.P3postition;
            this.transform.rotation = data.P3rotation;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.P3postition = this.transform.position;
        data.P3rotation = this.transform.rotation;
    }
}