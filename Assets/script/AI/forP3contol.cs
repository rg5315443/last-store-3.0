using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

public class forP3contol : MonoBehaviour, IDatapersistence
{
    public GameObject[] kidstesting;
    private static forP3contol instance;
    public static forP3contol Instance
    {
        get
        {
            return instance;
        }



    }
    private void Awake()
    {
        instance = this;
    }
    public void Start()
    {
        Invoke("enablep3kids", 1.5f);
    }
    public void enablep3kids() {
        transform.GetComponent<P3ㄏ畕羆北>().enabled = true;
    }
    // Start is called before the first frame update
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            transform.GetComponent<P3ㄏ畕羆北>().hadstarted = data.generatedP3kids;
            try
            {
                for (int i = 0; i < data.P3childobjs.Length; i++)
                {
                    GameObject childObj = data.P3childobjs[i];
                    GameObject parentObj = data.P3kidsparents[i];
                    Vector3 pos = data.P3kidspos[i];
                    Quaternion rot = data.P3kidsrot[i];

                    if (childObj != null && parentObj != null)
                    {
                        childObj.transform.SetParent(parentObj.transform);
                        childObj.transform.localPosition = pos;
                        childObj.transform.localRotation = rot;
                        childObj.SetActive(true);
                    }
                }
                for (int i = 0; i < P3ㄏ畕羆北.Instance.P3.Length; i++)
                {
                    P3ㄏ畕羆北.Instance.P3[i].GetComponent<Apostleselect>().HaveItem = data.P3KidsAlreadyHaveItem[i];
                }
            }
            catch (Exception e) { Debug.LogError(e); }
        }
    }



    public void savedata(ref Gamedata data)
    {
        int count = 0;
        kidstesting = new GameObject[P3ㄏ畕羆北.Instance.P3.Length];
        data.P3childobjs = new GameObject[P3ㄏ畕羆北.Instance.P3.Length];
        data.P3kidsparents = new GameObject[P3ㄏ畕羆北.Instance.P3.Length];
        data.P3kidspos = new Vector3[P3ㄏ畕羆北.Instance.P3.Length];
        data.P3kidsrot = new quaternion[P3ㄏ畕羆北.Instance.P3.Length];
        data.P3KidsAlreadyHaveItem = new bool[P3ㄏ畕羆北.Instance.P3.Length];
        foreach (GameObject p3kids in P3ㄏ畕羆北.Instance.P3)
        {

            if (p3kids.transform.GetChild(0).childCount > 0 && p3kids.transform.GetChild(0).GetChild(0).gameObject.activeSelf)
            {
                 kidstesting[count] = p3kids.transform.GetChild(0).GetChild(0).gameObject;
                 data.P3childobjs[count] = p3kids.transform.GetChild(0).GetChild(0).gameObject;
                 data.P3kidsparents[count] = p3kids.transform.GetChild(0).gameObject;
                 data.P3kidspos[count] = p3kids.transform.GetChild(0).GetChild(0).transform.localPosition;
                 data.P3kidsrot[count] = p3kids.transform.GetChild(0).GetChild(0).transform.localRotation;
                 data.P3KidsAlreadyHaveItem[count] = p3kids.GetComponent<Apostleselect>().HaveItem;
                 count++;
            }
            else if (p3kids.transform.GetChild(1).childCount > 0 && p3kids.transform.GetChild(1).GetChild(0).gameObject.activeSelf)
            {
                kidstesting[count] = p3kids.transform.GetChild(1).GetChild(0).gameObject;
                 data.P3childobjs[count] = p3kids.transform.GetChild(1).GetChild(0).gameObject;
                 data.P3kidsparents[count] = p3kids.transform.GetChild(1).gameObject;
                 data.P3kidspos[count] = p3kids.transform.GetChild(1).GetChild(0).transform.localPosition;
                 data.P3kidsrot[count] = p3kids.transform.GetChild(1).GetChild(0).transform.localRotation;
                 data.P3KidsAlreadyHaveItem[count] = p3kids.GetComponent<Apostleselect>().HaveItem;
                count++;
            }
            else if (p3kids.transform.childCount == 3)
            {
                Debug.Log("Found item saving here" + p3kids.transform.GetChild(2).name);
                kidstesting[count] = p3kids.transform.GetChild(2).gameObject;
                data.P3childobjs[count] = p3kids.transform.GetChild(2).gameObject;
                data.P3kidsparents[count] = p3kids;
                data.P3kidspos[count] = p3kids.transform.GetChild(2).transform.localPosition;
                data.P3kidsrot[count] = p3kids.transform.GetChild(2).transform.localRotation;
                data.P3KidsAlreadyHaveItem[count] = p3kids.GetComponent<Apostleselect>().HaveItem;
                count++;
            }
            else {
                count++;
            }
            if (p3kids.GetComponent<Apostleselect>().HaveItem)
                {
                    Debug.Log("Have item founded");
                }
        }
    }

}
