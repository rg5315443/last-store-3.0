using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.AI;

public class 追擊 : MonoBehaviour ,IDatapersistence
{
    public int i;
    public bool Canchess, SeeuInLocker, youcantseeme;
    public Transform LockerGps;
    public float distant;
    public bool chessbool;
    public float lostdistant;
    public AudioSource walk,run,roar;
    public GameObject Bgrid,Cgrid;
    public p3ai warnningcount;
    public IAMDEAD over;
    public time timescript;
    public Animator ani;
    public bool playerinlocker;
    public LayerMask whatIsGround, whatIsPlayer,whatisLight,whatisDark,whatisWarning, whatissavehouse,wall, Rollingdoor,obs;
    public float 巡邏間隔,GuardTimeLimit,GuardCurentTime;
    public bool 正在巡邏=false;
    public bool playerinlight;
    public float lockdis;
    //Patroling
     [SerializeField]Vector3 walkPoint,chesspoint,getDarkpoint,distancetodark;
    public bool walkPointSet,toDarkset,chessing;
    public float walkPointRange,walkDarkRange,standsight,walksight,runsight, crouchsight;
    public GameObject deadcamera,deletecam;
    CharacterController PlayerRig;
   
    //bool alreadyAttacked;
    public bool 視野成功觸發=false,chaseison=false,onwargps=false,isgpsing=false;
    //States
    public float sightRange, DectecRange, attackRange,guardRange,Rate;
    public bool playerinsafehouse,playerInDeadRange, playerinsight,InDark,InLight,isecu,inwarning,InGround,testwarn,isreset;
    float anispeed;
    float smoothdamp=0f;
    float smoothtime=0.3f;
    NavMeshAgent nav;
    public GameObject palyer, playerray, palyercam,P4cam;
    public GameObject detecobj;
    NavMeshHit hitInfo;
    RaycastHit sphit,hit;
    public 小心地滑[] P4AI;
    public GameObject DeletP4;
    public P4Manager P4Manager;
    public bool P4doing;
    Coroutine Lockercoroutine;
    public static int ScareCount;
    public static bool ScareGranted;
    void Start()
    {
        TraitRrefrsh();
        P4AI = FindObjectsOfType<小心地滑>();
        PlayerRig =palyer.GetComponent<CharacterController>();
        nav=this.GetComponent<NavMeshAgent>();
        InvokeRepeating("CheckDistanceScore", 0.01f, 1f);
        //timescript = FindObjectOfType<time>();
    }

    // Update is called once per frame
   void Update()
    {
        if (ScareCount >= 5 && !ScareGranted)
        {
            FinalScorecaculator.CaculatingScore += 1000;
            ScareGranted = true;
        }
        /* foreach (小心地滑 p in P4AI)
         {
             if (p.musicego)
             { 
                 P4doing = true;
                 break;
             }else P4doing = false;
         }*/


        P4doing = P4Manager.Debuffing;

        if (nav.isStopped == true)
        {
           // Debug.Log("stop");
        }
        if (Canchess == true)
        {
            Chess();
        }
        if (NewBehaviourScript.Instance.hiding == false)
        {
            playerinlocker = false;
        }
        distant = Vector3.Distance(transform.position, palyer.transform.position);
        
        Vector3 Ndir = playerray.transform.position - transform.position;
        Ray ray = new Ray(transform.position, (playerray.transform.position - transform.position).normalized);

        if (Physics.Raycast(ray, out hit, distant, Rollingdoor, QueryTriggerInteraction.Ignore))
        {
            Debug.DrawLine(transform.position, playerray.transform.position, Color.green);
            //Debug.Log(hit.collider.name);
            youcantseeme = true;
        }
        if (!Physics.Raycast(ray, out hit, distant, Rollingdoor, QueryTriggerInteraction.Ignore))
        {
           /* RaycastHit rayh;
            if (Physics.Raycast(ray, out rayh, distant))
            {
                //Debug.Log(rayh.collider.name);
            }*/

            youcantseeme = false;
            Debug.DrawLine(transform.position, playerray.transform.position, Color.red);
        }


        if (nav.pathStatus== NavMeshPathStatus.PathInvalid)
        {
            Debug.Log("巡路失敗");
        }
        if (NewBehaviourScript.Instance.endwarning == false)
        {
            switch (NewBehaviourScript.Instance.warrning)
            {
                case 0:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                    {
                        lostdistant = 20 * Rate;
                    }else lostdistant = 15 * Rate;

                    standsight = 0.25f * Rate;
                    crouchsight = 0.5f * Rate;
                    walksight = 2 * Rate;
                    runsight = 12 * Rate;
                    break;
                case 1:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                    {
                        lostdistant = 25 * Rate;
                    } else lostdistant = 18 * Rate;
                       
                    standsight = 0.35f * Rate;
                    crouchsight = 0.75f * Rate;
                    walksight =  4f* Rate;
                    runsight = 14 * Rate;
                    break;
                case 2:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                    {
                        lostdistant = 27 * Rate;
                    }
                    else lostdistant = 20 * Rate;
                        
                    standsight = 0.5f * Rate;
                    crouchsight = 1f * Rate;
                    walksight = 6 * Rate;
                    runsight = 16 * Rate;
                    break;
                case 3:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                    {
                        lostdistant = 29 * Rate;
                    }
                    else lostdistant = 22 * Rate;


                    standsight = 1 * Rate;
                    crouchsight = 2f * Rate;
                    walksight = 10 * Rate;
                    runsight = 20 * Rate;
                    break;
                case 4:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 31 * Rate;
                    else lostdistant = 25 * Rate;
                    standsight = 1 * Rate;
                    crouchsight = 3.5f * Rate;
                    walksight = 11.5f * Rate;
                    runsight = 22 * Rate;
                    break;
                case 5:
                    if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                        lostdistant = 100 * Rate;
                    else lostdistant = 40 * Rate;
                    standsight = 5 * Rate;
                    crouchsight = 12 * Rate;
                    walksight = 20 * Rate;
                    runsight = 40 * Rate;
                    break;
            }
        }
        else
        {
            if (!Physics.Raycast(ray, out hit, distant, obs, QueryTriggerInteraction.Ignore))
                lostdistant = 30 * Rate;
            else lostdistant = 20 * Rate;
            standsight = 1 * Rate;
            crouchsight = 2.5f * Rate;
            walksight = 12 * Rate;
            runsight = 20 * Rate;
        }
       

        if (NewBehaviourScript.Instance.warrning >0)
        { 

            /*if(!chessing)
            {
            if(inwarning==false)
            {
                testwarn=false;
                inwarning=true;
            }
           
            if(testwarn==false)
            {
                if(Bgrid.tag=="warrninggrid")
                {
                    nav.SetDestination(Bgrid.transform.position);
                     distant=Vector3.Distance(transform.position,Bgrid.transform.position);
                    if(distant<=1f&&testwarn==false)
                    {
                        testwarn=true;
                    }
                }else if(Cgrid.tag=="warrninggrid")
                {
                    nav.SetDestination(Cgrid.transform.position);
                     distant=Vector3.Distance(transform.position,Cgrid.transform.position);
                     if(distant<=1f&&testwarn==false)
                    {
                        testwarn=true;
                    }
                 }
                 
            }
            }*/
                /*NavMeshPath path = new NavMeshPath();
                if (NavMesh.CalculatePath(transform.position, nav.pathEndPosition, NavMesh.AllAreas, path))
                {
                    
                    for (int i = 1; i < path.corners.Length; i++)
                    {
                        Vector3 from = new Vector3(path.corners[i - 1].x,path.corners[i - 1].y,path.corners[i - 1].z) ;
                        Vector3 to = path.corners[i];
                        
                        
                        if (Physics.Raycast(from, to - from, out hitInfo, Vector3.Distance(from, to)))
                        {
                            if(hitInfo.collider.gameObject.layer!=8)
                            {
                                   
                            }
                            Debug.Log("Hit object with layer " + LayerMask.LayerToName(hitInfo.collider.gameObject.layer));
                        }
                    }
                }*/
               /* isgpsing=true;
             GameObject warrningGPS = GameObject.FindGameObjectWithTag("warGPS");
             Debug.Log(warrningGPS);
             distanttogps=this.gameObject.transform.position-warrningGPS.transform.position;
            nav.SetDestination(warrningGPS.transform.position);

        
        if(distancetodark.magnitude<1f)
        {
                Debug.Log("ff");
                onwargps=true;
                isgpsing=false;
        }*/
      /* Debug.Log(Bgrid.tag);

    

        if(isgpsing==false)
        {
            walkPointSet=false;
            toDarkset=false;
            isgpsing=true;
        }

        }
        else if(warnningcount.warrning<=0)
        {
            whatIsGround=whatisDark;
            walkPointRange=10;
            isgpsing=false;*/
        whatIsGround=whatisWarning;
        walkPointRange=200;
        }else
        whatIsGround=whatisDark;
            if(PlayerRig.velocity.magnitude==0)
        {
            guardRange=standsight;
            //Debug.Log("stand");
        }
        else if(PlayerRig.velocity.magnitude>0&&!Input.GetKey(KeyCode.LeftShift))
        {
            guardRange=walksight;
            //Debug.Log("walk");

        }
        if (((Input.GetKey(KeyCode.LeftShift)&& Input.GetKey(KeyCode.LeftControl))|| Input.GetKey(KeyCode.LeftControl))&& PlayerRig.velocity.magnitude > 0 && !TraitManager.Instance.lame)
        {
            guardRange = crouchsight;
        }
        else if((Input.GetKey(KeyCode.LeftShift)&&PlayerRig.velocity.magnitude>0) && !TraitManager.Instance.lame)
            {
                guardRange=runsight;
               // Debug.Log("run");
            }


        playerinsafehouse = Physics.CheckSphere(detecobj.transform.position, DectecRange, whatissavehouse);
        playerinsight =Physics.CheckSphere(transform.position,guardRange,whatIsPlayer);
        playerinlight=Physics.CheckSphere(detecobj.transform.position, DectecRange, whatisLight);
        playerInDeadRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        InDark = Physics.CheckSphere(transform.position, sightRange, whatIsGround);
        InGround = Physics.CheckSphere(transform.position, sightRange, whatisDark);
        InLight = Physics.CheckSphere(transform.position, sightRange, whatisLight);
       /* if(playerinlight)
        {
            isecu=false;
        }*/

        if(Canchess == false&&chessing==false&&InLight==false)
        {
            toDarkset=false;
           
            ani.SetBool("scare",false);
            Patroling();
            //Debug.Log("Patroling");
        } 
             if(InLight)
            { 
                Canchess = false;
                chessing=false;
                gotodark();
             // Debug.Log("Escaping");
            }
        if ((((視野成功觸發==true||playerinsight)&&toDarkset==false&&(!playerinlight || P4doing) && !playerinsafehouse) ||(chessing==true&&(!playerinlight || P4doing) &&!playerinsafehouse) &&!InLight)&& !playerinlocker&& !youcantseeme)
        {
           
            if(InDark)
            {
                isreset=false;
                 chessing=true;
                //StartCoroutine("chessanicount");
                Dected();
               // Debug.Log("Chessing");
            }else if(InGround)
            {
                 isreset=false;
                 chessing=true;
                //StartCoroutine("chessanicount");
                Dected();
            }
            
        }
        else if(InLight||(playerinlight && !P4doing) ||playerinsafehouse)
        {
            
           // Debug.Log("Lost");
            Lost();
        }
        else if(Vector3.Distance(transform.position,palyer.transform.position)>= lostdistant)
         {
            Lost();
         }
        
        if((chessing == false|| (playerinlight && !P4doing) || toDarkset == true) &&isreset == false)
        {
            isreset = true;
            Debug.Log("break");
            正在巡邏 = false;
            walkPointSet = false;
           //StopCoroutine("chessanicount");
           // Dected();

        }

        if(playerInDeadRange&&NewBehaviourScript.Instance.Cankill == true&&!InLight&&GOODENDTRIGGER.instance.Isend == false && !playerinlight)
        {
            deletecam.SetActive(false);
            P4cam.SetActive(false);
            DeletP4.SetActive(false);
            CharacterController playercon = palyer.GetComponent<CharacterController>();
            playercon.enabled = false;
            FirstPersonController firstPersonController = palyer.GetComponent<FirstPersonController>();
            firstPersonController.enabled = false;
            deadcamera.SetActive(true);
           
            over.getover(6f);
            Destroy(this.gameObject);
        }
    }
    private void Patroling()
    {
        
        if (!walkPointSet&&正在巡邏==false) 
        {              
            SearchWalkPoint();
        }
        if (walkPointSet&&正在巡邏==false)
        {
            /* if(NavMesh.SamplePosition(walkPoint,out hitInfo,1,NavMesh.AllAreas))
             {
                 GuardCurentTime += Time.deltaTime;
                 if (GuardCurentTime >= GuardTimeLimit)
                 {
                     walkPointSet = false;
                 }
                 nav.isStopped = false;
                 nav.speed=(4f);
                // Debug.Log("NavMeshAgent isStopped status: " + nav.isStopped);
                 nav.SetDestination(walkPoint);
                 ani.SetFloat("Speed",1);
             }
             else
             walkPointSet=false;*/
            GuardCurentTime += Time.deltaTime;
            if (GuardCurentTime >= GuardTimeLimit)
            {
                walkPointSet = false;
            }
            nav.isStopped = false;
            nav.speed = (4f);
            // Debug.Log("NavMeshAgent isStopped status: " + nav.isStopped);
            nav.SetDestination(walkPoint);
            ani.SetFloat("Speed", 1);
        }
        else ani.SetFloat("Speed",0);
        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
            if (distanceToWalkPoint.magnitude < 1f&&正在巡邏==false)
            {
            //Debug.Log("DDD");
            GuardCurentTime = 0;
            ani.SetFloat("Speed",0);
            StartCoroutine("ddd");
            }
    }
    private void SearchWalkPoint()
    {
        GuardCurentTime = 0;
        for (int i=0;i<20;i++)
        {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);
        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        
        if (!Physics.SphereCast(walkPoint, 0.5f, -transform.up,out sphit,4f,whatisLight)&&Physics.SphereCast(walkPoint, 0.5f, -transform.up,out sphit,4f,whatIsGround))
        {
           
            if(NavMesh.SamplePosition(walkPoint,out hitInfo,1,NavMesh.AllAreas))
            {

            nav.isStopped = false;
            walkPointSet = true;
            //Debug.Log("suc");
            Vector3 dir=(walkPoint-transform.position).normalized;
            float targetangle=Mathf.Atan2(dir.x,dir.z)*Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y,targetangle,ref smoothdamp,smoothtime);
            transform.rotation=Quaternion.Euler(0f,angle,0f);
            break;
            }
            else
            {
                //Debug.Log("失敗");
                    //SearchWalkPoint();
            }
        }

        }
        
        
    }
    private void gotodark()
    {
       
        if(!toDarkset) SearchDark();
        
        if(toDarkset) 
        {
            /*if(NavMesh.SamplePosition(getDarkpoint,out hitInfo,1,NavMesh.AllAreas))
           {
               nav.isStopped = false;
              // Debug.Log("NavMeshAgent isStopped status: " + nav.isStopped);
               nav.SetDestination(getDarkpoint);
               ani.SetBool("scare",true);
               nav.speed=(8f);
           }
           else
           toDarkset=false;*/
            nav.isStopped = false;
            // Debug.Log("NavMeshAgent isStopped status: " + nav.isStopped);
            nav.SetDestination(getDarkpoint);
            ani.SetBool("scare", true);
            ScareCount++;
            nav.speed = (8f);
        }
        Vector3 distancetodark=this.transform.position-getDarkpoint;
        if(distancetodark.magnitude<1f)
        {
         //Debug.Log("DDD");  
         //SearchWalkPoint();
         toDarkset=false;
         ani.SetFloat("Speed",0);
         ani.SetBool("scare",false);
        }
        
    }
    public void SearchDark(){
         nav.isStopped = true;

        for( i=0;i<30;i++)
        {
        float rZ = Random.Range(-walkDarkRange, walkDarkRange);
        float rX = Random.Range(-walkDarkRange, walkDarkRange);
        getDarkpoint=new Vector3(transform.position.x + rX, transform.position.y, transform.position.z + rZ);
            Debug.Log(i);
         if (!Physics.SphereCast(getDarkpoint, 0.5f, -transform.up,out sphit,10f,whatisLight)&&Physics.SphereCast(getDarkpoint, 0.5f,-transform.up,out sphit,10f,whatIsGround))
        {
                if (NavMesh.SamplePosition(getDarkpoint, out hitInfo, 1, NavMesh.AllAreas))
                {
                    nav.isStopped = false;
                    walkPointSet = false;
                    if (!walkPointSet) SearchWalkPoint();
                    toDarkset = true;
                    break;
                    //transform.LookAt(getDarkpoint);
                }
                //else SearchDark();
        }
            if (i == 29)
            {
                getDarkpoint = new Vector3(29.2743721f, 1.15999997f, -38.8164063f);
                nav.isStopped = false;
                walkPointSet = false;
                if (!walkPointSet) SearchWalkPoint();

                toDarkset = true;
                break;
            }
           // else SearchDark();
        } 
    }



     IEnumerator ddd()
    {   
        正在巡邏=true;
        yield return new WaitForSeconds(巡邏間隔); 
        walkPointSet = false;
        正在巡邏=false;
    } 
    public void Dected()
    {
        if (isecu == false)
        {
            isecu = true;
            StartCoroutine("Dectedcount");
        }
    }
    private void Lost()
    {
       // Debug.Log("Lost");
        /*if(isreset==false)
        {
            toDarkset=false;
            walkPointSet=false;
            isreset=true;
        }*/
        //StopCoroutine("chessanicount");

        //Debug.Log("Lost");
        chessing = false;
        isecu =false;
        Canchess = false;
        ani.SetBool("Lost",true);
        ani.SetBool("dected",false);
    }
    public void Chess()
    {
        if (distant >= 10)
        {
            nav.speed = (8.5f);
        }
        else if (distant < 10)
        {
            nav.speed = (8f);
        }
        if (NewBehaviourScript.Instance.hiding == false)
        {
            Debug.Log("chess");
            nav.SetDestination(palyer.transform.position);
            Vector3 dir = palyer.transform.position - transform.position;
            if (!Physics.Raycast(transform.position, dir, distant, wall)&&distant<=8)
            {

                SeeuInLocker = true;
                Debug.DrawLine(transform.position, palyer.transform.position, Color.red);
                
            }
            else if (Physics.Raycast(transform.position, dir, distant, wall)|| distant > 8)
            {
                SeeuInLocker = false;
                Debug.DrawLine(transform.position, palyer.transform.position, Color.green);
            }
        }
        else if (NewBehaviourScript.Instance.hiding == true)
        {
            if (SeeuInLocker)
            {
                NewBehaviourScript.Instance.p2cratekill = true;
            }else NewBehaviourScript.Instance.p2cratekill = false;
            nav.SetDestination(LockerGps.position);
            //Vector3 dir = LockerGps.position - transform.position;
            lockdis = Vector3.Distance(LockerGps.position, transform.position);
            if (lockdis <= 5f)
            {
                Lockercoroutine = StartCoroutine("Llockergps");
            }

        }
        chesspoint = new Vector3(palyer.transform.position.x, transform.position.y, palyer.transform.position.z);
        if (Physics.Raycast(chesspoint, -transform.up, 10f, whatisLight) && !P4doing)
        {

            Debug.Log("Lost");
            ani.SetBool("Lost", true);
            // ani.SetBool("dected",false);
            nav.speed = (4f);
            isecu = false;
            視野成功觸發 = false;
            Canchess = false;

        }
    }
    IEnumerator Dectedcount()
    {
        
        /*發現警告動畫*/

            
            
            ani.SetBool("Lost",false);
            ani.SetBool("dected",true);
            nav.isStopped = true;
            transform.LookAt(palyercam.transform.position);

            yield return new WaitForSeconds(1.2f);
            nav.isStopped = false;
            ani.SetBool("dected",false);
            Canchess = true;
        






             
        /*if(Physics.Raycast(chesspoint, -transform.up, 10f, whatIsGround))
        {
            
       
         }*/ 
    }

    IEnumerator Llockergps()
    {


        ani.speed = 0f;
        nav.isStopped = true;
        Canchess = false;
        yield return new WaitForSeconds(2);
        if (chessing == false)
        {
            StopCoroutine(Lockercoroutine);
        }
        ani.speed = 1f;
        nav.isStopped = false;
        chessbool = false;
        //onwalkspot = false;
        playerinlocker = true;
        chessing = false;
        isecu = false;
        ani.SetBool("Lost", true);
        ani.SetBool("dected", false);


    }
    void TraitRrefrsh()
    {
        if (TraitManager.Instance.AttractDown)
        {
            Rate = 1-TraitManager.Instance.AttractDownPower/100;
            
        }
        else if (TraitManager.Instance.AttractUp)
        {
            Rate = 1+TraitManager.Instance.AttractUpPower/100;          
        }else Rate = 1;
    }
    void CheckDistanceScore()
    {
        if (distant < 10 && Canchess && !IAMDEAD.GotKilled)
        {
            FinalScorecaculator.CaculatingScore += 50;
        }
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            this.transform.position = data.P2postition;
            this.transform.rotation = data.P2rotation;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.P2postition = this.transform.position;
        data.P2rotation = this.transform.rotation;
    }

}
