using System.Collections;
using System.Collections.Generic;
using System.Net;

using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;
using UnityEngine.XR;

public class P6 : MonoBehaviour
{
    public Transform Player_transform, Cam_transform, P6_transform;
    public SanManager SanManager;
    public float RandomXY,LimitXY;
    public float RefreshTime, ShowTime,angle;
    bool CanTeleport = true , InCD;
    public Vector3 TeleportPoint;
    float time;
    public bool state_1, state_2;
    public IAMDEAD over;
    public GameObject DeadScene, clip;
    /*public AudioClip clip;
    public AudioSource audioSource;*/
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 originalRotation = P6_transform.eulerAngles;
        P6_transform.LookAt(Player_transform);
        originalRotation.x = 0;
        if (SanManager.Sanity<= SanManager.Lv1valve&&(SanManager.Sanity>SanManager.Lv2valve))
        {
            state_1 = true;
            state_2 = false;
            State_1();
        }
        else if (SanManager.Sanity <= SanManager.Lv2valve)
        {
            state_1 = false;
            state_2 = true;
            State_2(); 
        }
       /* if (!CanTeleport)
        {
            time += Time.deltaTime;
            if (time >= RefreshTime)
            {
                CanTeleport = true;
                time = 0;
            }
        }*/
    }
    void State_1()
    {
        if (CanTeleport)
        {       
            RefreshTime = 10;
            float randomx = Random.Range(Random.Range(-LimitXY * 2, -RandomXY * 2) , Random.Range(LimitXY * 2, RandomXY * 2));
            float randomy = Random.Range(Random.Range(-LimitXY * 2, -RandomXY * 2), Random.Range(LimitXY * 2, RandomXY * 2));
            TeleportPoint = new Vector3(Player_transform.position.x+ randomx, Player_transform.position.y, Player_transform.position.z+ randomy);
            Dectec(TeleportPoint, 10);
        }
    }

    void State_2()
    {
        if (CanTeleport)
        {     
            RefreshTime = 5f;
            LimitXY = 2.5f;
            float randomx = Random.Range(Random.Range(-LimitXY, -RandomXY/2), Random.Range(LimitXY, RandomXY/2));
            float randomy = Random.Range(Random.Range(-LimitXY , -RandomXY/2), Random.Range(LimitXY , RandomXY/2));
            TeleportPoint = new Vector3(Player_transform.position.x + randomx, Player_transform.position.y, Player_transform.position.z + randomy);
            Dectec(TeleportPoint, 2.5f);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            clip.SetActive(true);
            DeadScene.SetActive(true);
            over.getover(1f);
        }
    }

    void Dectec(Vector3 teleportpoint, float Dis)
    {
        NavMeshHit meshHit;
        float PointDis;
        Vector3 dir = teleportpoint - Player_transform.position;
        PointDis = Vector3.Distance(teleportpoint, Player_transform.position);
        if (Vector3.Angle(Cam_transform.forward, dir) >= (angle / 2))
        {
            if (NavMesh.SamplePosition(teleportpoint, out meshHit, 1f, NavMesh.AllAreas) && PointDis >= Dis)
            {
                P6_transform.gameObject.SetActive(true);
                Invoke("ShowCount", ShowTime);
                P6_transform.position = new Vector3(teleportpoint.x, P6_transform.position.y, teleportpoint.z);
                CanTeleport = false;
            }
        }
    }
    void ShowCount()
    {
        P6_transform.gameObject.SetActive(false);
        Invoke("CD_Count", 5);
    }
    void CD_Count()
    {
        
        CanTeleport = true;
        
       
    }


}
