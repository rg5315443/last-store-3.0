using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UIElements.UxmlAttributeDescription;

public class P3使徒總控 : MonoBehaviour,IDatapersistence
{
    private static P3使徒總控 instance;
    public static P3使徒總控 Instance
    {
        get {
           return instance;
        }
            
        
        
    }
    public GameObject[] P3;
    public int spawncrew;
    public bool hadstarted,IsFull = false;
    Apostleselect Apostleselect_Script;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        if (PlayerPrefs.GetInt("Turtorial") == 2 && !hadstarted)
        {
            hadstarted = true;
            for (int i = 0; i < spawncrew; i++)
            {
                int num = Random.Range(0, P3.Length);

                while (P3[num].GetComponent<Apostleselect>().HaveItem == true)
                {
                    num = Random.Range(0, P3.Length);
                }
                Apostleselect APostleselect_Script = P3[num].GetComponent<Apostleselect>();
                Debug.Log(num);
                //P3[num].SetActive(true);

                APostleselect_Script.HaveItem = true;
                APostleselect_Script.Reset_P3();
            }
        }
        /*if (PlayerPrefs.GetInt("Turtorial") == 2&&!hadstarted)
        {
            Debug.Log("Bypassed!!");
            for (int i = 0; i < spawncrew; i++)
            {
                int num = Random.Range(0, P3.Length);
                while (P3[num].transform.childCount != 0)
                {
                    num = Random.Range(0, P3.Length);
                }
                //P3[num].SetActive(true);
                Apostleselect Select = P3[num].GetComponent<Apostleselect>();
                Select.Reset_P3();
            }
            hadstarted = true;
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /*  public void P3change(string P3name)
  {
      string P3namein=P3name;
      int P3number = Random.Range(0,P3.Length);

      if(P3[P3number].name!=P3name&& P3[P3number].activeSelf == false)
      {
          P3[P3number].SetActive(true);
          Apostleselect select = P3[P3number].GetComponent<Apostleselect>();
          select.Select();
      }//else P3change(P3namein);

  }*/
    public void FindSpawnSpace(GameObject P3Type)
    {
        int count = 0;
        foreach (GameObject gameObject in P3)
        {
            
             Apostleselect_Script = gameObject.GetComponent<Apostleselect>();
            if (Apostleselect_Script.HaveItem)
            {
                count++;
                if(count == P3.Length)
                IsFull = true;
            }
            if (P3Type.CompareTag("P3kind"))
            {
                if (Apostleselect_Script.GoodApsotle.transform.childCount == 0)
                {
                    P3Type.transform.SetParent(Apostleselect_Script.GoodApsotle.transform);
                    P3Type.transform.position = Apostleselect_Script.GoodApsotle.transform.position;
              
                    
                }
            }
            /*if (P3Type.CompareTag("BadApsotle"))
            {
                if (Apostleselect_Script.BadApsotle.transform.childCount == 0)
                {
                    P3Type.transform.SetParent(Apostleselect_Script.GoodApsotle.transform);
                    P3Type.transform.position = Apostleselect_Script.GoodApsotle.transform.position;
                }
            }*/
        }
               
                int num = Random.Range(0, P3.Length);
                P3[num].GetComponent<Apostleselect>().Refersh();
        if (!IsFull)
        {
            while (P3[num].GetComponent<Apostleselect>().HaveItem == true)
            {
                P3[num].GetComponent<Apostleselect>().Refersh();
                num = Random.Range(0, P3.Length);
            }
            Apostleselect apostleselect_Script = P3[num].GetComponent<Apostleselect>();
            apostleselect_Script.Reset_P3();
        }
        else
        {
            Apostleselect_Script.Reset_P3();
            IsFull = false;
        }
                
                Debug.Log(num);
                //P3[num].SetActive(true);
                
    }

        

    



    public void DetectedAndSpawn()
    { 
        foreach(GameObject gameObject in P3) 
        {
            if (gameObject.transform.childCount == 0)
            { 
                Apostleselect Apostleselect = gameObject.GetComponent<Apostleselect>();
                Apostleselect.Reset_P3();
                break;
            }
        }
    }

    public void loaddata(Gamedata data)
    {
        if (data != null) {
            hadstarted = data.generatedP3kids;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.generatedP3kids = hadstarted;
    }
}
