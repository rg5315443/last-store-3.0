using System.Diagnostics;
using UnityEngine;
using System.Windows.Forms;
using UnityEngine.AI;
using System;
using UnityEngine.SceneManagement;
using System.IO;
using Unity.Mathematics;

public class P5AI1 : MonoBehaviour,IDatapersistence
{
    
    public Transform P5head;
    public LayerMask obc;
    public AudioClip SE;
    AudioSource audioSource;
    public float fadetime=5,time,volume=0;
    public bool Playeringround, warning, show, notinrange, onend;
    public float warnline, showline, distance,Sdistance, range;
    public GameObject player,playercam;
    public Material material;
    public Vector3 p5pos, p5target;
    public Transform centrePoint;
    public Vector3 point;
    public Vector3 currentVelocity = Vector3.zero;
    private int nextUpdate = 1;
    public double radia;
    public int randomscare;
    RaycastHit Hit;
    NavMeshHit NavMeshHit;

    // Start is called before the first frame update
    void Start()
    {
        audioSource=GetComponent<AudioSource>();
        point = transform.position;
        InvokeRepeating("p5pathing", 1, 6f);
        material.SetFloat("_alpha", 0);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Time.time >= nextUpdate)
        {
            nextUpdate = (int)(Mathf.FloorToInt(Time.time) + 0.5);
            radiation();
        }

        transform.LookAt(player.transform);
        Vector3 PlayerPos = player.transform.position;
        Vector3 P5Pos = transform.position;
        distance = Vector3.Distance(P5Pos, PlayerPos);
        if (onend == false) transform.position =  point;
        Vector3 distantend = transform.position - point;
        if (distantend.magnitude < 1f)
        {
            onend = true;

        }
        if (distance < warnline)
        {
            Vector3 dir = playercam.transform.position-P5head.position ;
            if (!Physics.Raycast(P5head.position, dir, distance, obc))
            {
                //UnityEngine.Debug.Log("WALL"+Hit.collider.name);
                UnityEngine.Debug.DrawRay(P5head.position, dir, UnityEngine.Color.blue);
                warning = true;
                if (material.GetFloat("_alpha") <= 1)
                {
                    material.SetFloat("_alpha", (float)radia);
                }
            }
            else
            {
                //UnityEngine.Debug.Log("HIT" + Hit.collider.name);
                UnityEngine.Debug.DrawRay(P5head.position, dir, UnityEngine.Color.red);
              
                
                    material.SetFloat("_alpha", 0);
               
                warning = false;
            } 


        }
        else
        {
            warning = false;
        }
        if (distance <= showline && onend == true && NewBehaviourScript.Instance.hiding == false)
        {
            show = true;
        }
        if (show == true)
        {
            
                int scare;
                scare = UnityEngine.Random.Range(1, 5);
                switch (scare)
                {
                    case 1:
                        {

                            switch (MessageBox.Show(" ", "aHR0cHM6Ly90aG91Z2h0Y2F0YWxvZy5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTQvMDcvc2NyZWVuLXNob3QtMjAxNC0wNy0wMi1hdC00LTI5LTU0LXBtLnBuZz93PTE1NzEmaD0xMDIzJmNyb3A9MQ==",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                            {
                                case DialogResult.Yes:
                                    {
                                        ProcessStartInfo proc = new ProcessStartInfo("cmd.exe");
                                        proc.Verb = "runas";
                                        proc.UseShellExecute = false;
                                        proc.RedirectStandardOutput = true;
                                        proc.CreateNoWindow = true;
                                        proc.RedirectStandardInput = true;
                                        var process = Process.Start(proc);
                                        // process.StandardInput.WriteLine(@"shutdown /f /s /t 0");
                                        break;
                                    }
                                case DialogResult.No:
                                    MessageBox.Show("...");
                                    {
                                        ProcessStartInfo proc = new ProcessStartInfo("cmd.exe");
                                        proc.Verb = "runas";
                                        proc.UseShellExecute = false;
                                        proc.RedirectStandardOutput = true;
                                        proc.CreateNoWindow = true;
                                        proc.RedirectStandardInput = true;
                                        var process = Process.Start(proc);
                                        process.StandardInput.WriteLine(@"start secret_trigger.vbs");
                                        UnityEngine.Application.Quit();
                                        //UnityEditor.EditorApplication.isPlaying = false;
                                        break;

                                    }

                            }
                            break;

                        }
                    case 2:
                        {
                            PlayerPrefs.SetInt("P5kill", 1);
                            SceneManager.LoadScene(0);
                            break;

                        }
                    case 3:
                        {
                        int diarynumber = UnityEngine.Random.Range(1,7);
                        switch (diarynumber) {
                            case 1:
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnkx.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : 2039/05/06 星期一 警衛編號 : 9526\r\n");
                                        writer.Write("今天是入職後的第一個禮拜\r\n");
                                        writer.Write("夜晚的賣場比起早上\r\n");
                                        writer.Write("顯得更加得格外的陰森\r\n");
                                        writer.Write("尤其是餐飲區那塊\r\n");
                                        writer.Write("黑的伸手不見五指\r\n");
                                        writer.Write("得靠手電筒才能勉強看見前面有甚麼\r\n");
                                        writer.Write("真希望有比這破爛的手電筒更亮的東西\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                    //UnityEditor.EditorApplication.isPlaying = false;
                                    break;
                                }
                            case 2: 
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnky.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : 2039/05/13 星期一 警衛編號 : 9526\r\n");
                                        writer.Write("第二個禮拜了...這個禮拜有點不尋常?\r\n");
                                        writer.Write("燈開始會自己開開關關的\r\n");
                                        writer.Write("而且不知道是誰沒有把小心地滑告示牌收好\r\n");
                                        writer.Write("放在那邊我也懶得去管它\r\n");
                                        writer.Write("反正公司只叫我巡邏\r\n");
                                        writer.Write("可沒要叫我打掃衛生\r\n");
                                        writer.Write("還有半個月就領薪水了\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                   // UnityEditor.EditorApplication.isPlaying = false;
                                    break;
                                }
                            case 3:
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnkz.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : 2039/05/20 星期一 警衛編號 : 9526\r\n");
                                        writer.Write("媽的 這是甚麼鬼東西\r\n");
                                        writer.Write("怎麼感覺這個雕像在我不注意的時候會自己動起來\r\n");
                                        writer.Write("我記得我沒有移動他阿\r\n");
                                        writer.Write("明明本來在罐頭區怎麼會跑到這來\r\n");
                                        writer.Write("這裡可是跟罐頭區相差十萬八千里的電器區\r\n");
                                        writer.Write("算了可能是我上班上昏頭了\r\n");
                                        writer.Write("下個禮拜就能領到十萬多了再忍忍吧\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                   // UnityEditor.EditorApplication.isPlaying = false;
                                    break;
                                }
                            case 4:
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnk0.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : 2039/05/26 星期日 警衛編號 : 9526\r\n");
                                        writer.Write("不對勁不對勁不對勁\r\n");
                                        writer.Write("這個地方他媽的不對勁\r\n");
                                        writer.Write("我剛進來的確有說過希望有東西把餐飲區照亮\r\n");
                                        writer.Write("但我要的絕對不會是那隻大塊頭\r\n");
                                        writer.Write("媽的這個到底是甚麼鬼地方\r\n");
                                        writer.Write("不行我要是不繼續再這邊上班\r\n");
                                        writer.Write("六月他們來討債的話我可是會沒命的\r\n");
                                        writer.Write("反正橫豎都是死\r\n");
                                        writer.Write("六月他們來討債的話我可是會沒命的\r\n");
                                        writer.Write("賭一把看能不能拿到第一個月的錢就跑路\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                   // UnityEditor.EditorApplication.isPlaying = false;
                                    break;
                                }
                            case 5:
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnk1.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : 2039/06/01 星期五 警衛編號 : 9526\r\n");
                                        writer.Write("...\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                   // UnityEditor.EditorApplication.isPlaying = false;
                                    break;
                                }
                            case 6:
                                {
                                    var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Z3VhcmQgZGlhcnk2.Diary");
                                    using (StreamWriter writer = new StreamWriter(filename))
                                    {
                                        writer.Write("日期 : MjAzOS8wNi8wMw== 5pif5pyf5LqM 警衛編號 : OTUyNg==\r\n");
                                        writer.Write("aHR0cHM6Ly9xcGguY2YyLnF1b3JhY2RuLm5ldC9tYWluLXFpbWctM2FmZDg5MmEyZGI1ODc5ZmQ3NTcyNTU3MzdmYTI5NGUtbHE=\r\n");
                                        writer.Write("aHR0cHM6Ly9lbmNyeXB0ZWQtdGJuMC5nc3RhdGljLmNvbS9pbWFnZXM/cT10Ym46QU5kOUdjU3dGdkpLd0x4a19XbWxucVBuWWxfdER0VlVUdlZBaE5jOVN3JnVzcXA9Q0FV\r\n");
                                        writer.Write("aHR0cHM6Ly9pLnBpbmltZy5jb20vb3JpZ2luYWxzL2JjL2Y1LzQ0L2JjZjU0NDI1YjNiODA0Y2NiZjUzYWFhYzQyNzVkY2RiLmpwZw==\r\n");
                                        writer.Write("aHR0cHM6Ly9yZXMuY2xvdWRpbmFyeS5jb20vamVycmljay9pbWFnZS91cGxvYWQvdjE1MDEyNTYyMzcva212bG40aWZ1Zmx4eGo5NzBmaWIuanBn\r\n");
                                    }
                                    UnityEngine.Application.Quit();
                                   // UnityEditor.EditorApplication.isPlaying = false;
                                    break;

                                }
                        }
                            // UnityEditor.EditorApplication.isPlaying = false;
                            break;
                        }
                    case 4:
                        {
                            ProcessStartInfo proc2 = new ProcessStartInfo("cmd.exe");
                            proc2.Verb = "runas";
                            proc2.UseShellExecute = false;
                            proc2.RedirectStandardOutput = true;
                            proc2.CreateNoWindow = true;
                            proc2.RedirectStandardInput = true;
                            var process = Process.Start(proc2);
                            process.StandardInput.WriteLine(@"start secret_trigger2.vbs");
                            UnityEngine.Application.Quit();
                           //  UnityEditor.EditorApplication.isPlaying = false;
                            break;

                        }
                }
            
  
        }
        if (distance >= warnline)
        {
            warning = false;
            if (material.GetFloat("_alpha") > 0)
            {
                material.SetFloat("_alpha", (float)radia);

            }
            if (material.GetFloat("_alpha") < 0)
            {
                material.SetFloat("_alpha", 0);
            }
            else show = false;
        }


        if (NewBehaviourScript.Instance.warning1 == true && NewBehaviourScript.Instance.warning2 == true && NewBehaviourScript.Instance.warning3 == true && NewBehaviourScript.Instance.warning4 == true && NewBehaviourScript.Instance.warning5 == true)
        {
            gameObject.transform.position = new Vector3(13.483f, 0f, 0.312f);
            onend = true;
        }
        
    }
    //end update function


    


    public void p5pathing()
    {
        if (warning == false)
        {
            if (RandomPoint(centrePoint.position, range, out point) == true)
            {
                UnityEngine.Debug.DrawRay(point, Vector3.up, UnityEngine.Color.blue, 1.0f); ;
                onend = false;
            }
        }

    }

    public void radiation()
    {
        if (warning == true)
        {
            time=0f;
            audioSource.PlayOneShot(SE);
            volume=Mathf.Lerp(volume,1,(float)radia*0.1f);
            audioSource.volume=volume;
            radia = radia + 0.002;
            
            if (radia >= 1.08)
            {
                show = true;
            }
        }
        else
        {
            if(time<fadetime)
            {
                volume=Mathf.Lerp(volume,0,time/fadetime);
                audioSource.volume=volume;
                time+=Time.deltaTime;
            }

            if (radia != 0)
            {
                if (radia >= 0)
                {
                    radia = radia - 0.02;

                }
                else radia = 0;
            }

        }
    }

        bool RandomPoint(Vector3 center, float range, out Vector3 result)
        {
            Vector3 randomPoint = center + UnityEngine.Random.insideUnitSphere * range;
             Sdistance = Vector3.Distance(randomPoint, player.transform.position);
        if (Sdistance <= 20)
        {
            result = point;
            UnityEngine.Debug.Log("in20m");
            return false;
            
        }
        else
        {
            
                randomPoint.y = 1;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 1f, NavMesh.AllAreas) && Sdistance > 20)
                {
                    result = new Vector3(randomPoint.x,0, randomPoint.z);
                // transform.position = result;
              //  UnityEngine.Debug.Log(randomPoint);
               // UnityEngine.Debug.Log("Notin20m");
                notinrange = false;

                    return true;
                }
            
            

            }

        result = point;
            return false;
        }

    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            material.SetFloat("_alpha", data.P5effect);
        }
    }
    public void savedata(ref Gamedata data)
    {
        data.P5effect = material.GetFloat("_alpha");
    }
}
