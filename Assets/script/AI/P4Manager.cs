using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P4Manager : MonoBehaviour
{
    public bool Debuffing;
    public stamina stamina;
    public float Debufftime, ToxicTime;
    public Lightoff[] lightoff;
    // Start is called before the first frame update
    void Start()
    {
        lightoff = FindObjectsOfType<Lightoff>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Debuffing)
        {
            Debufftime += Time.deltaTime;

            if (Debufftime >= ToxicTime)
            {

                stamina.P4Hit = false;
                foreach (Lightoff item in lightoff)
                {
                    item.toxic = false;
                    item.entry = false;
                    item.over = true;
                    item.backtime = true;
                }
                Debufftime = 0;
                Debuffing = false;
            }
        }
    }
}
