using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.WSA;

public class display : MonoBehaviour
{
    private static display instance;
    public static display Instance
    {
        get
        {
            return instance;
        }
    }
    public Timer1 timer1;
    public Timer2 timer2;
    public Timer3 timer3;
    public Timer4 timer4;
    public Timer5 timer5;
    public STDtimer stdtime1;
    public STDtimer2 stdtime2;
    public STDtimer3 stdtime3;
    public STDtimer4 stdtime4;
    public STDtimer5 stdtime5;
    public time time;
    public NewBehaviourScript newBehaviourScript;
    [SerializeField]
    public Sprite[] digits;
    [SerializeField]
    public Image[] chr;
    int t1last,t1last2,t1lasthour,t1lasthour2,t2last,t2last2,t2lasthour,t2lasthour2,t3last,t3last2,t3lasthour,t3lasthour2,t4last,t4last2,t4lasthour,t4lasthour2,t5last,t5last2,t5lasthour,t5lasthour2;
    // Update is called once per frame



    void Awake()
    {
        instance = this;
    }

    public void Update()
    {
        if (t5last >= 10)
        {
            t5last = t5last - 10;
            t5last2 = t5last2 + 1;
        }
        else if (t5last2 >= 6)
        {
            t5last2 = t5last2 - 6;
            t5lasthour = t5lasthour + 1;
        }
        else if (t5lasthour >= 10)
        {
            t5lasthour = t5lasthour - 10;
            t5lasthour2 = t5lasthour2 + 1;
        }
        if (timer5.tsec >= 0)
        {
            chr[19].sprite = digits[t5last];
            chr[18].sprite = digits[t5last2];
            chr[17].sprite = digits[t5lasthour];
            chr[16].sprite = digits[t5lasthour2];
        }

        if (t4last >= 10)
        {
            t4last = t4last - 10;
            t4last2 = t4last2 + 1;
        }
        else if (t4last2 >= 6)
        {
            t4last2 = t4last2 - 6;
            t4lasthour = t4lasthour + 1;
        }
        else if (t4lasthour >= 10)
        {
            t4lasthour = t4lasthour - 10;
            t4lasthour2 = t4lasthour2 + 1;
        }
        if (timer4.tsec >= 0)
        {
            chr[15].sprite = digits[t4last];
            chr[14].sprite = digits[t4last2];
            chr[13].sprite = digits[t4lasthour];
            chr[12].sprite = digits[t4lasthour2];
        }

        if (t3last >= 10)
        {
            t3last = t3last - 10;
            t3last2 = t3last2 + 1;
        }
        else if (t3last2 >= 6)
        {
            t3last2 = t3last2 - 6;
            t3lasthour = t3lasthour + 1;
        }
        else if (t3lasthour >= 10)
        {
            t3lasthour = t3lasthour - 10;
            t3lasthour2 = t3lasthour2 + 1;
        }
        if (timer3.tsec >= 0)
        {
            chr[11].sprite = digits[t3last];
            chr[10].sprite = digits[t3last2];
            chr[9].sprite = digits[t3lasthour];
            chr[8].sprite = digits[t3lasthour2];
        }

        if (t2last >= 10)
        {
            t2last = t2last - 10;
            t2last2 = t2last2 + 1;
        }
        else if (t2last2 >= 6)
        {
            t2last2 = t2last2 - 6;
            t2lasthour = t2lasthour + 1;
        }
        else if (t2lasthour >= 10)
        {
            t2lasthour = t2lasthour - 10;
            t2lasthour2 = t2lasthour2 + 1;
        }
        if (timer2.tsec >= 0)
        {
            chr[7].sprite = digits[t2last];
            chr[6].sprite = digits[t2last2];
            chr[5].sprite = digits[t2lasthour];
            chr[4].sprite = digits[t2lasthour2];
        }

        if (t1last >= 10)
        {
            t1last = t1last - 10;
            t1last2 = t1last2 + 1;
        }
        else if (t1last2 >= 6)
        {
            t1last2 = t1last2 - 6;
            t1lasthour = t1lasthour + 1;
        }
        else if (t1lasthour >= 10)
        {
            t1lasthour = t1lasthour - 10;
            t1lasthour2 = t1lasthour2 + 1;
        }
        if (timer1.tsec >= 0)
        {
            chr[3].sprite = digits[t1last];
            chr[2].sprite = digits[t1last2];
            chr[1].sprite = digits[t1lasthour];
            chr[0].sprite = digits[t1lasthour2];
        }
    }
    public void Updatecardticktime1() {
        t1last = (timer1.min * 6 + time.min) % 10;
        t1last2 = (timer1.min * 6 + time.min) / 10;
        t1lasthour = (timer1.hour * 6 + time.hour) % 10;
        t1lasthour2 = (timer1.hour * 6 + time.hour) / 10;
        t2last = (stdtime2.summin) % 10;
        t2last2 = (stdtime2.summin) / 10;
        t2lasthour = (stdtime2.sumhour) % 10;
        t2lasthour2 = (stdtime2.sumhour) / 10;
        t3last = (stdtime3.summin) % 10;
        t3last2 = (stdtime3.summin) / 10;
        t3lasthour = (stdtime3.sumhour) % 10;
        t3lasthour2 = (stdtime3.sumhour) / 10;
        t4last = (stdtime4.summin) % 10;
        t4last2 = (stdtime4.summin) / 10;
        t4lasthour = (stdtime4.sumhour) % 10;
        t4lasthour2 = (stdtime4.sumhour) / 10;
        t5last = (stdtime5.summin) % 10;
        t5last2 = (stdtime5.summin) / 10;
        t5lasthour = (stdtime5.sumhour) % 10;
        t5lasthour2 = (stdtime5.sumhour) / 10;

    }
    public void Updatecardticktime2()
    {
        t1last = (stdtime1.summin) % 10;
        t1last2 = (stdtime1.summin) / 10;
        t1lasthour = (stdtime1.sumhour) % 10;
        t1lasthour2 = (stdtime1.sumhour) / 10;
        t2last = (timer2.min * 6 + time.min) % 10;
        t2last2 = (timer2.min * 6 + time.min) / 10;
        t2lasthour = (timer2.hour * 6 + time.hour) % 10;
        t2lasthour2 = (timer2.hour * 6 + time.hour) / 10;
        t3last = (stdtime3.summin) % 10;
        t3last2 = (stdtime3.summin) / 10;
        t3lasthour = (stdtime3.sumhour) % 10;
        t3lasthour2 = (stdtime3.sumhour) / 10;
        t4last = (stdtime4.summin) % 10;
        t4last2 = (stdtime4.summin ) / 10;
        t4lasthour = (stdtime4.sumhour) % 10;
        t4lasthour2 = (stdtime4.sumhour) / 10;
        t5last = (stdtime5.summin) % 10;
        t5last2 = (stdtime5.summin) / 10;
        t5lasthour = (stdtime5.sumhour) % 10;
        t5lasthour2 = (stdtime5.sumhour) / 10;
    }
    public void Updatecardticktime3()
    {
        t1last = (stdtime1.summin) % 10;
        t1last2 = (stdtime1.summin) / 10;
        t1lasthour = (stdtime1.sumhour) % 10;
        t1lasthour2 = (stdtime1.sumhour) / 10;
        t2last = (stdtime2.summin) % 10;
        t2last2 = (stdtime2.summin) / 10;
        t2lasthour = (stdtime2.sumhour) % 10;
        t2lasthour2 = (stdtime2.sumhour) / 10;
        t3last = (timer3.min * 6 + time.min) % 10;
        t3last2 = (timer3.min * 6 + time.min) / 10;
        t3lasthour = (timer3.hour * 6 + time.hour) % 10;
        t3lasthour2 = (timer3.hour * 6 + time.hour) / 10;
        t4last = (stdtime4.summin) % 10;
        t4last2 = (stdtime4.summin) / 10;
        t4lasthour = (stdtime4.sumhour) % 10;
        t4lasthour2 = (stdtime4.sumhour) / 10;
        t5last = (stdtime5.summin) % 10;
        t5last2 = (stdtime5.summin) / 10;
        t5lasthour = (stdtime5.sumhour) % 10;
        t5lasthour2 = (stdtime5.sumhour) / 10;
    }
    public void Updatecardticktime4()
    {
        t1last = (stdtime1.summin) % 10;
        t1last2 = (stdtime1.summin) / 10;
        t1lasthour = (stdtime1.sumhour) % 10;
        t1lasthour2 = (stdtime1.sumhour) / 10;
        t2last = (stdtime2.summin) % 10;
        t2last2 = (stdtime2.summin) / 10;
        t2lasthour = (stdtime2.sumhour) % 10;
        t2lasthour2 = (stdtime2.sumhour) / 10;
        t3last = (stdtime3.summin) % 10;
        t3last2 = (stdtime3.summin) / 10;
        t3lasthour = (stdtime3.sumhour) % 10;
        t3lasthour2 = (stdtime3.sumhour) / 10;
        t4last = (timer4.min * 6 + time.min) % 10;
        t4last2 = (timer4.min * 6 + time.min) / 10;
        t4lasthour = (timer4.hour * 6 + time.hour) % 10;
        t4lasthour2 = (timer4.hour * 6 + time.hour) / 10;
        t5last = (stdtime5.summin) % 10;
        t5last2 = (stdtime5.summin) / 10;
        t5lasthour = (stdtime5.sumhour) % 10;
        t5lasthour2 = (stdtime5.sumhour) / 10;

    }
    public void Updatecardticktime5()
    {
        t1last = (stdtime1.summin) % 10;
        t1last2 = (stdtime1.summin) / 10;
        t1lasthour = (stdtime1.sumhour) % 10;
        t1lasthour2 = (stdtime1.sumhour) / 10;
        t2last = (stdtime2.summin) % 10;
        t2last2 = (stdtime2.summin) / 10;
        t2lasthour = (stdtime2.sumhour) % 10;
        t2lasthour2 = (stdtime2.sumhour) / 10;
        t3last = (stdtime3.summin) % 10;
        t3last2 = (stdtime3.summin) / 10;
        t3lasthour = (stdtime3.sumhour) % 10;
        t3lasthour2 = (stdtime3.sumhour) / 10;
        t4last = (stdtime4.summin) % 10;
        t4last2 = (stdtime4.summin) / 10;
        t4lasthour = (stdtime4.sumhour) % 10;
        t4lasthour2 = (stdtime4.sumhour) / 10;
        t5last = (timer5.min * 6 + time.min) % 10;
        t5last2 = (timer5.min * 6 + time.min) / 10;
        t5lasthour = (timer5.hour * 6 + time.hour) % 10;
        t5lasthour2 = (timer5.hour * 6 + time.hour) / 10;
    }
    public void Updatecardtimeforguard()
    {
        t1last = (stdtime1.summin) % 10;
        t1last2 = (stdtime1.summin) / 10;
        t1lasthour = (stdtime1.sumhour) % 10;
        t1lasthour2 = (stdtime1.sumhour) / 10;
        t2last = (stdtime2.summin) % 10;
        t2last2 = (stdtime2.summin) / 10;
        t2lasthour = (stdtime2.sumhour) % 10;
        t2lasthour2 = (stdtime2.sumhour) / 10;
        t3last = (stdtime3.summin) % 10;
        t3last2 = (stdtime3.summin) / 10;
        t3lasthour = (stdtime3.sumhour) % 10;
        t3lasthour2 = (stdtime3.sumhour) / 10;
        t4last = (stdtime4.summin) % 10;
        t4last2 = (stdtime4.summin) / 10;
        t4lasthour = (stdtime4.sumhour) % 10;
        t4lasthour2 = (stdtime4.sumhour) / 10;
        t5last = (stdtime5.summin) % 10;
        t5last2 = (stdtime5.summin) / 10;
        t5lasthour = (stdtime5.sumhour) % 10;
        t5lasthour2 = (stdtime5.sumhour) / 10;
    }
}
