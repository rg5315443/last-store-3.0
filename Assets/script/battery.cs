using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Unity.Mathematics;
using UnityEngine;

public class battery : MonoBehaviour,IDatapersistence
{
    public GameObject[] batterys;
    public GameObject prefab;
    public int batteryspawn ;
    public float time;
    public bool hadgenerate;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("Turtorial") == 2&& hadgenerate!=true)
        {
            for (int i = 0; i < batteryspawn; i++)
            {
                int num = UnityEngine.Random.Range(0, batterys.Length);
                while (batterys[num].activeSelf == true)
                {
                     num = UnityEngine.Random.Range(0, batterys.Length);
                }
                batterys[num].SetActive(true);
                //Instantiate(prefab, batterys[num].transform.position, batterys[num].transform.rotation);
            }
            hadgenerate = true;
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    /*public void batterysposchange(string batterysname)
    {
        string batteryNAMEIN = batterysname;
        int batterysnumber = Random.Range(0, batterys.Length);

        if (batterys[batterysnumber].name != batterysname&& batterys[batterysnumber].activeSelf == false)
        {
            StartCoroutine(count(batterysnumber));

        }
        else batterysposchange(batteryNAMEIN);
        
    }
    IEnumerator count(int batterysnumber)
    {
        yield return new WaitForSeconds(time);
        Instantiate(prefab, batterys[batterysnumber].transform.position, batterys[batterysnumber].transform.rotation);
       // batterys[batterysnumber].SetActive(true);
    }*/

    public void loaddata(Gamedata data)
    {
        if (data != null)
        {

            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).gameObject.SetActive(data.batteriesactive[i]);
                }
                catch { }
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).position = data.activebatteriespos[i];
                }
                catch { }
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).rotation = data.activebatteriesros[i];
                }
                catch { }
            }
            hadgenerate = data.generatedbatteries;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.batteriesactive = new bool[transform.childCount];
        data.activebatteriespos = new Vector3[transform.childCount];
        data.activebatteriesros = new quaternion[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            data.batteriesactive[i] = transform.GetChild(i).gameObject.activeSelf;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.activebatteriespos[i] = transform.GetChild(i).transform.position;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.activebatteriesros[i] = transform.GetChild(i).transform.rotation;
        }
        data.generatedbatteries = hadgenerate;
    }
}
