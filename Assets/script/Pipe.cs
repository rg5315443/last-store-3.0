using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Pipe : MonoBehaviour
{
    public bool touched;
    public GameObject pipe;
    Rigidbody rb;
    public AudioClip clip;
    public AudioSource audioSource;
    public bool PipeGranted;
    // Start is called before the first frame update
    void Start()
    {
        rb = pipe.GetComponent<Rigidbody>();
        pipe.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (touched && !PipeGranted)
        {
            FinalScorecaculator.CaculatingScore += 1000;
            Debug.LogWarning("Add Pipe");
            PipeGranted = true;
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player" && touched != true)
        {
            pipe.SetActive(true);
            rb.AddForce(-pipe.transform.up*500);
            audioSource.PlayOneShot(clip);
            touched = true;
        }
        /*if (other.transform.tag == "Player"&&touched!=true) {
                gameObject.GetComponent<PlayableDirector>().enabled = true;
                touched = true;
        }*/
            // Do stuff
    }
}
