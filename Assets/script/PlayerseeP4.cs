using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PlayerseeP4 : MonoBehaviour
{
    
    public Transform campos;
    public Transform P3trans,Lpoint,Rpoint;
    public float angle;
    public p3ai P3Ai;
    public Animator P3animator;
    public bool ischange;
    bool bewatch;
    public RaycastHit hit;
    public float radius;
    public LayerMask P3Layer;
    public Vector3 boxridius;
    void Start()
    {
             
    }

   
    void Update()
    {
        #region raycastbox
        /*
        Vector3 dir = P3trans.position-campos.transform.position;
        if (Vector3.Angle(transform.forward,dir)<(angle/2))
        {
            Quaternion rotation = Quaternion.LookRotation(dir);
            if (!Physics.BoxCast(campos.position, boxridius, dir, rotation, Vector3.Distance(campos.position, P3trans.position), P3Ai.obstruct) && NewBehaviourScript.Instance.hiding == false)
            {
                
                //Debug.DrawLine(campos.position, P4trans.position, Color.blue);
                if (bewatch == false)
                {
                    int Num = UnityEngine.Random.Range(0, 2);
                    P3animator.SetInteger("Idelytpe", Num);
                    P3animator.SetBool("bewatch", true);
                    P3Ai.watched = true;
                    bewatch = true;
                }
                ischange = false;
            }
            else 
            {
                if (P3Ai.onwalkspot == true && ischange == false)
                {
                    ischange = true;
                    P3Ai.onwalkspot = false;
                }
                P3animator.SetBool("bewatch", false);
                bewatch = false;
                P3Ai.watched = false;
                //Debug.DrawLine(campos.position, P4trans.position, Color.yellow);
            }


        }
        else if(Vector3.Angle(transform.forward,dir)>=(angle/2)|| NewBehaviourScript.Instance.hiding == true)
        {
            if(P3Ai.onwalkspot==true&&ischange==false)
            {
                ischange=true;
                P3Ai.onwalkspot=false;
            }
            P3animator.SetBool("bewatch", false);
            bewatch = false;
            P3Ai.watched=false;
        }
        */
        #endregion
       
        Vector3 dir = P3trans.position - campos.transform.position;
        Vector3 Rdir = Rpoint.position - campos.transform.position;
        Vector3 Ldir = Lpoint.position - campos.transform.position;
        if (Vector3.Angle(transform.forward, dir) < (angle / 2))
        {
          
            if (!Physics.Raycast(campos.position, dir, out hit, Vector3.Distance(campos.position, P3trans.position), P3Ai.obstruct) && NewBehaviourScript.Instance.hiding == false)
            {

                //Debug.DrawLine(campos.position, P4trans.position, Color.blue);
                if (bewatch == false)
                {
                    int Num = UnityEngine.Random.Range(0, 2);
                    P3animator.SetInteger("Idelytpe", Num);
                    P3animator.SetBool("bewatch", true);
                    P3Ai.watched = true;
                    bewatch = true;
                }
                ischange = false;
            }

        }
        else if (Vector3.Angle(transform.forward, dir) >= (angle / 2) || NewBehaviourScript.Instance.hiding == true)
        {
            if (P3Ai.onwalkspot == true && ischange == false)
            {
                ischange = true;
                P3Ai.onwalkspot = false;
            }
            P3animator.SetBool("bewatch", false);
            bewatch = false;
            P3Ai.watched = false;
        }

    }

    /*  public void OnBecameVisible()
      { 

      }*/
}
