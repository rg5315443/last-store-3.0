﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.ProBuilder;
public class Randomspawn : MonoBehaviour, IDatapersistence
{
    public GameObject sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, item1;
    public GameObject sp9, sp10, sp11, sp12, sp13, sp14, sp15, sp16, item2;
    public GameObject sp17, sp18, sp19, sp20, sp21, sp22, sp23, sp24, item3;
    public GameObject sp25, sp26, sp27, sp28, sp29, sp30, sp31, sp32, item4;
    public GameObject sp33, sp34, sp35, sp36, sp37, sp38, sp39, sp40, item5;
    public GameObject sp41, sp42, sp43, sp44, sp45, sp46, sp47, sp48, item6;
    public GameObject sp49, sp50, sp51, sp52, sp53, sp54, sp55, sp56, item7;
    public GameObject sp57, sp58, sp59, sp60, sp61, sp62, sp63, sp64, item8;
    int a, b, c, d, e, f, g, h;
    bool alreadysp;
    private void Start()
    {
        if (alreadysp != true)
        {
            randomsp1();
            randomsp2();
            randomsp3();
            randomsp4();
            randomsp5();
            randomsp6();
            randomsp7();
            randomsp8();
            alreadysp = true;
        }
    }




    void randomsp1() //警衛室
    {
        a = Random.Range(1, 9);
        switch (a)
        {
            case 1:
                item1.transform.position = sp1.transform.position;
                item1.transform.rotation = sp1.transform.rotation;
                break;
            case 2:
                item1.transform.position = sp2.transform.position;
                item1.transform.rotation = sp2.transform.rotation;
                break;
            case 3:
                item1.transform.position = sp3.transform.position;
                item1.transform.rotation = sp3.transform.rotation;
                break;
            case 4:
                item1.transform.position = sp4.transform.position;
                item1.transform.rotation = sp4.transform.rotation;
                break;
            case 5:
                item1.transform.position = sp5.transform.position;
                item1.transform.rotation = sp5.transform.rotation;
                break;
            case 6:
                item1.transform.position = sp6.transform.position;
                item1.transform.rotation = sp6.transform.rotation;
                break;
            case 7:
                item1.transform.position = sp7.transform.position;
                item1.transform.rotation = sp7.transform.rotation;
                break;
            case 8:
                item1.transform.position = sp8.transform.position;
                item1.transform.rotation = sp8.transform.rotation;
                break;
        }
    }
    void randomsp2() //警衛室
    {
        b = Random.Range(1, 9);
        switch (b)
        {
            case 1:
                item2.transform.position = sp9.transform.position;
                item2.transform.rotation = sp9.transform.rotation;
                break;
            case 2:
                item2.transform.position = sp10.transform.position;
                item2.transform.rotation = sp10.transform.rotation;
                break;
            case 3:
                item2.transform.position = sp11.transform.position;
                item2.transform.rotation = sp11.transform.rotation;
                break;
            case 4:
                item2.transform.position = sp12.transform.position;
                item2.transform.rotation = sp12.transform.rotation;
                break;
            case 5:
                item2.transform.position = sp13.transform.position;
                item2.transform.rotation = sp13.transform.rotation;
                break;
            case 6:
                item2.transform.position = sp14.transform.position;
                item2.transform.rotation = sp14.transform.rotation;
                break;
            case 7:
                item2.transform.position = sp15.transform.position;
                item2.transform.rotation = sp15.transform.rotation;
                break;
            case 8:
                item2.transform.position = sp16.transform.position;
                item2.transform.rotation = sp16.transform.rotation;
                break;
        }
    }
    void randomsp3() //警衛室
    {
        c = Random.Range(1, 9);
        switch (c)
        {
            case 1:
                item3.transform.position = sp17.transform.position;
                item3.transform.rotation = sp17.transform.rotation;
                break;
            case 2:
                item3.transform.position = sp18.transform.position;
                item3.transform.rotation = sp18.transform.rotation;
                break;
            case 3:
                item3.transform.position = sp19.transform.position;
                item3.transform.rotation = sp19.transform.rotation;
                break;
            case 4:
                item3.transform.position = sp20.transform.position;
                item3.transform.rotation = sp20.transform.rotation;
                break;
            case 5:
                item3.transform.position = sp21.transform.position;
                item3.transform.rotation = sp21.transform.rotation;
                break;
            case 6:
                item3.transform.position = sp22.transform.position;
                item3.transform.rotation = sp22.transform.rotation;
                break;
            case 7:
                item3.transform.position = sp23.transform.position;
                item3.transform.rotation = sp23.transform.rotation;
                break;
            case 8:
                item3.transform.position = sp24.transform.position;
                item3.transform.rotation = sp24.transform.rotation;
                break;
        }
    }
    void randomsp4() //警衛室
    {
        d = Random.Range(1, 9);
        switch (d)
        {
            case 1:
                item4.transform.position = sp25.transform.position;
                item4.transform.rotation = sp25.transform.rotation;
                break;
            case 2:
                item4.transform.position = sp26.transform.position;
                item4.transform.rotation = sp26.transform.rotation;
                break;
            case 3:
                item4.transform.position = sp27.transform.position;
                item4.transform.rotation = sp27.transform.rotation;
                break;
            case 4:
                item4.transform.position = sp28.transform.position;
                item4.transform.rotation = sp28.transform.rotation;
                break;
            case 5:
                item4.transform.position = sp29.transform.position;
                item4.transform.rotation = sp29.transform.rotation;
                break;
            case 6:
                item4.transform.position = sp30.transform.position;
                item4.transform.rotation = sp30.transform.rotation;
                break;
            case 7:
                item4.transform.position = sp31.transform.position;
                item4.transform.rotation = sp31.transform.rotation;
                break;
            case 8:
                item4.transform.position = sp32.transform.position;
                item4.transform.rotation = sp32.transform.rotation;
                break;
        }
    }
    void randomsp5() //警衛室
    {
        e = Random.Range(1, 9);
        switch (e)
        {
            case 1:
                item5.transform.position = sp33.transform.position;
                item5.transform.rotation = sp33.transform.rotation;
                break;
            case 2:
                item5.transform.position = sp34.transform.position;
                item5.transform.rotation = sp34.transform.rotation;
                break;
            case 3:
                item5.transform.position = sp35.transform.position;
                item5.transform.rotation = sp35.transform.rotation;
                break;
            case 4:
                item5.transform.position = sp36.transform.position;
                item5.transform.rotation = sp36.transform.rotation;
                break;
            case 5:
                item5.transform.position = sp37.transform.position;
                item5.transform.rotation = sp37.transform.rotation;
                break;
            case 6:
                item5.transform.position = sp38.transform.position;
                item5.transform.rotation = sp38.transform.rotation;
                break;
            case 7:
                item5.transform.position = sp39.transform.position;
                item5.transform.rotation = sp39.transform.rotation;
                break;
            case 8:
                item5.transform.position = sp40.transform.position;
                item5.transform.rotation = sp40.transform.rotation;
                break;
        }
    }
    void randomsp6() //警衛室
    {
        f = Random.Range(1, 9);
        switch (f)
        {
            case 1:
                item6.transform.position = sp41.transform.position;
                item6.transform.rotation = sp41.transform.rotation;
                break;
            case 2:
                item6.transform.position = sp42.transform.position;
                item6.transform.rotation = sp42.transform.rotation;
                break;
            case 3:
                item6.transform.position = sp43.transform.position;
                item6.transform.rotation = sp43.transform.rotation;
                break;
            case 4:
                item6.transform.position = sp44.transform.position;
                item6.transform.rotation = sp44.transform.rotation;
                break;
            case 5:
                item6.transform.position = sp45.transform.position;
                item6.transform.rotation = sp45.transform.rotation;
                break;
            case 6:
                item6.transform.position = sp46.transform.position;
                item6.transform.rotation = sp46.transform.rotation;
                break;
            case 7:
                item6.transform.position = sp47.transform.position;
                item6.transform.rotation = sp47.transform.rotation;
                break;
            case 8:
                item6.transform.position = sp48.transform.position;
                item6.transform.rotation = sp48.transform.rotation;
                break;
        }
    }
    void randomsp7() //警衛室
    {
        g = Random.Range(1, 9);
        switch (g)
        {
            case 1:
                item7.transform.position = sp49.transform.position;
                item7.transform.rotation = sp49.transform.rotation;
                break;
            case 2:
                item7.transform.position = sp50.transform.position;
                item7.transform.rotation = sp50.transform.rotation;
                break;
            case 3:
                item7.transform.position = sp51.transform.position;
                item7.transform.rotation = sp51.transform.rotation;
                break;
            case 4:
                item7.transform.position = sp52.transform.position;
                item7.transform.rotation = sp52.transform.rotation;
                break;
            case 5:
                item7.transform.position = sp53.transform.position;
                item7.transform.rotation = sp53.transform.rotation;
                break;
            case 6:
                item7.transform.position = sp54.transform.position;
                item7.transform.rotation = sp54.transform.rotation;
                break;
            case 7:
                item7.transform.position = sp55.transform.position;
                item7.transform.rotation = sp55.transform.rotation;
                break;
            case 8:
                item7.transform.position = sp56.transform.position;
                item7.transform.rotation = sp56.transform.rotation;
                break;
        }
    }
    void randomsp8() //警衛室
    {
        h = Random.Range(1, 9);
        switch (h)
        {
            case 1:
                item8.transform.position = sp57.transform.position;
                item8.transform.rotation = sp57.transform.rotation;
                break;
            case 2:
                item8.transform.position = sp58.transform.position;
                item8.transform.rotation = sp58.transform.rotation;
                break;
            case 3:
                item8.transform.position = sp59.transform.position;
                item8.transform.rotation = sp59.transform.rotation;
                break;
            case 4:
                item8.transform.position = sp60.transform.position;
                item8.transform.rotation = sp60.transform.rotation;
                break;
            case 5:
                item8.transform.position = sp61.transform.position;
                item8.transform.rotation = sp61.transform.rotation;
                break;
            case 6:
                item8.transform.position = sp62.transform.position;
                item8.transform.rotation = sp62.transform.rotation;
                break;
            case 7:
                item8.transform.position = sp63.transform.position;
                item8.transform.rotation = sp63.transform.rotation;
                break;
            case 8:
                item8.transform.position = sp64.transform.position;
                item8.transform.rotation = sp64.transform.rotation;
                break;
        }
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            this.item1.transform.position = data.item1pos;
            this.item1.transform.rotation = data.item1rot;
            this.item2.transform.position = data.item2pos;
            this.item2.transform.rotation = data.item2rot;
            this.item3.transform.position = data.item3pos;
            this.item3.transform.rotation = data.item3rot;
            this.item4.transform.position = data.item4pos;
            this.item4.transform.rotation = data.item4rot;
            this.item5.transform.position = data.item5pos;
            this.item5.transform.rotation = data.item5rot;
            this.item6.transform.position = data.item6pos;
            this.item6.transform.rotation = data.item6rot;
            this.item7.transform.position = data.item7pos;
            this.item7.transform.rotation = data.item7rot;
            this.item8.transform.position = data.item8pos;
            this.item8.transform.rotation = data.item8rot;
        }
    }
    public void savedata(ref Gamedata data)
    {
        data.item1pos = this.item1.transform.position;
        data.item1rot = this.item1.transform.rotation;
        data.item2pos = this.item2.transform.position;
        data.item2rot = this.item2.transform.rotation;
        data.item3pos = this.item3.transform.position;
        data.item3rot = this.item3.transform.rotation;
        data.item4pos = this.item4.transform.position;
        data.item4rot = this.item4.transform.rotation;
        data.item5pos = this.item5.transform.position;
        data.item5rot = this.item5.transform.rotation;
        data.item6pos = this.item6.transform.position;
        data.item6rot = this.item6.transform.rotation;
        data.item7pos = this.item7.transform.position;
        data.item7rot = this.item7.transform.rotation;
        data.item8pos = this.item8.transform.position;
        data.item8rot = this.item8.transform.rotation;
            }
}
