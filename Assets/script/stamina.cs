using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace StarterAssets
{
    public class stamina : MonoBehaviour, IDatapersistence
    {
        private static stamina instance;
        public static stamina Instance
        {
            get
            {
                return instance;
            }
        }
        public StarterAssetsInputs starterAssetsInputs;
        public float staminavalue, StaminaCostSpeed = 1f;
        public float maxstaminavalue;
        public Slider staminabar;
        public float dvalue;
        public bool P4Hit,Lame;
        public Animator animator;
        public GameObject cam;
        public float resetttime;
        float time;
        float t;
        public AudioClip cantbreath;
        public AnimationCurve curve;
        AudioSource player;
        void Awake()
        {
            instance = this;
        }
        // Start is called before the first frame update
        void Start()
        {
            TraitRrefrsh();
            player = gameObject.GetComponent<AudioSource>();
            staminabar.maxValue = maxstaminavalue;
            P4Hit=false;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.LeftControl)&&!Lame)
            {
                time += Time.deltaTime;
                if (time >= resetttime)
                {
                    time = 0;
                }
                cam.transform.position = new Vector3(cam.transform.position.x, Mathf.Lerp(cam.transform.position.y, 0.6f, time / resetttime), cam.transform.position.z);
                if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)))
                {
                    animator.SetFloat("state", 3);
                }
                else    
                {
                    animator.SetFloat("state", 0);
                }
            }
            else if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)) && (!Input.GetKey(KeyCode.LeftShift) || staminavalue <= 0) && (!Input.GetKey(KeyCode.LeftControl)))
            {
                cam.transform.position = new Vector3(cam.transform.position.x, 1.573f, cam.transform.position.z);
                animator.SetFloat("state", 1);
            }
            else
            {
                animator.SetFloat("state", 0);
                cam.transform.position = new Vector3(cam.transform.position.x, 1.573f, cam.transform.position.z);
            } 


            if (((Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.D)) && !Input.GetKey(KeyCode.LeftControl))&& !Lame)
            {
                DecreaseStamina();
            }
            else if (staminavalue != maxstaminavalue && P4Hit == false)
                IncreaseStamina();
            staminabar.value = staminavalue;

            if (staminavalue <= 0) {
                starterAssetsInputs.sprint = false;
            }
            player.volume = curve.Evaluate((maxstaminavalue - staminavalue + 0.00001f) / 15f);
            //Debug.Log(staminavalue);
        }
        private void DecreaseStamina()
        {
           if(staminavalue > 0)
          animator.SetFloat("state", 2);
            if (staminavalue != 0)
            {
                
                staminavalue -= StaminaCostSpeed * Time.deltaTime;
                if (staminavalue <= -1)
                    staminavalue = 0;
            }
        }
        private void IncreaseStamina()
        {
            staminavalue += 1.5f * Time.deltaTime;
            if (staminavalue >= maxstaminavalue)
                staminavalue = maxstaminavalue;
        }
        void TraitRrefrsh()
        {
            if (TraitManager.Instance.StaminaCostDown)
            {
                float Workspeedcache = StaminaCostSpeed;
                StaminaCostSpeed -= Workspeedcache * TraitManager.Instance.StaminaCostDownPower / 100;
            }
            if (TraitManager.Instance.StaminaCostUp)
            {
                float Workspeedcache = StaminaCostSpeed;
                StaminaCostSpeed += Workspeedcache * TraitManager.Instance.StaminaCostUpPower / 100;
            }

            Lame = TraitManager.Instance.lame;
        }
        public void loaddata(Gamedata data)
        {
            if (data != null)
            {
                this.staminavalue = data.stamina;
            }
        }
        public void savedata(ref Gamedata data)
        {
            data.stamina = this.staminavalue;
        }
    }
}
