using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STEP : MonoBehaviour
{
    
    public AudioClip[] walkclip,runclip;
    public AudioClip roar;
    public AudioSource Audio;
    
    // Start is called before the first frame update
    void Start()
    {
         //Audio=this.gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
        private void Step()
    {
        
        Audio.PlayOneShot(walkclip[Random.Range(0,walkclip.Length)]);
    }
         private void Run()
    {
        
        Audio.PlayOneShot(runclip[Random.Range(0,runclip.Length)]);
    }
    private void Roar()
    {
       
        Audio.PlayOneShot(roar);
    }

}
