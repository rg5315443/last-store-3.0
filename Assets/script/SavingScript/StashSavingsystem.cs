using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class StashSavingsystem : MonoBehaviour,IDatapersistence
{
    // Start is called before the first frame update
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            try
            {
                for (int i = 0; i < data.stashobjs.Length; i++)
                {
                    GameObject childObj = data.stashobjs[i];
                    GameObject parentObj = data.stashobjsparent[i];
                    Vector3 pos = data.stashobjpos[i];
                    Quaternion rot = data.stashobjrot[i];
                    if (childObj != null && parentObj != null)
                    {
                        childObj.transform.SetParent(parentObj.transform);
                        childObj.transform.localPosition = pos;
                        childObj.transform.localRotation = rot;
                        childObj.SetActive(true);
                    }
                }
            }
            catch (Exception e)
            {
              Debug.LogException(e);
            }
                    
        }


    }
    public void savedata(ref Gamedata data)
    {
        data.stashobjs = new GameObject[transform.childCount];
        data.stashobjpos = new Vector3[transform.childCount];
        data.stashobjrot = new quaternion[transform.childCount];
        data.stashobjsparent = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            try 
            {
                data.stashobjs[i] = transform.GetChild(i).GetChild(0).gameObject;
                data.stashobjpos[i] = transform.GetChild(i).GetChild(0).gameObject.transform.localPosition;
                data.stashobjrot[i] = transform.GetChild(i).GetChild(0).gameObject.transform.localRotation;
                data.stashobjsparent[i] = transform.GetChild(i).gameObject;
            }
            catch
            {

            }
        }


    }
}
