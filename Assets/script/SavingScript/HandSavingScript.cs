using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using static UnityEngine.UIElements.UxmlAttributeDescription;

public class HandSavingScript : MonoBehaviour, IDatapersistence
{
    
    public void loaddata(Gamedata data)
    {
        if (data != null) 
        {
        HandSystem.Instance.righthandobj = data.Righthandobj;
        HandSystem.Instance.lefthandobj = data.Lefthandobj;
        if (data.Righthandobjparent!=null)
        {
            HandSystem.Instance.righthandobj.transform.SetParent(data.Righthandobjparent);
            HandSystem.Instance.righthandobj.SetActive(true);
        }
        if (data.Lefthandobjparent != null)
        {
            HandSystem.Instance.lefthandobj.transform.SetParent(data.Lefthandobjparent);
            HandSystem.Instance.lefthandobj.SetActive(true);
        }
        if (data.Righthandobj != null) {
            HandSystem.Instance.righthandobj.transform.localPosition = data.Righthandobjpos;
            HandSystem.Instance.righthandobj.transform.localRotation = data.Righthandobjrot;
            HandSystem.Instance.righthandobj.GetComponentInChildren<Onhandcheck>().Right = data.checkhandright;
            HandSystem.Instance.Rightfull = data.Righthandbool;
            if (HandSystem.Instance.righthandobj.GetComponentInChildren<flashlight>() != null)
            {
                HandSystem.Instance.righthandobj.GetComponentInChildren<flashlight>().currentelectric = data.currentrightele;
            }
        }
            if (data.Lefthandobj != null)
            {
                HandSystem.Instance.lefthandobj.transform.localPosition = data.Lefthandobjpos;
                HandSystem.Instance.lefthandobj.transform.localRotation = data.Lefthandobjrot;
                HandSystem.Instance.lefthandobj.GetComponentInChildren<Onhandcheck>().Left = data.checkhandleft;
                HandSystem.Instance.Leftfull = data.Lefthandbool;
                if (HandSystem.Instance.lefthandobj.GetComponent<flashlight>() != null)
                {
                    HandSystem.Instance.lefthandobj.GetComponentInChildren<flashlight>().currentelectric = data.currentleftele;
                }
            }
        }





        //Flashlight case

    }
    public void savedata(ref Gamedata data)
    {
        if (HandSystem.Instance.righthandobj != null) {
            Debug.Log("Found Right");
            data.Righthandobj = HandSystem.Instance.righthandobj;
            data.Righthandobjpos = HandSystem.Instance.righthandobj.transform.localPosition;
            data.Righthandobjrot = HandSystem.Instance.righthandobj.transform.localRotation;
            data.Righthandobjparent = HandSystem.Instance.righthandobj.transform.parent;
            data.checkhandright = HandSystem.Instance.righthandobj.GetComponentInChildren<Onhandcheck>().Right;
            data.Righthandbool = HandSystem.Instance.Rightfull;
            if (data.Righthandobj.GetComponentInChildren<flashlight>() != null)
            {
                data.currentrightele = HandSystem.Instance.righthandobj.GetComponentInChildren<flashlight>().currentelectric;
            }
        }
        if (HandSystem.Instance.lefthandobj != null) {
            Debug.Log("Find Left");
            data.Lefthandobj = HandSystem.Instance.lefthandobj;
            data.Lefthandobjpos = HandSystem.Instance.lefthandobj.transform.localPosition;
            data.Lefthandobjrot = HandSystem.Instance.lefthandobj.transform.localRotation;
            data.Lefthandobjparent = HandSystem.Instance.lefthandobj.transform.parent;
            data.checkhandleft = HandSystem.Instance.lefthandobj.GetComponentInChildren<Onhandcheck>().Left;
            data.Lefthandbool = HandSystem.Instance.Leftfull;

            if (data.Lefthandobj.GetComponentInChildren<flashlight>() != null)
            {
                data.currentleftele = HandSystem.Instance.lefthandobj.GetComponentInChildren<flashlight>().currentelectric;
            }
        }

        //Flashlight case


    }
}
