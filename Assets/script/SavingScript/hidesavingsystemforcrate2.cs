using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class hidesavingsystemforcrate2 : MonoBehaviour,IDatapersistence
{
    // Start is called before the first frame update
    public void loaddata(Gamedata data)
    {
        int index = 0;
        if (data != null)
        {
            for (int a = 0; a < 2; a++)
            {
                Transform dropPoint = transform.GetChild(a + 3);
                Debug.Log(dropPoint.childCount);
                for (int b = 0; b < data.hidedropobjs2.Length; b++)
                {
                    try
                    {
                        GameObject childgameobj;
                        childgameobj = data.hidedropobjs2[index];
                        childgameobj.SetActive(true);
                        childgameobj.transform.SetParent(data.hidedropparent2[index]);
                        childgameobj.transform.position = data.hidedroponjspos2[index];
                        childgameobj.transform.rotation = data.hidedroprot2[index];
                        index++;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }



    public void savedata(ref Gamedata data)
    {
        int totalChildren = transform.childCount * 10;
        data.hidedropobjs2 = new GameObject[totalChildren];
        data.hidedroponjspos2 = new Vector3[totalChildren];
        data.hidedroprot2 = new quaternion[totalChildren];
        data.hidedropparent2 = new Transform[totalChildren];

        int index = 0;
        for (int a = 0; a < 2; a++)
        {
            Transform dropPoint = transform.GetChild(a + 3);
            for (int b = 0; b < dropPoint.childCount; b++)
            {
                try
                {
                    Transform child = dropPoint.GetChild(b);
                    data.hidedropobjs2[index] = child.gameObject;
                    data.hidedroponjspos2[index] = child.position;
                    data.hidedroprot2[index] = child.rotation;
                    data.hidedropparent2[index] = child.parent;
                    index++;
                }
                catch
                {
                }
            }
        }
    }
}
