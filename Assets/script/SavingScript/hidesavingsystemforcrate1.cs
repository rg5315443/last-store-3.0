using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class hidesavingscript : MonoBehaviour,IDatapersistence
{
    public void loaddata(Gamedata data)
    {
        int index = 0;
        if (data != null)
        {
            for (int a = 0; a < 2; a++)
            {
                Transform dropPoint = transform.GetChild(a + 3);
                for (int b = 0; b < data.hidedropobjs.Length; b++)
                {
                    try
                    {
                        GameObject childgameobj;
                        childgameobj = data.hidedropobjs[index];
                        childgameobj.SetActive(true);
                        childgameobj.transform.SetParent(data.hidedropparent[index]);
                        childgameobj.transform.position = data.hidedroponjspos[index];
                        childgameobj.transform.rotation = data.hidedroprot[index];
                        index++;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }



    public void savedata(ref Gamedata data)
    {
        int totalChildren = transform.childCount * 10;
        data.hidedropobjs = new GameObject[totalChildren];
        data.hidedroponjspos = new Vector3[totalChildren];
        data.hidedroprot = new quaternion[totalChildren];
        data.hidedropparent = new Transform[totalChildren];

        int index = 0;
        for (int a = 0; a < 2; a++)
        {
            Transform dropPoint = transform.GetChild(a + 3);
            for (int b = 0; b < dropPoint.childCount; b++)
            {
                try
                {
                    Transform child = dropPoint.GetChild(b);
                    data.hidedropobjs[index] = child.gameObject;
                    data.hidedroponjspos[index] = child.position;
                    data.hidedroprot[index] = child.rotation;
                    data.hidedropparent[index] = child.parent;
                    index++;
                }
                catch
                {
                }
            }
        }
    }
}
