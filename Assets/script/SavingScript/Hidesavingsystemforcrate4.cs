using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Hidesavingsystemforcrate4 : MonoBehaviour,IDatapersistence
{
    // Start is called before the first frame update
    public void loaddata(Gamedata data)
    {
        int index = 0;
        if (data != null)
        {
            for (int a = 0; a < 2; a++)
            {
                Transform dropPoint = transform.GetChild(a + 5);
                for (int b = 0; b < data.hidedropobjs4.Length; b++)
                {
                    try
                    {
                        GameObject childgameobj;
                        childgameobj = data.hidedropobjs4[index];
                        childgameobj.SetActive(true);
                        childgameobj.transform.SetParent(data.hidedropparent4[index]);
                        childgameobj.transform.position = data.hidedroponjspos4[index];
                        childgameobj.transform.rotation = data.hidedroprot4[index];
                        index++;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }



    public void savedata(ref Gamedata data)
    {
        int totalChildren = transform.childCount * 10;
        data.hidedropobjs4 = new GameObject[totalChildren];
        data.hidedroponjspos4 = new Vector3[totalChildren];
        data.hidedroprot4 = new quaternion[totalChildren];
        data.hidedropparent4 = new Transform[totalChildren];
        int index = 0;
            for (int a = 0; a < 2; a++)
            {
                Transform dropPoint = transform.GetChild(a + 3);
                for (int b = 0; b < dropPoint.childCount; b++)
                {
                    try
                    {
                        Transform child = dropPoint.GetChild(b);
                        data.hidedropobjs4[index] = child.gameObject;
                        data.hidedroponjspos4[index] = child.position;
                        data.hidedroprot4[index] = child.rotation;
                        data.hidedropparent4[index] = child.parent;
                        Debug.Log(child.gameObject);
                        index++;
                    }
                    catch
                    {
                    }
                }
            }
    }

}
