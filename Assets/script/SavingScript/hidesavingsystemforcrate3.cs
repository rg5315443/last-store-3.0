using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class hidesavingsystemforcrate3 : MonoBehaviour,IDatapersistence
{
    // Start is called before the first frame update
    public void loaddata(Gamedata data)
    {
        int index = 0;
        if (data != null)
        {
            for (int a = 0; a < 2; a++)
            {
                Transform dropPoint = transform.GetChild(a + 3);
                for (int b = 0; b < data.hidedropobjs3.Length; b++)
                {
                    try
                    {
                        GameObject childgameobj;
                        childgameobj = data.hidedropobjs3[index];
                        childgameobj.SetActive(true);
                        childgameobj.transform.SetParent(data.hidedropparent3[index]);
                        childgameobj.transform.position = data.hidedroponjspos3[index];
                        childgameobj.transform.rotation = data.hidedroprot3[index];
                        index++;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }



    public void savedata(ref Gamedata data)
    {
        int totalChildren = transform.childCount * 10;
        data.hidedropobjs3 = new GameObject[totalChildren];
        data.hidedroponjspos3 = new Vector3[totalChildren];
        data.hidedroprot3 = new quaternion[totalChildren];
        data.hidedropparent3 = new Transform[totalChildren];

        int index = 0;
        for (int a = 0; a < 2; a++)
        {
            Transform dropPoint = transform.GetChild(a + 3);
            for (int b = 0; b < dropPoint.childCount; b++)
            {
                try
                {
                    Transform child = dropPoint.GetChild(b);
                    data.hidedropobjs3[index] = child.gameObject;
                    data.hidedroponjspos3[index] = child.position;
                    data.hidedroprot3[index] = child.rotation;
                    data.hidedropparent3[index] = child.parent;
                    index++;
                }
                catch
                {
                }
            }
        }
    }
}
