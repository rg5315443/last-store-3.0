using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class OnGroundItemSavingSystem : MonoBehaviour,IDatapersistence
{
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            for (int i = 0; i < data.OnGroundItems.Length; i++)
            {
                try
                {
                    data.OnGroundItems[i].transform.SetParent(transform);
                    data.OnGroundItems[i].SetActive(true);
                }
                catch { }
            }
            for (int i = 0; i < data.OnGroundItemPos.Length; i++)
            {
                try
                {
                    transform.GetChild(i).position = data.OnGroundItemPos[i];
                }
                catch { }
            }
            for (int i = 0; i < data.OnGroundItemRot.Length; i++)
            {
                try
                {
                    transform.GetChild(i).rotation = data.OnGroundItemRot[i];
                }
                catch { }
            }
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.OnGroundItems = new GameObject[transform.childCount];
        data.OnGroundItemPos = new Vector3[transform.childCount];
        data.OnGroundItemRot = new quaternion[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            data.OnGroundItems[i] = transform.GetChild(i).gameObject;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.OnGroundItemPos[i] = transform.GetChild(i).transform.position;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.OnGroundItemRot[i] = transform.GetChild(i).transform.rotation;
        }
    }

}
