using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TextBox;

public class TraitSelectScript : MonoBehaviour
{
    public static bool Niceear, Niceeyes, Badear, Badeyes, Onearm, RunSpeedUp, RunSpeedDown, WalkSpeedUp, WalkSpeedDown, WorkSpeedUp, WorkSpeedDown, AttractDown, AttractUp, StaminaCostDown, StaminaCostUp, BatteryCostDown, BatteryCostUp, Frugal, Adrenaline, OCD, Nicevision, lame,ScareAlone, Retreat, RecoverSanityUp, RecoverSanityDown, ReduceSanityDown, ReduceSanityUp, TravelLight, Psychopath, havetrait;
    public GameObject SelectedGoodEyes, SelectedGoodEars, SelectedBadEyes, SelectedBadEars, SelectedOnearm, SelectedRunSpeedUp, SelectedRunSpeedDown, SelectedWalkSpeedUp, SelectedWalkSpeedDown,SelectedWorkSpeedUp,SelectedWorkSpeedDown,SelectedAttractDown,SelectedAttractUp,SelectedStaminaCostDown,SelectedStaminaCostUp,SelectedBatteryCostDown,SelectedBatteryCostUp, SelectedFrugal, SelectedAdrenaline,SelectedOCD,SelectedNicevision,Selectedlame,SelectedScareAlone, SelectedRetreat, SelectedRecoverSanityUp, SelectedRecoverSanityDown, SelectedReduceSanityDown, SelectedReduceSanityUp, SelectedTravelLight, SelectedPsychopath;
    public GameObject GoodEarsButton, GoodEyesButton, BadEarsButton, BadEyesButton, OnearmButton, RunSpeedUpButton, RunSpeedDownButton, WalkSpeedUpButton, WalkSpeedDownButton,WorkSpeedUpButton,WorkSpeedDownButton,AttractDownButton,AttractUpButton,StaminaCostDownButton,StaminaCostUpButton,BatteryCostDownButton,BatteryCostUpButton, FrugalButton, AdrenalineButton,OCDButton,NicevisionButton,lameButton,ScareAloneButton, RetreatButton, RecoverSanityUpButton, RecoverSanityDownButton, ReduceSanityDownButton, ReduceSanityUpButton, TravelLightButton, PsychopathButton;
    public static int TraitPoints;
    public TextMeshProUGUI TraitPointsText;
    public GameObject PointsError,SelectedIndex;
    private static TraitSelectScript instance;
    public static TraitSelectScript Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
    }
    public void OnButtonNiceearClicked() 
    {
        Niceear = true;
        GoodEarsButton.SetActive(false);
        BadEarsButton.SetActive(false);
        SelectedGoodEars.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonNiceeyesClicked()
    {
        Niceeyes = true;
        GoodEyesButton.SetActive(false);
        BadEyesButton.SetActive(false);
        SelectedGoodEyes.SetActive(true);
        TraitPoints -= 5;
    }

    public void OnButtonBadearClicked() 
    {
        Badear = true;
        BadEarsButton.SetActive(false);
        GoodEarsButton.SetActive(false);
        SelectedBadEars.SetActive(true);
        TraitPoints += 5;

    }
    public void OnButtonBadeyesClicked() 
    {
        Badeyes = true;
        BadEyesButton.SetActive(false);
        GoodEyesButton.SetActive(false);
        SelectedBadEyes.SetActive(true);
        TraitPoints += 5;
    }
    public void OnButtonOnearmClicked() 
    {
        Onearm = true;
        OnearmButton.SetActive(false);
        SelectedOnearm.SetActive(true);
        TraitPoints += 10;
    }
    public void OnButtonRunSpeedUpClicked() 
    {
        RunSpeedUp = true;
        RunSpeedUpButton.SetActive(false);
        SelectedRunSpeedUp.SetActive(true);
        TraitPoints -= 3;
    }
    public void OnButtonRunSpeedDownClicked() 
    {
        RunSpeedDown = true;
        RunSpeedDownButton.SetActive(false);
        SelectedRunSpeedDown.SetActive(true);
        TraitPoints += 3;
    }
    public void OnButtonWalkSpeedUpClicked() 
    {
        WalkSpeedUp = true;
        WalkSpeedUpButton.SetActive(false);
        SelectedWalkSpeedUp.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonWalkSpeedDownClicked() 
    {
        WalkSpeedDown = true;
        WalkSpeedDownButton.SetActive(false);
        SelectedWalkSpeedDown.SetActive(true);
        TraitPoints += 6;
    }
    public void OnButtonWorkSpeedUpClicked()
    {
        WorkSpeedUp = true;
        WorkSpeedUpButton.SetActive(false);
        SelectedWorkSpeedUp.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonWorkSpeedDownClicked()
    { 
        WorkSpeedDown = true;
        WorkSpeedDownButton.SetActive(false);
        SelectedWorkSpeedDown.SetActive(true);
        TraitPoints += 3;
    }
    public void OnButtonAttractDownClicked()
    { 
        AttractDown = true;
        AttractDownButton.SetActive(false);
        SelectedAttractDown.SetActive(true);
        TraitPoints -= 2;
    }
    public void OnButtonAttractUpClicked()
    {
        AttractUp = true;
        AttractUpButton.SetActive(false);
        SelectedAttractUp.SetActive(true);
        TraitPoints += 5;
    }
    public void OnButtonStaminaCostDownClicked()
    { 
        StaminaCostDown = true;
        StaminaCostDownButton.SetActive(false);
        SelectedStaminaCostDown.SetActive(true);
        TraitPoints -= 6;
    }
    public void OnButtonStaminaCostUpClicked()
    { 
        StaminaCostUp = true;
        StaminaCostUpButton.SetActive(false);
        SelectedStaminaCostUp.SetActive(true);
        TraitPoints += 6;
    }
    public void OnButtonBatteryCostDownClicked()
    { 
        BatteryCostDown = true;
        BatteryCostDownButton.SetActive(false);
        SelectedBatteryCostDown.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonBatteryCostUpClicked() 
    {
        BatteryCostUp = true;
        BatteryCostUpButton.SetActive(false);
        SelectedBatteryCostUp.SetActive(true);
        TraitPoints += 3;
    }
    public void OnButtonFrugalClicked()
    {
        Frugal = true;
        FrugalButton.SetActive(false);
        SelectedFrugal.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonAdrenalineClicked()
    {
        Adrenaline = true;
        AdrenalineButton.SetActive(false);
        SelectedAdrenaline.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonOCDClicked() 
    {
        OCD = true;
        OCDButton.SetActive(false);
        SelectedOCD.SetActive(true);
        TraitPoints += 5;
    }
    public void OnButtonNicevisionClicked() 
    {
        Nicevision = true;
        NicevisionButton.SetActive(false);
        SelectedNicevision.SetActive(true);
        TraitPoints -= 5;
    }
    public void OnButtonlameClicked()
    {
        lame = true;
        lameButton.SetActive(false);
        Selectedlame.SetActive(true);
        TraitPoints += 6;
    }
    public void OnButtonScareAloneClicked() 
    {
        ScareAlone = true;
        ScareAloneButton.SetActive(false);
        SelectedScareAlone.SetActive(true);
        TraitPoints += 3;
    }
    public void OnButtonRetreatClicked()
    {
        Retreat = true;
        RetreatButton.SetActive(false);
        SelectedRetreat.SetActive(true);
        TraitPoints -= 3;
    }
    public void OnButtonRecoverSanityUpClicked()
    {
        RecoverSanityUp = true;
        RecoverSanityUpButton.SetActive(false);
        SelectedRecoverSanityUp.SetActive(true);
        TraitPoints -= 4;
    }
    public void OnButtonRecoverSanityDownClicked()
    {
        RecoverSanityDown = true;
        RecoverSanityDownButton.SetActive(false);
        SelectedRecoverSanityDown.SetActive(true);
        TraitPoints += 4;
    }
    public void OnButtonReduceSanityDownClicked()
    {
        ReduceSanityDown = true;
        ReduceSanityDownButton.SetActive(false);
        SelectedReduceSanityDown.SetActive(true);
        TraitPoints -= 4;
    }
    public void OnButtonReduceSanityUpClicked()
    {
        ReduceSanityUp = true;
        ReduceSanityUpButton.SetActive(false);
        SelectedReduceSanityUp.SetActive(true);
        TraitPoints += 3;
    }
    public void OnButtonTravelLightClicked()
    {
        TravelLight = true;
        TravelLightButton.SetActive(false);
        SelectedTravelLight.SetActive(true);
        TraitPoints -= 2;
    }
    public void OnButtonPsychopathClicked()
    {
        Psychopath = true;
        PsychopathButton.SetActive(false);
        SelectedPsychopath.SetActive(true);
        TraitPoints -= 4;
    }
    //Cancel Function
    public void OnButtonCancelSelectedNiceEyes() 
    {
        Niceeyes = false;
        GoodEyesButton.SetActive(true);
        BadEyesButton.SetActive(true);
        SelectedGoodEyes.SetActive(false);
        TraitPoints += 5;

    }
    public void OnButtonCancelSelectedNiceEars()
    {
        Niceear = false;
        GoodEarsButton.SetActive(true);
        BadEarsButton.SetActive(true);
        SelectedGoodEars.SetActive(false);
        TraitPoints += 5;

    }
    public void OnButtonCancelSelectedBadEyes()
    {
        Badeyes = false;
        BadEyesButton.SetActive(true);
        GoodEyesButton.SetActive(true);
        SelectedBadEyes.SetActive(false);
        TraitPoints -= 5;

    }
    public void OnButtonCancelSelectedBadEars()
    {
        Badear = false;
        BadEarsButton.SetActive(true);
        GoodEarsButton.SetActive(true);
        SelectedBadEars.SetActive(false);
        TraitPoints -= 5;

    }
    public void OnButtonCancelSelectedOnearm()
    {
        Onearm = false;
        OnearmButton.SetActive(true);
        SelectedOnearm.SetActive(false);
        TraitPoints -= 10;

    }
    public void OnButtonCancelSelectedRunSpeedUP() {
        RunSpeedUp = false;
        RunSpeedUpButton.SetActive(true);
        SelectedRunSpeedUp.SetActive(false);
        TraitPoints += 3;
    }
    public void OnButtonCancelSelectedRunSpeedDown() {
        RunSpeedDown = false;
        RunSpeedDownButton.SetActive(true);
        SelectedRunSpeedDown.SetActive(false);
        TraitPoints -= 3;
    }
    public void OnButtonCancelSelectedWalkSpeedUp() 
    {
        WalkSpeedUp = false;
        WalkSpeedUpButton.SetActive(true);
        SelectedWalkSpeedUp.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedWalkSpeedDown() 
    {
        WalkSpeedDown = false;
        WalkSpeedDownButton.SetActive(true);
        SelectedWalkSpeedDown.SetActive(false);
        TraitPoints -= 6;
    }
    public void OnButtonCancelSelectedWorkSpeedUp() 
    {
        WorkSpeedUp = false;
        WorkSpeedUpButton.SetActive(true);
        SelectedWorkSpeedUp.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedWorkSpeedDown()
    {
        WorkSpeedDown = false;
        WorkSpeedDownButton.SetActive(true);
        SelectedWorkSpeedDown.SetActive(false);
        TraitPoints -= 3;
    }
    public void OnButtonCancelSelectedAttractDown()
    {
        AttractDown = false;
        AttractDownButton.SetActive(true);
        SelectedAttractDown.SetActive(false);
        TraitPoints += 2;
    }
    public void OnButtonCancelSelectedAttractUp()
    {
        AttractUp = false;
        AttractUpButton.SetActive(true);
        SelectedAttractUp.SetActive(false);
        TraitPoints -= 5;
    }
    public void OnButtonCancelSelectedStaminaCostDown()
    { 
        StaminaCostDown = false;
        StaminaCostDownButton.SetActive(true);
        SelectedStaminaCostDown.SetActive(false);
        TraitPoints += 6;
    }
    public void OnButtonCancelSelectedStaminaCostUp()
    { 
        StaminaCostUp = false;
        StaminaCostUpButton.SetActive(true);
        SelectedStaminaCostUp.SetActive(false);
        TraitPoints -= 6;
    }
    public void OnButtonCancelSelectedBatteryCostDown() 
    {
        BatteryCostDown = false;
        BatteryCostDownButton.SetActive(true);
        SelectedBatteryCostDown.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedBatteryCostUp()
    {
        BatteryCostUp = false;
        BatteryCostUpButton.SetActive(true);
        SelectedBatteryCostUp.SetActive(false);
        TraitPoints -= 3;
    }
    public void OnButtonCancelSelectedFrugal()
    {
        Frugal = false;
        FrugalButton.SetActive(true);
        SelectedFrugal.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedAdrenaline() 
    {
        Adrenaline = false;
        AdrenalineButton.SetActive(true);
        SelectedAdrenaline.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedOCD() 
    {
        OCD = false;
        OCDButton.SetActive(true);
        SelectedOCD.SetActive(false);
        TraitPoints -= 5;
    }
    public void OnButtonCancelSelectedNicevision() 
    {
        Nicevision = false;
        NicevisionButton.SetActive(true);
        SelectedNicevision.SetActive(false);
        TraitPoints += 5;
    }
    public void OnButtonCancelSelectedlame()
    { 
        lame = false;
        lameButton.SetActive(true);
        Selectedlame.SetActive(false);
        TraitPoints -= 6;
    }

    public void OnButtonCancelSelectedScareAlone() 
    {
        ScareAlone = false;
        ScareAloneButton.SetActive(true);
        SelectedScareAlone.SetActive(false);
        TraitPoints -= 3;
    }
    public void OnButtonCancelSelectedRetreat() 
    {
        Retreat = false;
        RetreatButton.SetActive(true);
        SelectedRetreat.SetActive(false);
        TraitPoints += 3;
    }
    public void OnButtonCancelSelectedRecoverSanityUp()
    {
        RecoverSanityUp = false;
        RecoverSanityUpButton.SetActive(true);
        SelectedRecoverSanityUp.SetActive(false);
        TraitPoints += 4;
    }
    public void OnButtonCancelSelectedRecoverSanityDown()
    {
        RecoverSanityDown = false;
        RecoverSanityDownButton.SetActive(true);
        SelectedRecoverSanityDown.SetActive(false);
        TraitPoints -= 4;
    }
    public void OnButtonCancelSelectedReduceSanityDown()
    {
        ReduceSanityDown = false;
        ReduceSanityDownButton.SetActive(true);
        SelectedReduceSanityDown.SetActive(false);
        TraitPoints += 4;
    }
    public void OnButtonCancelSelectedReduceSanityUp()
    {
        ReduceSanityUp = false;
        ReduceSanityUpButton.SetActive(true);
        SelectedReduceSanityUp.SetActive(false);
        TraitPoints -= 3;
    }
    public void OnButtonCancelSelectedTravelLight()
    {
        TravelLight = false;
        TravelLightButton.SetActive(true);
        SelectedTravelLight.SetActive(false);
        TraitPoints += 2;
    }
    public void OnButtonCancelSelectedPsychopath()
    {
        Psychopath = false;
        PsychopathButton.SetActive(true);
        SelectedPsychopath.SetActive(false);
        TraitPoints += 4;
    }

    public void OffErrorTab() 
    {
        PointsError.SetActive(false);
    }
    public void Offpanel() {
        if (TraitPoints >= 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            PointsError.SetActive(true);
        }
    }
    bool CheckChildrenActive(GameObject parent)
    {
        foreach (Transform child in parent.transform)
        {
            if (child.gameObject.activeSelf)
            {
                return true;
            }
        }
        return false;
    }

    private void Update()
    {/*
        if (Niceear)
        {
            Debug.Log("goodear");
        }
        if (Niceeyes) 
        {   
            Debug.Log("goodeyes");
        }
        if (Badear) 
        {
            Debug.Log("badear");
        }
        if (Badeyes)
        { 
            Debug.Log("badeyes"); 
        }
        if (Onearm) 
        {
            Debug.Log("poorguy");
        }*/
        TraitPointsText.text = TraitPoints.ToString();
            
            if (CheckChildrenActive(SelectedIndex)) {
                havetrait = true;
            }
            else
            {
                havetrait = false;
            }
        }
    }

