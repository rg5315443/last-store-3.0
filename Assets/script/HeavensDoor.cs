using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class HeavensDoor : MonoBehaviour
{
    private GameObject player;
    RaycastHit hit;
    int door = 0;
    public RawImage raw;
    public GameObject orgdoor;
    // Start is called before the first frame update
    private void Start()
    {
        player = NewBehaviourScript.Instance.transform.parent.parent.gameObject;
        orgdoor.SetActive(false);
}
    private void Update()
    {
        if (Physics.Raycast(NewBehaviourScript.Instance.playercam.transform.position, NewBehaviourScript.Instance.playercam.transform.forward, out hit, NewBehaviourScript.Instance.dis, NewBehaviourScript.Instance.player.layer)) {
            if (Input.GetKeyDown(KeyCode.E)&&hit.transform.tag == "HeavensDoor")
            {
                if (door < 4)
                {
                    door++;
                    CharacterController character = player.GetComponent<CharacterController>();
                    character.enabled = false;
                    player.transform.position = new Vector3(55 , 0, -5);
                    character.enabled = true;

                    player.transform.eulerAngles = new Vector3(0f, 270f, 0f);
                    Debug.Log("door ready");
                    VideoPlayer[] video = FindObjectsOfType<VideoPlayer>();
                    Debug.Log(video[door-1].clip.name);
                    video[door-1].enabled = true;
                    raw.gameObject.SetActive(true);
                    Invoke("killRAW", (float)video[door - 1].length);
                    if (door == 4) {
                     NewBehaviourScript.Instance.lasttimeopendoor = true;
                    }
                    
                }
                else {
                    Debug.Log("Heaven's Door!!!!!");
                    gameObject.SetActive(false);
                    orgdoor.SetActive(true);
                }
            }
           // Debug.Log(hit.transform.tag);

        }
    }
    private void killRAW()
    {
        raw.gameObject.SetActive(false);
    }
}
