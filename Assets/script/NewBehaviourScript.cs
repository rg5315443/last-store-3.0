using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.InputSystem.HID;
using UnityEngine.Playables;
using Cinemachine;
using System;
using UnityEngine.Timeline;
using System.Resources;

using UnityEditor;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms.Impl;
//using UnityEngine.WSA;

public class NewBehaviourScript : MonoBehaviour, IDatapersistence
{
    private static NewBehaviourScript instance;
    public static NewBehaviourScript Instance
    { 
       get
        {
            return instance; 
        }
    }
    public GameObject plane;
    /*Player Object Define*/
    /*--------------------*/
    public GameObject tutorail;
    public Tutorial tutorialbgm;
    public float sans;
    public FirstPersonController playerController;
    public float dis;
    public Camera playercam;
    public GameObject player, dpob;
    public Transform playersoc;
    Vector3 orgpos;
    Quaternion orgrot;
    public GameObject midgamemenu, outsidemenu, setting;
    /*--------------------*/


    /*Player Items*/
    /*--------------------*/

    //Pick Up Easter Items Function DEF
    public bool onWatching;
    public LayerMask ignormask, picklayer;
    GameObject inspected;
    public GameObject brushplane, salaryplane, plugplane, eyesplane, medicalplane, nameboardplane, radioplane,smokeplane;
    public GameObject objcontrol;

    //FlashLight Function DEF
    public Light flashlight;
    public GameObject flashlightOBJ, batterygrid, batteryimg;
    public int battery;
    public bool open;
    public battery batterycontroller;

    //Fuse Function DEF
    public GameObject fuse;
    public FUSEcontroller fUSEcontroller;
    public bool fuseinsight,有保險絲, fuseput, onInsepct , switchinsight;

    //Tablet Function DEF
    public GameObject tablet;
    /*--------------------*/


    /*Game Utility*/
    /*--------------------*/

    //Timer Function DEF
    public STDtimer STDtime;
    public STDtimer2 STDtime2;
    public STDtimer3 STDtime3;
    public STDtimer4 STDtime4;
    public STDtimer5 STDtime5;
    public Timer1 timer;
    public Timer2 timer2;
    public Timer3 timer3;
    public Timer4 timer4;
    public Timer5 timer5;
    public display display;
    public Slider slider;
    public float slidervaule,Workspeed = 0.333f;
    public float OrgWorkSpeed;
    public bool WarrningLv1, WarrningLv2, WarrningLv3, WarrningLv4, WarrningLv5;
    public bool cardtick = false, cardtick2 = false, cardtick3 = false, cardtick4 = false, cardtick5 = false;
    public bool warning1isset = false, warning2isset = false, warning3isset = false, warning4isset = false, warning5isset = false;
    public bool warning1, warning2, warning3, warning4, warning5, endwarning;
    public int warrning;
    public bool GetFiveWarn;

    //Lever Function DEF
    public bool EndRepair, EndRepaired,fixtriggered;
    float mousevalue;
    Quaternion lastmpos, lastmpos2;

    //Crate Function DEF
    public bool sumprocceed, Cankill;
    public IAMDEAD over;
    public GameObject currenthiding;
    public bool p3cratekill, p2cratekill;
    public bool hiding;
    public bool hidingitemdrop;
    public static int HideCount;

    //Door Function DEF P.S including guard room
    DOOR doorscript;
    public 鐵捲門 rollingdoorscript;
    public bool lasttimeopendoor;

    //Light Controll Function DEF
    public 官開燈控制[] lightcontrol;
    public GameObject BLight, CLight, DLight, ELight, FLight;
    public 電力異常 電力異常;
    //public bool elecisrunout;

    //Computer & Menu etc...DEF
    public Material greyout;
    public savetrigger saveplace;
    public setting settings;
    public GameObject blackoutro;
    public float mainaudio;
    public int res;

    //Sound utility 
    public AudioClip[] audioClips;
    public AudioSource audioSource, flashaudio;

    //Turtorial && subtitle
    bool firsttutorial = true;
    public TMP_Text UItext;
    public GameObject Background, Bigbackground;
    public GameObject[] Watcher;
    public int WatcherCount = 0;
    /*--------------------*/


    /*AI Function*/
    public GameObject P3OBJ, P2OBJ;
    public 追擊 P2AI;
    public p3ai p3Ai;

    /*Sanity*/
    public SanManager sanManager;

    /*-------------------*/
    public HandSystem handSystem;
    string Takeobj, Leftchangeobj, Rightchangeobj;
    public static int Goodendcount;
    public Onhandcheck Pad;
    public padfunction padfun;
    public Light VisionLight;
    
    public bool FiveWarnGranted;
    //public GameObject[] warGps;
    private bool HadSaved;
    bool HoloStatus;
    int HoloCount;
    public TMP_Text FoundedItemCount;
    bool CardTickScoreCoolDown;
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        TraitRrefrsh();

        if (TraitSelectScript.Niceear == true)
        {
            Debug.Log("GoodEars!");
        }
        if (TraitSelectScript.Niceeyes == true)
        {
            Debug.Log("GoodEyes!");
        }
        if (TraitSelectScript.Badear == true)
        {
            Debug.Log("PoorEars!");
        }
        if (TraitSelectScript.Badeyes == true)
        {
            Debug.Log("PoorEyes!");
        }
        if (TraitSelectScript.Onearm == true)
        {
            Debug.Log("Poorguy!");
        }


        if (hiding == false)
        {
            Cankill = true;
        }
        Bigbackground.SetActive(false);
        Background.SetActive(false);
        for (int i = 0; i < battery; i++)
        {
            Instantiate(batteryimg, batterygrid.transform);
        }
            
            open = false;
            //flashlight.enabled = false;
            fuseput = true;

            STDtime.updatestdtime();
            STDtime2.updatestdtime();
            STDtime3.updatestdtime();
            STDtime4.updatestdtime();
            STDtime5.updatestdtime();
        

        greyout.SetFloat("_greyout", 0);
        greyout.SetFloat("_orgin", 1);
        mainaudio = PlayerPrefs.GetFloat("mainaudio");
        res = PlayerPrefs.GetInt("resolution");
        settings.Setvolume(mainaudio);
        settings.setresolution(res);
        audioSource = GetComponent<AudioSource>();
        HideCount = 0;
        Goodendcount = 0;
        InvokeRepeating("LivingTimeScoreCheck", 1.5f, 1f);
        // settings.setresolution(res);

    }

    // Update is called once per frame
    [System.Obsolete]
    private void Update()
    {
        if (warrning == 5)
        {
            GetFiveWarn = true;
        }
        if (GetFiveWarn && GOODENDTRIGGER.instance.InEndingScean&&!FiveWarnGranted)
        {
            FinalScorecaculator.CaculatingScore += 1000;
            FiveWarnGranted = true;
        }
        //Debug.Log(FinalScorecaculator.CaculatingScore);
        RaycastHit hi;
       /* if (Physics.Raycast(playercam.transform.position, playercam.transform.forward, out hi, dis))
        { 
         Debug.Log(hi.transform.gameObject.name);
        }*/

            slider.value = slidervaule ;
        if (TraitManager.Instance.OCD)
        {
            if (warrning == 1 && !WarrningLv1)
            {
                Debug.Log("WarrningLv1");
                WarrningLv1 = true;
                WarrningLv1WorkSpeedDown(TraitManager.Instance.OCDLv1Power, WarrningLv1);
            }
            else if (warrning != 1 && WarrningLv1)
            {
                Debug.Log("DesWarrningLv1");
                WarrningLv1 = false;
                WarrningLv1WorkSpeedDown(TraitManager.Instance.OCDLv1Power, WarrningLv1);
            }
            if (warrning == 2 && !WarrningLv2)
            {
                Debug.Log("WarrningLv2");
                WarrningLv2 = true;
                WarrningLv2WorkSpeedDown(TraitManager.Instance.OCDLv2Power, WarrningLv2);
            }
            else if (warrning != 2 && WarrningLv2)
            {
                Debug.Log("DesWarrningLv2");
                WarrningLv2 = false;
                WarrningLv2WorkSpeedDown(TraitManager.Instance.OCDLv2Power, WarrningLv2);
            }
            if (warrning == 3 && !WarrningLv3)
            {
                WarrningLv3 = true;
                WarrningLv3WorkSpeedDown(TraitManager.Instance.OCDLv3Power, WarrningLv3);
            }
            else if (warrning != 3 && WarrningLv3)
            {
                WarrningLv3 = false;
                WarrningLv3WorkSpeedDown(TraitManager.Instance.OCDLv3Power, WarrningLv3);
            }
            if (warrning == 4 && !WarrningLv4)
            {
                WarrningLv4 = true;
                WarrningLv4WorkSpeedDown(TraitManager.Instance.OCDLv4Power, WarrningLv4);
            }
            else if (warrning != 4 && WarrningLv4)
            {
                WarrningLv4 = false;
                WarrningLv4WorkSpeedDown(TraitManager.Instance.OCDLv4Power, WarrningLv4);
            }
            if (warrning == 5 && !WarrningLv5)
            {
                WarrningLv5 = true;
                WarrningLv5WorkSpeedDown(TraitManager.Instance.OCDLv5Power, WarrningLv5);
            }
            else if (warrning !=5 && WarrningLv5)
            {
                WarrningLv5 = false;
                WarrningLv5WorkSpeedDown(TraitManager.Instance.OCDLv5Power, WarrningLv5);
            }

        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            slidervaule = 0;
        }
        display.Updatecardtimeforguard();
        if (!handSystem.selecting)
        {
            UItext.text = "";
            Bigbackground.SetActive(false);
            Background.SetActive(false);
        }
        if (hiding == true)
        {
            if (sanManager.Sanity > 0)
            {
                sanManager.Sanity -= 2 * Time.deltaTime*sanManager.SanityCostRate;
            }
            
            UItext.text = "[E]離開";
        }    

/*
        if (Input.GetKeyDown(KeyCode.Z))
        {
            for (int i = 0; i < lightcontrol.Length; i++)
            {
                lightcontrol[i].isLightOn = false;
                lightcontrol[i].irregular = true;
                電力異常.triggered = false;
                電力異常.異常 = 5;
                電力異常.aud.PlayOneShot(電力異常.audioClip);
            }
            UnityEngine.Debug.Log("OK");*/
            /*if (電力異常.increaseRate != 3)
            {
                電力異常.increaseRate = 3;
            }
            else if (電力異常.increaseRate == 3)
            {
                電力異常.increaseRate = 0;
            }*/
        
        timer.orgtsec = timer.orgsec + timer.orgmin * 60 + timer.orghour * 60 * 60;
        timer2.orgtsec = timer2.orgsec + timer2.orgmin * 60 + timer2.orghour * 60 * 60;
        timer3.orgtsec = timer3.orgsec + timer3.orgmin * 60 + timer3.orghour * 60 * 60;
        timer4.orgtsec = timer4.orgsec + timer4.orgmin * 60 + timer4.orghour * 60 * 60;
        timer5.orgtsec = timer5.orgsec + timer5.orgmin * 60 + timer5.orghour * 60 * 60;



        if (Input.GetKeyDown(KeyCode.E) && hiding == true && currenthiding.GetComponent<PlayableDirector>().time - currenthiding.GetComponent<PlayableDirector>().duration == 0 || SanManager.Lv2 && hiding == true && currenthiding.GetComponent<PlayableDirector>().time - currenthiding.GetComponent<PlayableDirector>().duration == 0) {
            if (p3cratekill == true && p2cratekill != true)
            {
                P3OBJ.SetActive(false);
                over.getover(1.5f);
                currenthiding.transform.parent.parent.GetChild(1).gameObject.active = true;
                currenthiding.GetComponent<PlayableDirector>().enabled = false;
                if (currenthiding.transform.parent.GetChild(3).GetComponent<PlayableDirector>().state != PlayState.Playing)
                {
                    currenthiding.transform.parent.GetChild(3).GetComponent<PlayableDirector>().Stop();
                    currenthiding.transform.parent.GetChild(3).GetComponent<PlayableDirector>().time = 0;
                }
                currenthiding.transform.parent.GetChild(3).GetComponent<PlayableDirector>().enabled = true;
            }
            else if ((p3cratekill != true && p2cratekill == true || p3cratekill == true && p2cratekill == true)&& P2AI.InLight == false)
            {
                over.getover(2.5f);
                P2OBJ.SetActive(false);
                currenthiding.GetComponent<PlayableDirector>().enabled = false;
                StartCoroutine(sump2crate());
                if (currenthiding.transform.parent.GetChild(7).GetComponent<PlayableDirector>().state != PlayState.Playing)
                {
                    Debug.Log("TriggerAnime");
                    currenthiding.transform.parent.GetChild(7).GetComponent<PlayableDirector>().Stop();
                    currenthiding.transform.parent.GetChild(7).GetComponent<PlayableDirector>().time = 0;
                }
                Debug.Log(currenthiding.name);
                currenthiding.transform.parent.GetChild(7).GetComponent<PlayableDirector>().enabled = true;
            }
            else
            {
                hiding = false;
                //flashlightOBJ.SetActive(true);
                Invoke("leavehideanime", (float)currenthiding.transform.parent.GetChild(2).GetComponent<PlayableDirector>().duration);
                
                currenthiding.GetComponent<PlayableDirector>().enabled = false;
                if (currenthiding.transform.parent.GetChild(2).GetComponent<PlayableDirector>().state != PlayState.Playing)
                {
                    currenthiding.transform.parent.GetChild(2).GetComponent<PlayableDirector>().Stop();
                    currenthiding.transform.parent.GetChild(2).GetComponent<PlayableDirector>().time = 0;
                }
                currenthiding.transform.parent.GetChild(2).GetComponent<PlayableDirector>().enabled = true;
                currenthiding.transform.parent.parent.GetChild(5).GetComponent<AudioSource>().enabled = false;
                currenthiding = null;
                hidingitemdrop = false;
            }
            
        }


        Vector3 target = transform.TransformDirection(Vector3.forward);
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        RaycastHit hit;
        UnityEngine.Debug.DrawRay(transform.position, forward, Color.green);
        if (PlayerPrefs.GetInt("Turtorial") == 2)
        {
            if (cardtick == false)
            {
                timer.timerstart();
                warning1 = false;
                cardtick = true;
            }
            if (cardtick2 == false)
            {
                timer2.timerstart();
                warning2 = false;
                cardtick2 = true;
            }
            if (cardtick3 == false)
            {
                timer3.timerstart();
                warning3 = false;
                cardtick3 = true;
            }
            if (cardtick4 == false)
            {
                timer4.timerstart();
                warning4 = false;
                cardtick4 = true;
            }
            if (cardtick5 == false)
            {
                timer5.timerstart();
                warning5 = false;
                cardtick5 = true;
            }
        }
        //手電筒測試
      /* if (Input.GetMouseButtonDown(1) && outsidemenu.active == false && midgamemenu.active == false)
        {
            if (elecisrunout == false)
            {
                open = !open;
            }

            if (open == false)
            {
                flashlight.enabled = false;
                flashaudio.PlayOneShot(audioClips[5]);

            }
            else if (open == true)
            {
                flashlight.enabled = true;
                flashaudio.PlayOneShot(audioClips[4]);
            }
        }
      */
        /*if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (tablet.active == false)
                tablet.SetActive(true);
            else
                StartCoroutine(Turnofftablet());
        }*/
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (saveplace.inforntcom == true)
            {
                if (midgamemenu.active == false && outsidemenu.active == false)
                {
                    midmenu();
                }
            }
            if (midgamemenu.active == false && outsidemenu.active == false)
            {
                outsidemidmenu();
            }
        }

        if (Physics.Raycast(playercam.transform.position, playercam.transform.forward, out hit, dis, picklayer))
        {
           // Debug.Log(hit.transform.name);
            if (hit.transform.tag == "canbepick1" || hit.transform.tag == "canbepick2" || hit.transform.tag == "canbepick3" || hit.transform.tag == "canbepick4" || hit.transform.tag == "canbepick5" || hit.transform.tag == "canbepick6" || hit.transform.tag == "canbepick7" || hit.transform.tag == "canbepick8")
            {
                UItext.text = "[E]拾取";
            }

            if ((Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick1") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick2") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick3") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick4") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick5") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick6") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick7") || (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "canbepick8"))
            {
                audioSource.PlayOneShot(audioClips[0]);
                Goodendcount++;
                UItext.text = "";


                if (hit.transform.tag == "canbepick1")
                {
                    objcontrol.transform.GetChild(7).gameObject.SetActive(true);
                    smokeplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick2")
                {
                    objcontrol.transform.GetChild(6).gameObject.SetActive(true);
                    salaryplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick3")
                {
                    objcontrol.transform.GetChild(5).gameObject.SetActive(true);
                    radioplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem")) 
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick4")
                {
                    objcontrol.transform.GetChild(4).gameObject.SetActive(true);
                    plugplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick5")
                {
                    objcontrol.transform.GetChild(3).gameObject.SetActive(true);
                    eyesplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick6")
                {
                    objcontrol.transform.GetChild(2).gameObject.SetActive(true);
                    medicalplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick7")
                {
                    objcontrol.transform.GetChild(1).gameObject.SetActive(true);
                    nameboardplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                if (hit.transform.tag == "canbepick8")
                {
                    objcontrol.transform.GetChild(0).gameObject.SetActive(true);
                    brushplane.SetActive(false);
                    HoloStatus = true;
                    HoloCount = 0;
                    if (IsInvoking("HoloFoundedItem"))
                    {
                        HoloStatus = false;
                        CancelInvoke("HoloFoundedItem");
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                    else
                    {
                        InvokeRepeating("HoloFoundedItem", 0f, 1f);
                    }
                }
                hit.transform.gameObject.SetActive(false);
            }
        }



            if (Physics.Raycast(playercam.transform.position, playercam.transform.forward, out hit, dis, ~ignormask))
        {
            if (hiding != true && hit.transform.tag == "hide" && currenthiding == null && !padfun.watching && !SanManager.Lv2&&!TraitManager.Instance.ScareAlone)
            {
                UItext.text = "[E]躲藏";
            }
            else if (hit.transform.tag == "hide" && TraitManager.Instance.ScareAlone)
            {
                UItext.text = "你有幽閉恐懼症 你不敢躲進去";
            }
            else if(hit.transform.tag == "hide" && SanManager.Lv2)
            {
                UItext.text = "你嚇瘋了 無法躲入";
            }

            if (hit.transform.tag == "Battery" || hit.transform.tag == "保險絲"|| hit.transform.tag == "P3kind"
                || hit.transform.tag == "Flashlight" || hit.transform.tag == "Pad" || hit.transform.tag == "Stash")
            {
                #region hittag
                if (hit.transform.tag == "Battery")
                {
                    Takeobj = "電池";
                }
                if (hit.transform.tag == "保險絲")
                {
                    Takeobj = "保險絲";
                }
                if (hit.transform.tag == "P3kind")
                {
                    Takeobj = "使徒";
                }
                if (hit.transform.tag == "Flashlight")
                {
                    Takeobj = "手電筒";
                }
                if (hit.transform.tag == "Pad")
                {
                    Takeobj = "平板";
                }
                #endregion

                if ((handSystem.Leftfull || handSystem.Rightfull) && (handSystem.selecting || handSystem.stashselect) && !handSystem.canmove && !handSystem.Invertcanmove && !handSystem.watching)
                {
                    #region handtag
                    if (handSystem.lefthandobj != null )
                    {
                        if (handSystem.lefthandobj.tag == "保險絲")
                        {
                            Leftchangeobj = "保險絲";
                        }
                        else if (handSystem.lefthandobj.tag == "Battery")
                        {
                            Leftchangeobj = "電池";
                        }
                        else if (handSystem.lefthandobj.tag == "P3kind")
                        {
                            Leftchangeobj = "使徒";
                        }
                        else if (handSystem.lefthandobj.tag == "Flashlight")
                        {
                            Leftchangeobj = "手電筒";
                        }
                        else if (handSystem.lefthandobj.tag == "Pad")
                        {
                            Leftchangeobj = "平板";
                        }
                    }

                    if (handSystem.righthandobj != null)
                    {
                        if (handSystem.righthandobj.tag == "保險絲")
                        {
                            Rightchangeobj = "保險絲";
                        }
                        else if (handSystem.righthandobj.tag == "Battery")
                        {
                            Rightchangeobj = "電池";
                        }
                        else if (handSystem.righthandobj.tag == "P3kind")
                        {
                            Rightchangeobj = "使徒";
                        }
                        else if (handSystem.righthandobj.tag == "Flashlight")
                        {
                            Rightchangeobj = "手電筒";
                        }
                        else if (handSystem.righthandobj.tag == "Pad")
                        {
                            Rightchangeobj = "平板";
                        }
                    }
                    #endregion
                    if (handSystem.Leftfull && handSystem.Rightfull)
                    {
                        //Debug.Log(hit.transform.gameObject + "LLLRRR");
                        if (!handSystem.selecting)
                        {
                            if (!handSystem.stashfull)
                            {
                                UItext.text = "按住左鍵存入" + "[" + Leftchangeobj + "]\n" + "按住右鍵存入" + "[" + Rightchangeobj + "]";
                            }
                            else UItext.text = "儲藏櫃已滿";
                        }
                        else UItext.text = "按住左鍵用" + "[" + Leftchangeobj + "]與" + "[" + Takeobj + "]交換\n" + "按住右鍵用" + "[" + Rightchangeobj + "]與" + "[" + Takeobj + "]交換";

                        Bigbackground.SetActive(true);
                        Background.SetActive(false);
                    }
                    else if (handSystem.Leftfull)
                    {
                        // Debug.Log(hit.transform.gameObject + "LLL");
                        if (!handSystem.selecting)
                        {
                            if(!handSystem.stashfull)
                            {
                                UItext.text = "按住左鍵存入" + "[" + Leftchangeobj + "]"; 
                            }
                            else UItext.text = "儲藏櫃已滿";


                        }
                        else
                            UItext.text = "按住左鍵用" + "[" + Leftchangeobj + "]與" + "[" + Takeobj + "]交換\n" + "按下右鍵拾取" + "[" + Takeobj + "]";
                        Bigbackground.SetActive(true);
                        Background.SetActive(false);
                    }
                    else if (handSystem.Rightfull)
                    {
                        //Debug.Log(hit.transform.gameObject + "RRR");
                        if (!handSystem.selecting)
                        {
                            if (!handSystem.stashfull)
                            UItext.text = "按住右鍵存入" + "[" + Rightchangeobj + "]";
                            else UItext.text = "儲藏櫃已滿";
                        }
                        else
                            UItext.text = "按下左鍵拾取"+"["+Takeobj+"]\n"+"按住右鍵用" + "[" + Rightchangeobj + "]與" + "[" + Takeobj + "]交換";
                        Bigbackground.SetActive(true);
                        Background.SetActive(false);
                    }
                }
                else if (!handSystem.Leftfull && !handSystem.Rightfull)
                {
                    if (handSystem.selecting)
                    {
                        UItext.text = "按下左鍵拾取" + "[" + Takeobj + "]\n" + "按下右鍵拾取" + "[" + Takeobj + "]";
                        Bigbackground.SetActive(true);
                    }
                    else
                    {
                        UItext.text = "";
                        Bigbackground.SetActive(false);
                    }


                }               
            }
            /* else if (hit.transform.tag == "Battery" && battery >= 3)
             {
                 UItext.text = "口袋滿了";
             }*/

            /*if (hit.transform.tag == "保險絲" && 有保險絲 == false)
            {
                UItext.text = "[E]拾取";
            } else if (hit.transform.tag == "保險絲"&&有保險絲 == true)
            {
                UItext.text = "口袋滿了";
            }*/

            /*if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "保險絲")
            {
                audioSource.PlayOneShot(audioClips[0]);
                有保險絲 = true;
                UItext.text = "";
                if (PlayerPrefs.GetInt("Turtorial") == 2)
                {
                    //fUSEcontroller.fuseposchange(hit.transform.gameObject.name);
                }
                else if (PlayerPrefs.GetInt("Turtorial") == 1)
                {
                    {
                        tutorialbgm.step3object.SetActive(true);
                    }
                }
                hit.transform.gameObject.SetActive(false);
            }*/
        }
        else
        { 
            UItext.text = "";
           /* Bigbackground.SetActive(false);
            Background.SetActive(false);*/
        }

        if (Physics.Raycast(playercam.transform.position, playercam.transform.forward, out hit, dis, ~ignormask))
        {
            void function()
            {
                inspected = hit.transform.gameObject;
                orgpos = hit.transform.position;
                orgrot = hit.transform.rotation;
                //Debug.Log(hit.transform.rotation);
                StartCoroutine(pick());
            }
            void function2()
            {
                StartCoroutine(drop());
                onInsepct = false;
            }

            if (playerController.enabled == true)
            {
                onWatching = false;
            }

            if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "obj" && onInsepct != true)
            {
                Debug.Log("pick!");
                function();
            }
            if (Input.GetKeyDown(KeyCode.E) && onInsepct == true)
            {
                function2();
            }

            if (hiding != true && Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "hide" && currenthiding == null && !padfun.watching && !SanManager.Lv2 && !TraitManager.Instance.ScareAlone)
            {
                Transform droppoint1, droppoint2;
                droppoint1 = hit.transform.parent.parent.GetChild(3);
                droppoint2 = hit.transform.parent.parent.GetChild(4);
                if (handSystem.lefthandobj != null)
                {
                    hidingitemdrop = true;
                    handSystem.Inverthand = handSystem.lefthandobj;
                    handSystem.Inverttransformpos = droppoint1.position;
                    handSystem.Invertcanmove = true;
                    handSystem.lefthandobj.transform.SetParent(droppoint1);
                    handSystem.Leftfull = false;
                    handSystem.lefthandobj = null;
                    hit.transform.parent.parent.GetChild(5).GetComponent<AudioSource>().enabled = true;
                }
                if (handSystem.righthandobj != null)
                {
                    hidingitemdrop = true;
                    handSystem.secInverthand = handSystem.righthandobj;
                    handSystem.secInverttransformpos = droppoint2.position;
                    handSystem.SecInvertcanmove = true;
                    handSystem.righthandobj.transform.SetParent(droppoint2);
                    handSystem.Rightfull = false;
                    handSystem.righthandobj = null;
                    hit.transform.parent.parent.GetChild(5).GetComponent<AudioSource>().enabled = true;
                }
                /* if (p3cratekill == true)
                 { 
                     P3OBJ.SetActive(false);
                 }*/
                //flashlightOBJ.SetActive(false);
                player.transform.GetComponent<FirstPersonController>().enabled = false;
                PlayableDirector playableDirectorenter;
                playableDirectorenter = hit.transform.gameObject.GetComponent<PlayableDirector>();
                hit.transform.parent.GetChild(2).GetComponent<PlayableDirector>().enabled = false;
                Invoke("hideanime", (float)playableDirectorenter.duration);
                StartCoroutine(hide(playableDirectorenter));
                Debug.Log(playableDirectorenter.state);
                if (playableDirectorenter.state != PlayState.Playing)
                {
                    playableDirectorenter.Stop();
                    playableDirectorenter.time = 0;
                }
                playableDirectorenter.enabled = true;
                HideCount++;
                // Debug.Log("test");
                currenthiding = hit.transform.gameObject;
                /*see Update() 134~138/*
                /* CinemachineTrack track;
                 track = hit.transform.gameObject.GetComponent<CinemachineTrack>();*/

            }


            //tablet.active == false 這段為舊PAD所屬先刪掉
            if ((hit.transform.tag == "Test" || hit.transform.tag == "card2" || hit.transform.tag == "card3" || hit.transform.tag == "card4" || hit.transform.tag == "card5"))
            {
                UItext.text = "按住[E]打卡";
            }
            if (endwarning == false)
            {

                if (Input.GetKey(KeyCode.E) && hit.transform.tag == "Test")
                {
                    slidervaule += Time.deltaTime * Workspeed;
                    //Debug.Log(slidervaule);
                    if (slidervaule >= 1)
                    {
                        /*if (PlayerPrefs.GetInt("Turtorial") == 1 && firsttutorial == true)
                        {
                            tutorialbgm.intro.Stop();

                            tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip5);
                            //tutorialbgm.StopCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip4.length));
                            tutorialbgm.StopAllCoroutines();
                            tutorialbgm.text.text = "只要他上面的時間重置後 打卡就完成了 這份工作主要就是做這件事";
                            tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip5.length));
                            firsttutorial = false;

                            StartCoroutine(play4_2());
                        }*/
                        if (warning1 == true)
                        {
                            warrning--;
                        }
                        AudioSource beep;
                        beep = hit.transform.GetComponent<AudioSource>();
                        beep.PlayOneShot(beep.clip);
                        if (!CardTickScoreCoolDown)
                        {
                            FinalScorecaculator.CaculatingScore += 100;
                        }
                        else if (!IsInvoking("ClearCoolDown"))
                        {
                            Invoke("ClearCoolDown",10f);
                        }
                        CardTickScoreCoolDown = true;
                        timer.cancelinvoke();
                        timer.sec = timer.orgsec;
                        timer.min = timer.orgmin;
                        timer.hour = timer.orghour;
                        STDtime.updatestdtime();
                        display.Instance.Updatecardtimeforguard();
                        display.Updatecardticktime1();
                        warning1 = false;
                        warning1isset = false;
                        lightcontrol[0].ifwarnning = false;

                        // warGps[1].SetActive(false);
                        // CancelInvoke("offdisplaytimer");
                        // Invoke("offdisplaytimer", 8f);
                        GameObject.FindGameObjectWithTag("warning1").transform.GetChild(0).gameObject.SetActive(false);
                        GameObject.FindGameObjectWithTag("warning1").transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        GameObject.FindGameObjectWithTag("warning1").transform.GetChild(0).GetComponent<AudioSource>().Stop();


                        BLight.layer = LayerMask.NameToLayer("Default");
                        // Debug.Log("tru");
                        slidervaule = 0;
                    }
                }

                if (Input.GetKey(KeyCode.E) && hit.transform.tag == "card2")
                {
                    slidervaule += Time.deltaTime * Workspeed;
                    if (slidervaule >= 1)
                    {
                        AudioSource beep;
                        beep = hit.transform.GetComponent<AudioSource>();
                        beep.PlayOneShot(beep.clip);
                        if (!CardTickScoreCoolDown)
                        {
                            FinalScorecaculator.CaculatingScore += 100;
                        }
                        else if (!IsInvoking("ClearCoolDown"))
                        {
                            Invoke("ClearCoolDown", 10f);
                        }
                        CardTickScoreCoolDown = true;
                        timer2.cancelinvoke();
                        timer2.sec = timer2.orgsec;
                        timer2.min = timer2.orgmin;
                        timer2.hour = timer2.orghour;
                        STDtime2.updatestdtime();
                        display.Instance.Updatecardtimeforguard();
                        display.Updatecardticktime2();
                        if (warning2 == true)
                        {
                            warrning--;
                        }
                        warning2 = false;
                        warning2isset = false;
                        lightcontrol[1].ifwarnning = false;
                        // CancelInvoke("offdisplaytimer");
                        //Invoke("offdisplaytimer", 8f);
                        // warGps[0].SetActive(false);
                        GameObject.FindGameObjectWithTag("warning2").transform.GetChild(0).gameObject.SetActive(false);
                        GameObject.FindGameObjectWithTag("warning2").transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        GameObject.FindGameObjectWithTag("warning2").transform.GetChild(0).GetComponent<AudioSource>().Stop();
                        CLight.layer = LayerMask.NameToLayer("Default");
                        // Debug.Log("tru");
                        slidervaule = 0;
                    }
                }
                if (Input.GetKey(KeyCode.E) && hit.transform.tag == "card3")
                {
                    slidervaule += Time.deltaTime * Workspeed;
                    if (slidervaule >= 1)
                    {
                        AudioSource beep;
                        beep = hit.transform.GetComponent<AudioSource>();
                        beep.PlayOneShot(beep.clip);
                        if (!CardTickScoreCoolDown)
                        {
                            FinalScorecaculator.CaculatingScore += 100;
                        }
                        else if (!IsInvoking("ClearCoolDown"))
                        {
                            Invoke("ClearCoolDown", 10f);
                        }
                        CardTickScoreCoolDown = true;
                        timer3.cancelinvoke();
                        timer3.sec = timer3.orgsec;
                        timer3.min = timer3.orgmin;
                        timer3.hour = timer3.orghour;
                        STDtime3.updatestdtime();
                        display.Instance.Updatecardtimeforguard();
                        display.Updatecardticktime3();
                        if (warning3 == true)
                        {
                            warrning--;
                        }

                        warning3 = false;
                        warning3isset = false;
                        lightcontrol[2].ifwarnning = false;
                        //  CancelInvoke("offdisplaytimer");
                        //   Invoke("offdisplaytimer", 8f);
                        GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).gameObject.SetActive(false);
                        GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).GetComponent<AudioSource>().Stop();
                        // Debug.Log("tru");
                        slidervaule = 0;
                    }
                }
                if (Input.GetKey(KeyCode.E) && hit.transform.tag == "card4")
                {
                    slidervaule += Time.deltaTime * Workspeed;
                    if (slidervaule >= 1)
                    {
                        AudioSource beep;
                        beep = hit.transform.GetComponent<AudioSource>();
                        beep.PlayOneShot(beep.clip);
                        if (!CardTickScoreCoolDown)
                        {
                            FinalScorecaculator.CaculatingScore += 100;
                        }
                        else if (!IsInvoking("ClearCoolDown"))
                        {
                            Invoke("ClearCoolDown", 10f);
                        }
                        CardTickScoreCoolDown = true;
                        timer4.cancelinvoke();
                        timer4.sec = timer4.orgsec;
                        timer4.min = timer4.orgmin;
                        timer4.hour = timer4.orghour;
                        STDtime4.updatestdtime();
                        display.Instance.Updatecardtimeforguard();
                        display.Updatecardticktime4();
                        if (warning4 == true)
                        {
                            warrning--;
                        }

                        warning4 = false;
                        warning4isset = false;
                        lightcontrol[3].ifwarnning = false;
                        /*CancelInvoke("offdisplaytimer");
                        Invoke("offdisplaytimer", 8f);*/
                        GameObject.FindGameObjectWithTag("warning4").transform.GetChild(0).gameObject.SetActive(false);
                        GameObject.FindGameObjectWithTag("warning4").transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        GameObject.FindGameObjectWithTag("warning4").transform.GetChild(0).GetComponent<AudioSource>().Stop();
                        // Debug.Log("tru");
                        slidervaule = 0;
                    }
                }
                if (Input.GetKey(KeyCode.E) && hit.transform.tag == "card5")
                {
                    slidervaule += Time.deltaTime * Workspeed;
                    if (slidervaule >= 1)
                    {
                        AudioSource beep;
                        beep = hit.transform.GetComponent<AudioSource>();
                        beep.PlayOneShot(beep.clip);
                        if (!CardTickScoreCoolDown)
                        {
                            FinalScorecaculator.CaculatingScore += 100;
                        }
                        else if (!IsInvoking("ClearCoolDown"))
                        {
                            Invoke("ClearCoolDown", 10f);
                        }
                        CardTickScoreCoolDown = true;
                        timer5.cancelinvoke();
                        timer5.sec = timer5.orgsec;
                        timer5.min = timer5.orgmin;
                        timer5.hour = timer5.orghour;
                        STDtime5.updatestdtime();
                        display.Instance.Updatecardtimeforguard();
                        display.Updatecardticktime5();
                        //CancelInvoke("offdisplaytimer");
                        // Invoke("offdisplaytimer", 8f);
                        if (warning5 == true)
                        {
                            warrning--;
                        }

                        warning5 = false;
                        warning5isset = false;
                        lightcontrol[4].ifwarnning = false;
                        GameObject.FindGameObjectWithTag("warning5").transform.GetChild(0).gameObject.SetActive(false);
                        GameObject.FindGameObjectWithTag("warning5").transform.GetChild(0).GetComponent<Animator>().enabled = false;
                        GameObject.FindGameObjectWithTag("warning5").transform.GetChild(0).GetComponent<AudioSource>().Stop();
                        // Debug.Log("tru");
                        slidervaule = 0;
                    }
                }
                /* if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "recoversta")
                 {
                     P3使徒總控.Instance.P3change(this.gameObject.transform.parent.gameObject.name);
                     hit.transform.gameObject.transform.parent.gameObject.SetActive(false);

                     stamina.Instance.staminavalue = stamina.Instance.maxstaminavalue;

                 }*/
                if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "com")
                {
                    if (FinalScorecaculator.CaculatingScore >= 1000)
                    {
                        FinalScorecaculator.CaculatingScore -= 1000;
                    }
                    else { 
                    
                    }
                    if (!HadSaved)
                    {
                        FinalScorecaculator.CaculatingScore += FinalScorecaculator.Instance.TraitScore;
                        TraitSelectScript.TraitPoints = 0;
                        HadSaved = true;
                    }
                    DataPersistence.instance.save();
                    UnityEngine.Debug.Log("game saved!");
                }

            }
            else
            {
                timer.sec = 1;
                timer.min = 0;
                timer.hour = 0;
                timer2.sec = 1;
                timer2.min = 0;
                timer2.hour = 0;
                timer3.sec = 1;
                timer3.min = 0;
                timer3.hour = 0;
                timer4.sec = 1;
                timer4.min = 0;
                timer4.hour = 0;
                timer5.sec = 1;
                timer5.min = 0;
                timer5.hour = 0;
            }
            if (hit.transform.tag == "com")
            {
                UItext.text = "[E]存檔";
            }

            if (Input.GetMouseButton(0) && hit.transform.tag == "電閘觸發" && fuseput == true && (電力異常.異常 >= 1 || EndRepair))
            {
                lever_system();

            }
            if (hit.transform.tag == "電閘觸發" && fuseput == true && (電力異常.異常 >= 1 || EndRepair))
            {

                UItext.text = "[滑鼠左鍵]往下拖拽";
                switchinsight = true;

            }
            else switchinsight = false;

            if (hit.transform.tag == "fusepos" && 有保險絲 == true && (電力異常.異常 >= 1 || EndRepair))
            {
                fuseinsight = true;
                if (handSystem.Leftfull && handSystem.lefthandobj.CompareTag("保險絲") && handSystem.Rightfull && handSystem.righthandobj.CompareTag("保險絲"))
                {
                    UItext.text = "按下左鍵放入[保險絲]\n" + "按下右鍵放入[保險絲]";
                }
                else if (handSystem.Leftfull && handSystem.lefthandobj.CompareTag("保險絲"))
                {
                    UItext.text = "按下左鍵放入[保險絲]";
                }
                else if (handSystem.righthandobj.CompareTag("保險絲")) UItext.text = "按下右鍵放入[保險絲]";

            }
            else fuseinsight = false;


            if (hit.transform.tag == "鐵捲門" && rollingdoorscript.canuse == true)
            {
                if (Pad.Right || Pad.Left)
                {
                    UItext.text = "[E]互動";
                }
                else UItext.text = "需要[平板]";

            }
            if (hit.transform.tag == "EndDoor" && EndgameironDoor.Firstouch == false && time.Instance.EndGame == true)
            {
                UItext.text = "[E]互動";
            }
            else if (hit.transform.tag == "EndDoor" && EndgameironDoor.Firstouch == false && time.Instance.EndGame == false)
            {
                UItext.text = "還沒到下班時間";
            }
            if (hit.transform.tag == "DOOR")
            {
                doorscript = hit.transform.gameObject.GetComponent<DOOR>();
                if (doorscript.canuse == true)
                {
                    UItext.text = "[E]互動";
                }
                else UItext.text = "";
            }
            if (hit.transform.tag == "Watcher")
            {
                UItext.text = "[E]互動";
            }
            if (hit.transform.tag == "Watcher" && Input.GetKeyDown(KeyCode.E))
            {
                plane.SetActive(true);
                if (WatcherCount < Watcher.Length)
                {
                    WatcherCount++;
                }
                else if (WatcherCount == Watcher.Length) WatcherCount = 1;
                for (int j = 0; j < Watcher.Length; j++)
                {
                    Watcher[j].SetActive(false);
                }
                Watcher[WatcherCount - 1].SetActive(true);

            }
            if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "DOOR")
            {
                if (!GOODENDTRIGGER.instance.InEndingScean)
                {
                    plane.SetActive(false);
                }
                if (!lasttimeopendoor)
                {
                    for (int j = 0; j < Watcher.Length; j++)
                    {
                        WatcherCount = 0;
                        Watcher[j].SetActive(false);
                    }
                }

                doorscript = hit.transform.gameObject.GetComponent<DOOR>();
                doorscript.door();
                UItext.text = "";
            }

            if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "EndDoor" && EndgameironDoor.Firstouch == false && time.Instance.EndGame == true)
            {
                EndgameironDoor.Firstouch = true;
            }
            if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "鐵捲門" && (Pad.Right || Pad.Left))
            {

                if (rollingdoorscript.canuse == true)
                {
                    if (rollingdoorscript.isopen == false)
                    {
                        rollingdoorscript.isopen = true;
                        StartCoroutine(rollingdoorscript.opencount());
                    }
                    else if (rollingdoorscript.isopen == true)
                    {
                        rollingdoorscript.isopen = false;
                        StartCoroutine(rollingdoorscript.closecount());
                    }
                }
                UnityEngine.Debug.Log("sus");
            }

        }
        else
        { 
            switchinsight = false;
            fuseinsight = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            lastmpos.x = 0;
            lastmpos2.x = 0;

        }
        //        UnityEngine.Debug.Log("lastpos is "+ lastmpos);

        //        UnityEngine.Debug.Log("lastpos2 is " + lastmpos2);

        if (warning1 == true && warning1isset == false)
        {
            P2AI.Bgrid.tag = ("warrninggrid");
            //warGps[1].SetActive(true);
            lightcontrol[0].ifwarnning = true;
            warning1isset = true;
            BLight.layer = LayerMask.NameToLayer("warning");
            warrning++;
            UnityEngine.Debug.Log("warning1");
        }
        if (warning2 == true && warning2isset == false)
        {
            P2AI.Cgrid.tag = ("warrninggrid");
            //warGps[0].SetActive(true);
            lightcontrol[1].ifwarnning = true;
            CLight.layer = LayerMask.NameToLayer("warning");
            warning2isset = true;
            warrning++;
            UnityEngine.Debug.Log("warning2");

        }
        if (warning3 == true && warning3isset == false)
        {
            lightcontrol[2].ifwarnning = true;
            DLight.layer = LayerMask.NameToLayer("warning");
            warning3isset = true;
            warrning++;
            UnityEngine.Debug.Log("warning3");

        }
        if (warning4 == true && warning4isset == false)
        {
            lightcontrol[3].ifwarnning = true;
            ELight.layer = LayerMask.NameToLayer("warning");
            warning4isset = true;
            warrning++;
            UnityEngine.Debug.Log("warning4");

        }
        if (warning5 == true && warning5isset == false)
        {
            lightcontrol[4].ifwarnning = true;
            FLight.layer = LayerMask.NameToLayer("warning");
            warning5isset = true;
            warrning++;
            UnityEngine.Debug.Log("warning5");

        }
        if (onInsepct)
        {
            inspected.transform.position = Vector3.Lerp(inspected.transform.position, playersoc.position, 0.2f);
            playersoc.Rotate(new Vector3(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0) * Time.deltaTime * 400f);
        }
        else if (inspected != null)
        {
            inspected.transform.SetParent(null);
            inspected.transform.position = Vector3.Lerp(inspected.transform.position, orgpos, 0.2f);
        }

        IEnumerator pick()
        {
            playerController.enabled = false;
            yield return new WaitForSeconds(0.2f);
            inspected.transform.SetParent(playersoc);
            onInsepct = true;

        }
        IEnumerator drop()
        {
            if (hit.transform.gameObject != null)
            {
                inspected.transform.rotation = orgrot;
                yield return new WaitForSeconds(0.2f);
                playerController.enabled = true;
            }
        }
        //        UnityEngine.Debug.LogError(hit.transform.tag);

    }

    /*音檔處理*/
    /*  IEnumerator play4_2()
   {
      yield return new WaitForSeconds(tutorialbgm.introClip5.length);
       for (int i = 0; i < lightcontrol.Length; i++)
       {
           lightcontrol[i].isLightOn = false;
           lightcontrol[i].irregular = true;
           電力異常.triggered = false;
           電力異常.異常 = 4;

       }
       tutorialbgm.intro.Stop();
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip12);

       yield return new WaitForSeconds(tutorialbgm.introClip6.length - 1f);
       tutorialbgm.intro.Stop();
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip6);
       //tutorialbgm.StopCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip4.length));
       tutorialbgm.StopAllCoroutines();
       tutorialbgm.text.text = " 看來賣場的電力系統又故障了，接下來是第二個工作，恢復電力，你身上有手電筒可以照明";
       tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip6.length));
       yield return new WaitForSeconds(tutorialbgm.introClip6.length);
       //tutorialbgm.intro.Stop();
       tutorialbgm.StopAllCoroutines();
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip7);

       //目前這段依然有跳關問題

       tutorialbgm.text.text = "先回到警衛室，找到替換用的保險絲，然後從前門離開，離開後走到底，抵達電器區後繞過他就可以看到機房的門走進去後直走到底就能看到電盤了 ";
       tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip7.length));
       tutorialbgm.step2 = true;
}*/

    /*IEnumerator playtur6()
    {

       tutorialbgm.intro.Stop();
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip9);
       //tutorialbgm.StopCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip6.length));
       tutorialbgm.StopAllCoroutines();
       tutorialbgm.text.text = "你身上有個平板，可以看打卡機下次打卡的時間，還有賣場的哪個區域供電出現問題，你在每台打卡機打卡後就能看到那一台打卡機下次的打卡時間，回到警衛室跟電腦連線就可以看到所有區域的時間 ";
       tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip9.length));
       yield return new WaitForSeconds(tutorialbgm.introClip9.length);
       tutorialbgm.intro.Stop();
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip10);
       //tutorialbgm.StopCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip6_1.length));
       tutorialbgm.StopAllCoroutines();
       tutorialbgm.text.text = "切記不要讓五個打卡機同時響起，不要在黑暗處久留，不要嘗試去尋找異聲，若發生意外本公司一律不負責";
       tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip10.length));
       yield return new WaitForSeconds(tutorialbgm.introClip10.length);
       tutorialbgm.intro.PlayOneShot(tutorialbgm.introClip11);
       //tutorialbgm.StopCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip6_1.length));
       tutorialbgm.StopAllCoroutines();
       tutorialbgm.text.text = "以上就是本次培訓的所有內容，祝好運";
       tutorialbgm.StartCoroutine(tutorialbgm.waittexttime(tutorialbgm.introClip11.length));
       yield return new WaitForSeconds(tutorialbgm.introClip11.length);
       blackoutro.SetActive(true);
       PlayerPrefs.SetInt("Turtorial", 2);
       yield return new WaitForSeconds(2);
       DataPersistence.instance.newgame();
       SceneManager.LoadSceneAsync("START");

    }
    */
    IEnumerator sump2crate()
    {
        if (sumprocceed != true)
        {
            sumprocceed = true;
            yield return new WaitForSeconds(1.1f);
            currenthiding.transform.parent.parent.GetChild(0).gameObject.SetActive(true);
        }


    }

    public void Puntinfuse()
    {
        if (有保險絲 == true)
        {
            audioSource.PlayOneShot(audioClips[2]);
            UItext.text = "";
            fuse.SetActive(true);
            有保險絲 = false;
            fuseput = true;
        }
    }

    void lever_system()
    {
        UnityEngine.Debug.Log("1s");
        if (Input.GetMouseButton(0))
        {
            
            UnityEngine.Debug.Log("2");
            if (Input.GetAxis("Mouse Y") > 0)
            {
                mousevalue = 0;
            }
            else
            {
                mousevalue = Input.GetAxis("Mouse Y");
            }
            GameObject.FindGameObjectWithTag("電閘").transform.Rotate(0, 0, mousevalue * 5 * 1, Space.Self);
        }

        if (GameObject.FindGameObjectWithTag("電閘").transform.localEulerAngles.z <= 180 && fixtriggered != true)
        {
            UnityEngine.Debug.LogWarning("?");
            if (EndRepair)
            {
                EndRepaired = true;
            }
            audioSource.PlayOneShot(audioClips[1]);
            電力異常.解決 = true;
            電力異常.triggered = false;
            UItext.text = "";
            GameObject.FindGameObjectWithTag("電閘").transform.localEulerAngles = new Vector3(0, 0, 180);
            /*if (PlayerPrefs.GetInt("Turtorial") == 1)
            {
                StartCoroutine(playtur6());
            }*/
            電力異常.FixedErrorLightCount += (int)電力異常.異常;
            if (電力異常.FixedErrorLightCount >= 20 && !電力異常.FixedLightGranted)
            {
                FinalScorecaculator.CaculatingScore += 1000;
                電力異常.FixedLightGranted = true;
            }
            fixtriggered = true;

        }

    }

    public void midmenu()
    {
        playerController.RotationSpeed = 0;
        UnityEngine.Cursor.lockState = CursorLockMode.None;
        UnityEngine.Cursor.visible = true;
        Time.timeScale = 0;
        midgamemenu.SetActive(true);
        greyout.SetFloat("_greyout", 1);
        greyout.SetFloat("_orgin", 0);
    }
    public void outsidemidmenu()
    {
        playerController.RotationSpeed = 0;
        UnityEngine.Cursor.lockState = CursorLockMode.None;
        UnityEngine.Cursor.visible = true;
        Time.timeScale = 0;
        outsidemenu.SetActive(true);
        greyout.SetFloat("_greyout", 1);
        greyout.SetFloat("_orgin", 0);
    }
    public void offmenu()
    {
        Time.timeScale = 1;
        midgamemenu.SetActive(false);
        setting.SetActive(false);
        playerController.RotationSpeed = 1;
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        UnityEngine.Cursor.visible = false;
        greyout.SetFloat("_greyout", 0);
        greyout.SetFloat("_orgin", 1);
    }
    public void offoutsidemenu()
    {
        Time.timeScale = 1;
        outsidemenu.SetActive(false);
        setting.SetActive(false);
        playerController.RotationSpeed = 1;
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        UnityEngine.Cursor.visible = false;
        greyout.SetFloat("_greyout", 0);
        greyout.SetFloat("_orgin", 1);
    }

    public void exitgame()
    {
        UnityEngine.Application.Quit();
    }
    /*IEnumerator Turnofftablet()
    {
        tablet.GetComponent<Animator>().Play("Padoff");
        yield return new WaitForSeconds(1f);
        tablet.SetActive(false);

    }*/
    public void settingmenu()
    {
        setting.active = true;
    }
    public void offsettingmenu()
    {
        setting.active = false;
    }


    IEnumerator hide(PlayableDirector playableDirectorenter)
    {
        player.transform.GetComponent<FirstPersonController>().enabled = false;
        yield return new WaitForSeconds ((float)playableDirectorenter.duration);
        player.transform.GetComponent<CharacterController>().enabled = false;
        //UnityEngine.Debug.Log("hide");
        Cankill = false;
    }
    public void hideanime()
    {
        
        hiding = true;
        
       // player.transform.GetComponent<CharacterController>().enabled = false;

    }
    public void leavehideanime()
    {
        Cankill = true;
        player.transform.GetComponent<FirstPersonController>().enabled = true;
        player.transform.GetComponent<CharacterController>().enabled = true;
    }

    void TraitRrefrsh()
    {
        if (TraitManager.Instance.WorkSpeedUp)
        {
            float Workspeedcache = Workspeed;
            Workspeed += Workspeedcache * TraitManager.Instance.WorkSpeedUpPower/100;
        }
        if (TraitManager.Instance.WorkSpeedDown)
        {
            float Workspeedcache = Workspeed;
            Workspeed -= Workspeedcache * TraitManager.Instance.WorkSpeedDownPower/100;
        }
        OrgWorkSpeed = Workspeed;
    }

    void WarrningLv1WorkSpeedDown(float DebuffSpeed , bool Debuff)
    {
        if (Debuff)
        {
            Workspeed -= OrgWorkSpeed* DebuffSpeed;
        }
        else Workspeed = OrgWorkSpeed;
    }
    void WarrningLv2WorkSpeedDown(float DebuffSpeed, bool Debuff)
    {
        if (Debuff)
        {
            Workspeed -= OrgWorkSpeed * DebuffSpeed;
        }
        else if (!Debuff)
        {
            Workspeed += OrgWorkSpeed * DebuffSpeed;
            Debug.Log("DDDWDW");
        } 
    }
    void WarrningLv3WorkSpeedDown(float DebuffSpeed, bool Debuff)
    {
        if (Debuff)
        {
            Workspeed -= OrgWorkSpeed * DebuffSpeed;
        }
        else if (!Debuff) Workspeed += OrgWorkSpeed * DebuffSpeed;
    }
    void WarrningLv4WorkSpeedDown(float DebuffSpeed, bool Debuff)
    {
        if (Debuff)
        {
            Workspeed -= OrgWorkSpeed * DebuffSpeed;
        }
        else if (!Debuff) Workspeed += OrgWorkSpeed * DebuffSpeed;
    }
    void WarrningLv5WorkSpeedDown(float DebuffSpeed, bool Debuff)
    {
        if (Debuff)
        {
            Workspeed -= OrgWorkSpeed * DebuffSpeed;
        }
        else if (!Debuff) Workspeed += OrgWorkSpeed * DebuffSpeed;
    }
    void LivingTimeScoreCheck() {
        if (!IAMDEAD.GotKilled)
        {
            FinalScorecaculator.CaculatingScore += 1;
        }
    }
    void HoloFoundedItem() 
    {

        if (HoloStatus)
        {
            FoundedItemCount.text = "Founded" + " " + Goodendcount.ToString() + "/8 Items";
            FoundedItemCount.gameObject.SetActive(true);
            HoloStatus = false;
            HoloCount++;
        }
        else 
        {
            FoundedItemCount.gameObject.SetActive(false);
            HoloStatus = true;
        }
        if (HoloCount == 5) 
        {
            FoundedItemCount.gameObject.SetActive(false);
            CancelInvoke("HoloFoundedItem");
        }
    }
    void ClearCoolDown()
    {
        CardTickScoreCoolDown = false;
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            objcontrol.transform.GetChild(7).transform.gameObject.SetActive(data.obj1);
            objcontrol.transform.GetChild(6).transform.gameObject.SetActive(data.obj2);
            objcontrol.transform.GetChild(5).transform.gameObject.SetActive(data.obj3);
            objcontrol.transform.GetChild(4).transform.gameObject.SetActive(data.obj4);
            objcontrol.transform.GetChild(3).transform.gameObject.SetActive(data.obj5);
            objcontrol.transform.GetChild(2).transform.gameObject.SetActive(data.obj6);
            objcontrol.transform.GetChild(1).transform.gameObject.SetActive(data.obj7);
            objcontrol.transform.GetChild(0).transform.gameObject.SetActive(data.obj8);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(7).transform.gameObject.SetActive(data.pick8);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(6).transform.gameObject.SetActive(data.pick7);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(5).transform.gameObject.SetActive(data.pick6);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(4).transform.gameObject.SetActive(data.pick5);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(3).transform.gameObject.SetActive(data.pick4);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(2).transform.gameObject.SetActive(data.pick3);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(1).transform.gameObject.SetActive(data.pick2);
            GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(0).transform.gameObject.SetActive(data.pick1);
            smokeplane.SetActive(data.objplane1);
            salaryplane.SetActive(data.objplane2);
            radioplane.SetActive(data.objplane3);
            plugplane.SetActive(data.objplane4);
            eyesplane.SetActive(data.objplane5);
            medicalplane.SetActive(data.objplane6);
            nameboardplane.SetActive(data.objplane7);
            brushplane.SetActive(data.objplane8);
            fuse.SetActive(data.fuseactive);
            fuseput = data.fusecheckfun;
            TraitSelectScript.Onearm = data.onearmtrait;
            TraitSelectScript.Badear = data.badearstrait;
            TraitSelectScript.Badeyes = data.badeyestrait;
            TraitSelectScript.Niceear = data.goodearstrait;
            TraitSelectScript.Niceeyes = data.goodeyestrait;
            Goodendcount = data.goodcount;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.obj1 = objcontrol.transform.GetChild(7).transform.gameObject.activeSelf;
        data.obj2 = objcontrol.transform.GetChild(6).transform.gameObject.activeSelf;
        data.obj3 = objcontrol.transform.GetChild(5).transform.gameObject.activeSelf;
        data.obj4 = objcontrol.transform.GetChild(4).transform.gameObject.activeSelf;
        data.obj5 = objcontrol.transform.GetChild(3).transform.gameObject.activeSelf;
        data.obj6 = objcontrol.transform.GetChild(2).transform.gameObject.activeSelf;
        data.obj7 = objcontrol.transform.GetChild(1).transform.gameObject.activeSelf;
        data.obj8 = objcontrol.transform.GetChild(0).transform.gameObject.activeSelf;
        data.objplane1 = smokeplane.activeSelf;
        data.objplane2 = salaryplane.activeSelf;
        data.objplane3 = radioplane.activeSelf;
        data.objplane4 = plugplane.activeSelf;
        data.objplane5 = eyesplane.activeSelf;
        data.objplane6 = medicalplane.activeSelf;
        data.objplane7 = nameboardplane.activeSelf;
        data.objplane8 = brushplane.activeSelf;
        data.pick8 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(7).transform.gameObject.activeSelf;
        data.pick7 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(6).transform.gameObject.activeSelf;
        data.pick6 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(5).transform.gameObject.activeSelf;
        data.pick5 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(4).transform.gameObject.activeSelf;
        data.pick4 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(3).transform.gameObject.activeSelf;
        data.pick3 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(2).transform.gameObject.activeSelf;
        data.pick2 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(1).transform.gameObject.activeSelf;
        data.pick1 = GameObject.FindGameObjectWithTag("savepickcon").transform.GetChild(0).transform.gameObject.activeSelf;
        data.fuseactive = fuse.activeSelf;
        data.fusecheckfun = fuseput;
        data.onearmtrait = TraitSelectScript.Onearm;
        data.badearstrait = TraitSelectScript.Badear;
        data.badeyestrait = TraitSelectScript.Badeyes;
        data.goodearstrait = TraitSelectScript.Niceear;
        data.goodeyestrait = TraitSelectScript.Niceeyes;
        data.goodcount = Goodendcount;
    }
}




