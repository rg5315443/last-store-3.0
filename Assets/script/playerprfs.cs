using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerprfs : MonoBehaviour
{
    public setting setting;
    // Start is called before the first frame update
    public void saveprf()
    {
        PlayerPrefs.SetFloat("mainaudio", setting.savevolume);
        PlayerPrefs.SetInt("resolution", setting.resolutionindex);
    }
}
