using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FinalScoreAnimation : MonoBehaviour
{
    public TMP_Text ScoreText, YourScoreText;
    private float lastPlayTime = 0f;
    public float InitScore;
    public float TickRate;
    public AudioClip clip, SlowTicking,LastSound;
    public AudioSource countaudio;
    public TMP_InputField UserInput;
    public GameObject SubmitButtonGameObject, LastAudioGameObject;
    public UnityEngine.UI.Button SubmitButton;
    bool StartTicking;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTicking)
        {

            TickRate = FinalScorecaculator.Instance.Score - InitScore;
            InitScore += Time.deltaTime * TickRate;
            float playInterval = 1f / TickRate;
            playInterval = Mathf.Clamp(playInterval, 0.05f, 1f);
            if (TickRate > 0 && TickRate < 1)
            {
                Invoke("AddOne", 1.5f);
            }

            if (Time.time - lastPlayTime >= playInterval)
            {
                countaudio.PlayOneShot(SlowTicking);
                lastPlayTime = Time.time;
            }
        }
        ScoreText.text = Mathf.Floor(InitScore).ToString();
        if (UserInput.text != "" && UserInput != null)
        {
            SubmitButton.interactable = true;
        }
        else {
            if (SubmitButton != null)
            {
                SubmitButton.interactable = false;
            }
        }

    }
    void AddOne()
    {
        if (FinalScorecaculator.Instance.Score != InitScore)
        {
            LastAudioGameObject.SetActive(true);
            countaudio.enabled = false;
            InitScore = FinalScorecaculator.Instance.Score;
            UserInput.gameObject.SetActive(true);
            SubmitButtonGameObject.SetActive(true);
        }
    }
    void PlayFirstTMP()
    {
        AudioSource audioSource;
        audioSource = GetComponent<AudioSource>();
        YourScoreText.gameObject.SetActive(true);
        audioSource.PlayOneShot(clip);
    }
    void PlaySecondTMP()
    {
        AudioSource audioSource;
        audioSource = GetComponent<AudioSource>();
        ScoreText.gameObject.SetActive(true);
        audioSource.PlayOneShot(clip);
    }
    void StartTick()
    {
        StartTicking = true;
        countaudio.enabled = true;

    }
}
