using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class TraitManager : MonoBehaviour,IDatapersistence
{
    private static TraitManager instance;
    public static TraitManager Instance
    {
        get 
        {
            return instance;
        }
    }
    [Header("幽閉恐懼症")]
    public bool ScareAlone;

    [Header("跛腳")]
    public bool lame;

    [Header("夜梟")]
    public bool Nicevision;

    public HDAdditionalLightData VisionLight;

    public float NicevisionPower, NicevisionCurrentPower;

    [Header("強迫症")]

    public bool OCD;

    public float OCDLv1Power, OCDLv2Power, OCDLv3Power, OCDLv4Power, OCDLv5Power;

    [Header("腎上腺素")]
    public bool Adrenaline;

    public float AdrenalineRange;

    public float AdrenalinePower;

    [Header("勤儉節約")]
    public bool Frugal;
    
    public float FrugalPower;

    [Header("飛毛腿")]
    public bool RunSpeedUp;

    public float RunSpeedUpPower;

    [Header("淡定")]
    public bool RunSpeedDown;

    public float RunSpeedDownPower;

    [Header("健步如飛")]
    public bool WalkSpeedUp;

    public float WalkSpeedUpPower;

    [Header("怯步")]
    public bool WalkSpeedDown;

    public float WalkSpeedDownPower;

    [Header("熟練工")]
    public bool WorkSpeedUp;

    public float WorkSpeedUpPower;

    [Header("菜鳥")]
    public bool WorkSpeedDown;

    public float WorkSpeedDownPower;

    [Header("貓步")]
    public bool AttractDown;

    public float AttractDownPower;

    [Header("粗魯")]
    public bool AttractUp;

    public float AttractUpPower;

    [Header("鐵人")]
    public bool StaminaCostDown;

    public float StaminaCostDownPower;

    [Header("肥胖")]
    public bool StaminaCostUp;

    public float StaminaCostUpPower;

    [Header("電池咬一咬")]
    public bool BatteryCostDown;

    public float BatteryaCostDownPower;

    [Header("舊電池")]
    public bool BatteryCostUp;

    public float BatteryaCostUpPower;

    [Header("全速撤退")]
    public bool Retreat;

    public float RetreatPower;

    [Header("信仰心")]
    public bool RecoverSanityUp;

    public float RecoverSanityUpPower;

    [Header("驚魂未定")]
    public bool RecoverSanityDown;

    public float RecoverSanityDownPower;

    [Header("麻木")]
    public bool ReduceSanityDown;

    public float ReduceSanityDownPower;

    [Header("神經質")]
    public bool ReduceSanityUp;

    public float ReduceSanityUpPower;

    [Header("輕裝上陣")]
    public bool TravelLight;

    public float TravelLightPower;

    [Header("精神病患")]
    public bool Psychopath;

    public float PsychopathPower;

    void Awake()
    {
        instance = this;

        Frugal = TraitSelectScript.Frugal;
        RunSpeedUp = TraitSelectScript.RunSpeedUp;
        RunSpeedDown = TraitSelectScript.RunSpeedDown;
        WalkSpeedUp = TraitSelectScript.WalkSpeedUp;
        WalkSpeedDown = TraitSelectScript.WalkSpeedDown;
        WorkSpeedUp = TraitSelectScript.WorkSpeedUp;
        WorkSpeedDown = TraitSelectScript.WorkSpeedDown;
        AttractDown = TraitSelectScript.AttractDown;
        AttractUp = TraitSelectScript.AttractUp;
        StaminaCostDown = TraitSelectScript.StaminaCostDown;
        StaminaCostUp = TraitSelectScript.StaminaCostUp;
        BatteryCostDown = TraitSelectScript.BatteryCostDown;
        BatteryCostUp = TraitSelectScript.BatteryCostUp;
        Adrenaline = TraitSelectScript.Adrenaline;
        OCD = TraitSelectScript.OCD;
        Nicevision = TraitSelectScript.Nicevision;
        lame = TraitSelectScript.lame;
        ScareAlone = TraitSelectScript.ScareAlone;
        Retreat = TraitSelectScript.Retreat;
        RecoverSanityUp = TraitSelectScript.RecoverSanityUp;
        RecoverSanityDown = TraitSelectScript.RecoverSanityDown;
        ReduceSanityDown = TraitSelectScript.ReduceSanityDown;
        ReduceSanityUp = TraitSelectScript.ReduceSanityUp;
        TravelLight = TraitSelectScript.TravelLight;
        Psychopath = TraitSelectScript.Psychopath;
       

    }
    void Start()
    {


        if (Nicevision)
        {
            Debug.Log("Nicevision");
            VisionLight.intensity = NicevisionPower;
        }
        else VisionLight.intensity = 200;
    }

    // Update is called once per frame
    void Update()
    {
        NicevisionCurrentPower = VisionLight.intensity;
    }

    public void loaddata(Gamedata data)
    {
        if (data != null) 
        {
            Frugal = data.Frugal;
            RunSpeedUp = data.RunSpeedUp;
            RunSpeedDown = data.RunSpeedDown;
            WalkSpeedUp = data.WalkSpeedUp;
            WalkSpeedDown = data.WalkSpeedDown;
            WorkSpeedUp = data.WorkSpeedUp;
            WorkSpeedDown = data.WorkSpeedDown;
            AttractDown = data.AttractDown;
            AttractUp = data.AttractUp;
            StaminaCostDown = data.StaminaCostDown;
            StaminaCostUp = data.StaminaCostUp;
            BatteryCostDown = data.BatteryCostDown;
            BatteryCostUp = data.BatteryCostUp;
            Adrenaline = data.Adrenaline;
            OCD = data.OCD;
            Nicevision = data.Nicevision;
            lame = data.lame;
            ScareAlone = data.ScareAlone;
            Retreat = data.Retreat;
            RecoverSanityUp = data.RecoverSanityUp;
            RecoverSanityDown = data.RecoverSanityDown;
            ReduceSanityDown = data.ReduceSanityDown;
            ReduceSanityUp = data.ReduceSanityUp;
            TravelLight = data.TravelLight;
            Psychopath = data.Psychopath;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.Frugal = Frugal;
        data.RunSpeedUp = RunSpeedUp;
        data.RunSpeedDown = RunSpeedDown;
        data.WalkSpeedUp = WalkSpeedUp;
        data.WalkSpeedDown = WalkSpeedDown;
        data.WorkSpeedUp = WorkSpeedUp;
        data.WorkSpeedDown = WorkSpeedDown;
        data.AttractDown = AttractDown;
        data.AttractUp = AttractUp;
        data.StaminaCostDown = StaminaCostDown;
        data.StaminaCostUp = StaminaCostUp;
        data.BatteryCostDown = BatteryCostDown;
        data.BatteryCostUp = BatteryCostUp;
        data.Adrenaline = Adrenaline;
        data.OCD = OCD;
        data.Nicevision = Nicevision;
        data.lame = lame;
        data.ScareAlone = ScareAlone;
        data.Retreat = Retreat;
        data.RecoverSanityUp = RecoverSanityUp;
        data.RecoverSanityDown = RecoverSanityDown;
        data.ReduceSanityDown = ReduceSanityDown;
        data.ReduceSanityUp = ReduceSanityUp;
        data.TravelLight = TravelLight;
        data.Psychopath = Psychopath;
    }
}
