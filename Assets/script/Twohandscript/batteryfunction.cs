using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class batteryfunction : MonoBehaviour
{

    Onhandcheck handcheck;
    public  Onhandcheck flashhandcheck;
    public  flashlight flashlight1;
    public GameObject[] flashligh;
    public battery batterycontroll;
    public AudioSource audio;
    public AudioClip audioClip;
    // Start is called before the first frame update
    void Start()
    {
        batterycontroll = GameObject.FindAnyObjectByType<battery>();
        handcheck = GetComponent<Onhandcheck>();
        flashligh = GameObject.FindGameObjectsWithTag("Flashlight");
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetMouseButtonDown(1) && handcheck.Right == true) || (Input.GetMouseButtonDown(0) && handcheck.Left == true))
        {
            for (int i = 0; i < flashligh.Length; i++)
            {
                flashlight1 = flashligh[i].GetComponentInChildren<flashlight>();
                flashhandcheck = flashligh[i].GetComponentInChildren<Onhandcheck>();
                if ((flashhandcheck.Right == true || flashhandcheck.Left == true) && HandSystem.Instance.selecting == false && !NewBehaviourScript.Instance.fuseinsight && !NewBehaviourScript.Instance.switchinsight&& HandSystem.Instance.stashselect == false)
                {
                    if (flashlight1.currentelectric != 100)
                    {

                        flashlight1.time = 0;
                        flashlight1.currentelectric = 100;
                        audio.PlayOneShot(audioClip);
                        //batterycontroll.batterysposchange(this.gameObject.name);
                        if (TraitManager.Instance.Frugal)
                        {
                            int Ramdomint = Random.Range(0, 100);
                            Debug.Log(Ramdomint);
                            if (Ramdomint > TraitManager.Instance.FrugalPower)
                            {
                                if (handcheck.Left == true) { HandSystem.Instance.Leftfull = false; } else { HandSystem.Instance.Rightfull = false; }
                                Destroy(this.gameObject);
                            }
                        }
                        else
                        {
                            if (handcheck.Left == true) { HandSystem.Instance.Leftfull = false; } else { HandSystem.Instance.Rightfull = false; }
                            Destroy(this.gameObject);
                        }

                    }

                }
            }
           
        }
    }

   /* public void changetarget(GameObject flashlight)
    {
        Debug.Log(flashlight1.gameObject.name);
        Debug.Log(flashhandcheck.gameObject.name);
        flashhandcheck = flashlight.GetComponent<Onhandcheck>();
        flashlight1 = flashlight.GetComponent<flashlight>();
    }*/
}
