using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using UnityEngine;

public class flashlight : MonoBehaviour
{
    public bool  Opened,cheat;
    public float currentelectric;
    public float maxelectric;
    public float consume;
    public float time;
    public Light Flashlight;
    public SanManager SanManager_Script;
    public NewBehaviourScript NewBehaviourScript;
    Onhandcheck handcheck;
    float inter;
    float flains;
    public AudioSource audio;
    public AudioClip audioClip;
    bool FlashLight_Triggered;
    // Start is called before the first frame update
    void Start()
    {
        TraitRrefrsh();
         flains = Flashlight.intensity;
        Flashlight = GetComponentInChildren<Light>();
        handcheck = GetComponent<Onhandcheck>();
    }

    // Update is called once per frame
    void Update()
    {
        //�s��
        if (((Input.GetMouseButtonDown(1)&&handcheck.Right == true)||(Input.GetMouseButtonDown(0) && handcheck.Left == true))
            &&HandSystem.Instance.selecting == false && !NewBehaviourScript.Instance.fuseinsight && !NewBehaviourScript.Instance.switchinsight&& HandSystem.Instance.stashselect == false)
        {
            if (currentelectric > 0)
            {
                Opened = !Opened;
                audio.PlayOneShot(audioClip);
            }
            
        }
        if (Opened)
        {
            if (!FlashLight_Triggered)
            {
                SanManager_Script.FlashLightSan(true);
                FlashLight_Triggered = true;
            }
            
            Flashlight.enabled = true;
            if (currentelectric > 0)
            {
                currentelectric = currentelectric - consume * Time.deltaTime;
            }
            if (currentelectric > 25f)
            {
                Flashlight.intensity = flains;
            }
            if (currentelectric <= 25f)
            {
                time += Time.deltaTime;
                inter = time * consume / 25f;

                Flashlight.intensity = Mathf.Lerp(flains, 0, inter);

            }
        }
        else
        {
            Flashlight.enabled=false;
            if (FlashLight_Triggered)
            {
                SanManager_Script.FlashLightSan(false);
                FlashLight_Triggered = false;
            }
            
        }
        if (currentelectric <= 0f)
        {
            if (FlashLight_Triggered)
            {
                SanManager_Script.FlashLightSan(false);
                FlashLight_Triggered = false;
            }
            Flashlight.enabled = false;
            Opened = false;
           // NewBehaviourScript.elecisrunout = false;
            //NewBehaviourScript.open = false;
        }


        //�ª�
        /*if (Input.GetKeyDown(KeyCode.F))
        {
            if (cheat == false)
            {
                cheat = true;
                currentelectric = 10;
                time = 0;
            }
            else cheat=false;
        }    
        if (NewBehaviourScript.open == true&&cheat == false)
        {
            if (currentelectric > 0)
            {
                currentelectric = currentelectric - consume * Time.deltaTime;
            }
            
        }
        if (NewBehaviourScript.open == true)
        {
            if (currentelectric > 25f)
            {
                Flashlight.intensity = flains;
            }
            if (currentelectric <= 25f)
            {
                time += Time.deltaTime;
                inter = time*consume / 25f;

                Flashlight.intensity = Mathf.Lerp(flains, 0, inter);

            }
        }


        if (currentelectric <= 0f)
        {
            NewBehaviourScript.elecisrunout = false;
            //NewBehaviourScript.open = false;
        }
        if (NewBehaviourScript.battery > 0 && Input.GetKeyDown(KeyCode.R))
        {
            time = 0;
            NewBehaviourScript.flashaudio.PlayOneShot(NewBehaviourScript.audioClips[3]);
            GameObject BATTERYIMG = GameObject.FindGameObjectWithTag("BATTERYIMG");
            Destroy(BATTERYIMG);
            currentelectric = maxelectric;
            NewBehaviourScript.elecisrunout = false;
            NewBehaviourScript.battery--;
        }
        */
    }
    void TraitRrefrsh()
    {
        if (TraitManager.Instance.BatteryCostDown)
        {
            float consumecache = consume;
            consume -= consumecache * TraitManager.Instance.BatteryaCostDownPower / 100;
        }
        if (TraitManager.Instance.BatteryCostUp)
        {
            float consumecache = consume;
            consume += consumecache * TraitManager.Instance.BatteryaCostUpPower / 100;
        }
    }
}
