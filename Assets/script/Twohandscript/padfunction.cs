using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class padfunction : MonoBehaviour
{

    Onhandcheck handcheck;
    public float fadeintime;
    public float fadeouttime;
    float time;
    public Vector3 rightpos, leftpos,pos,oringpos;
    bool incouting,outcouting,Maping;
    public bool watching;
    public GameObject Map;
    public BoxCollider BoxCollider;
    // Start is called before the first frame update
    void Start()
    {
        BoxCollider = GetComponent<BoxCollider>();
        handcheck = GetComponent<Onhandcheck>();
        if (Maping)
        {
            Map.SetActive(true);
        }else Map.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)&& watching)
        {
            Maping = !Maping;
            if (Maping)
            {
                Map.SetActive(true);
            }
            else Map.SetActive(false);
        }

        if (incouting)
        {

            if (time < fadeintime)
            {

                time += Time.deltaTime;
                transform.localPosition = Vector3.Lerp(transform.localPosition, pos, time / fadeintime);
            }
            else
            {
                watching = true;
                time = 0;
                incouting = false;
            } 
        }
        else if (outcouting)
        {
            if (time < fadeintime)
            {

                time += Time.deltaTime;
                transform.localPosition = Vector3.Lerp(transform.localPosition, oringpos, time / fadeintime);
            }
            else
            {
                HandSystem.Instance.watching = false;
                watching = false;
                time = 0;
                outcouting = false;
            }
        }
            if (((Input.GetMouseButtonDown(1) && handcheck.Right == true) || (Input.GetMouseButtonDown(0) 
            && handcheck.Left == true)) && HandSystem.Instance.selecting == false && !NewBehaviourScript.Instance.
            fuseinsight && !NewBehaviourScript.Instance.switchinsight && HandSystem.Instance.stashselect == false && !HandSystem.Instance.canmove && !HandSystem.Instance.Invertcanmove)
        {           
                if (handcheck.Right == true) pos = rightpos; else pos = leftpos;
            if (!watching && (!incouting && !outcouting))
            {
                HandSystem.Instance.watching = true;
                oringpos = transform.localPosition;
                incouting = true;
                BoxCollider.enabled = false;
            }
            else
            {
                outcouting = true;
                BoxCollider.enabled = true;
            }
                        
        }
    }
}
