using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P3kind : MonoBehaviour
{
    Onhandcheck handcheck;
    public stamina stamina;
    // Start is called before the first frame update
    void Start()
    {

        handcheck = GetComponent<Onhandcheck>();
        StartCoroutine(StartGetPlayer());
    }

    // Update is called once per frame
    void Update()
    {
        if (((Input.GetMouseButtonDown(1) && handcheck.Right == true) || (Input.GetMouseButtonDown(0) && handcheck.Left == true)) && HandSystem.Instance.selecting == false && !NewBehaviourScript.Instance.fuseinsight && !NewBehaviourScript.Instance.switchinsight && !HandSystem.Instance.stashselect)
        {
            if (stamina != null)
            {
                if (stamina.staminavalue != stamina.maxstaminavalue)
                {

                    stamina.staminavalue = stamina.maxstaminavalue;


                    if (TraitManager.Instance.Frugal)
                    {
                        int Ramdomint = Random.Range(0, 100);
                        Debug.Log(Ramdomint);
                        if (Ramdomint > TraitManager.Instance.FrugalPower)
                        {
                            if (handcheck.Left == true) { HandSystem.Instance.Leftfull = false; } else { HandSystem.Instance.Rightfull = false; }
                            P3使徒總控.Instance.FindSpawnSpace(this.gameObject);
                            this.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        if (handcheck.Left == true) { HandSystem.Instance.Leftfull = false; } else { HandSystem.Instance.Rightfull = false; }
                        //P3使徒總控.Instance.DetectedAndSpawn();
                        P3使徒總控.Instance.FindSpawnSpace(this.gameObject);
                        this.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                try
                {
                    stamina = GameObject.Find("PlayerCapsule").GetComponent<stamina>();
                }
                catch { }
            }
        }
    }

    IEnumerator StartGetPlayer()
    {
        //stamina = GameObject.Find("PlayerCapsule").GetComponent<stamina>();
        yield return new WaitForSeconds(6f);
        stamina = GameObject.Find("PlayerCapsule").GetComponent<stamina>();
    }
}
