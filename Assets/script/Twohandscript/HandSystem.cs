using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.UIElements;

public class HandSystem : MonoBehaviour
{
    private static HandSystem instance;
    public static HandSystem Instance
    {
        get
        {
            return instance;
        }
    }
    public GameObject[] Stashpoint;
    public Camera PlayerCam;
    [SerializeField] float objdistance, Secobjdistance;
    public LayerMask itemlayer,AvoidLayer, StashLayer;
    public float limittime,time, changetime, Inverttime,SecInvertime,Centertobottom,SecCentertobottom,raycastdis;
    public GameObject lefthand,righthand,handpos, Inverthand,secInverthand, Stashobj;
    public GameObject lefthandobj = null,righthandobj = null;
    Transform obj;
    public Vector3 Inverttransformpos, secInverttransformpos, hitpoint,sechitpoint;
    private Transform Inverttransform;
    public Slider sliderbar;
    RaycastHit hit;
    Vector3 rayorigin, raydir;
    public bool Invertcanmove,SecInvertcanmove, canmove,Leftfull,Rightfull,handfull,selecting,watching,stashing, stashselect , stashfull;
    public int stashcount;
    bool useed,HadPlacedParent;
    public GameObject ApostleParent;
    bool ApostleChange;
    public int TakedCount,TakedFlower;
    bool TakedCountGranted,TakedFlowerGranted;
    void Awake()
    {
        instance = this;
    }
    void Start()
    {
    }

   
    void Update()
    {
        if (TakedCount >= 20 && !TakedCountGranted)
        {
            FinalScorecaculator.CaculatingScore += 10000;
            TakedCountGranted = true;
        }
        if (TakedFlower >= 5 && !TakedFlowerGranted) {
            FinalScorecaculator.CaculatingScore += 1000;
            TakedFlowerGranted = true;
        }
         rayorigin = PlayerCam.transform.position;
         raydir = PlayerCam.transform.forward;
        // sliderbar.value = changetime;
        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer))
        {
           // Debug.Log(hit.transform.name);
            if (hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery"|| hit.transform.tag == "P3kind"||hit.transform.tag == "保險絲" ||
                hit.transform.tag == "Pad")
            {
                //Debug.Log(hi.transform.name);
                // Debug.Log(hit.transform.name);
                selecting = true;
                
            }
            else if (hit.transform == null)
            {
                selecting = false;
            }
            else selecting = false;
        }
        else selecting = false;

        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, StashLayer))
        {
            if (hit.transform.CompareTag("Stash"))
            {
                stashselect = true;
               // if (selecting == false)
                    //NewBehaviourScript.Instance.UItext.text = "存入";
            }
            else
            {
                //NewBehaviourScript.Instance.UItext.text = "";
                stashselect = false; 
            }
        }
        else
        {
            //NewBehaviourScript.Instance.UItext.text = "";
            stashselect = false;
        }


        sliderbar.value = changetime;
        /* if(Inverttransform!=null)
         Inverttransformpos = Inverttransform.position;*/

        //canmove 當拾取物件在往手上移動時回傳true 
        //Invertcanmove 當手上的物件再往放置目的地移動時回傳true 
        //watching 當正在使用Pad時回傳true 
        // Leftfull or Rightfull 判斷該手是否持有物件
        if (Input.GetMouseButtonDown(0)&& !canmove && !Invertcanmove && !watching)
        {
            if (!Leftfull)
            {
                LeftGrab();
                useed = false;
            }
        }
        if (Input.GetMouseButtonDown(1) && !canmove && !Invertcanmove && !watching)
        {
            if (!Rightfull)
            {
                RightGrab();
                useed = false;
            }
        }
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) //放開滑鼠重製changetime時間
        {
            changetime = 0;
        }
        if (Input.GetMouseButton(0)&& Leftfull && !canmove && !Invertcanmove && !watching && !stashing) //左手交換方法
        {
            if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, StashLayer) && !selecting)
            {
                if (hit.transform.tag == "Stash"&& !stashfull)
                {
                    changetime += Time.deltaTime;
                    if (changetime >= 1)
                    {
                        Debug.Log("Stash");
                        LeftStash();
                        changetime = 0;
                    }
                }
                else changetime = 0;
            }
           else if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer)&&!stashing)
            {
                if (hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery" || hit.transform.tag == "P3kind" 
                    || hit.transform.tag == "保險絲" || hit.transform.tag == "Pad" || hit.transform.tag == "Stash")
                {
                    changetime += Time.deltaTime; //如果為預期目標changetime每秒+1
                }
                else changetime = 0;
                if (changetime >= 1) //若changetime>=1執行LeftChange()方法
                {
                    LeftChange();
                    changetime = 0;
                }
                
            }
            

        }


        //右手交換方法
        if (Input.GetMouseButton(1) && Rightfull && !canmove && !Invertcanmove && !watching && !stashing)
        {
            if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, StashLayer) && !selecting)
            {
                if (hit.transform.tag == "Stash" && !stashfull)
                {
                    changetime += Time.deltaTime;
                    if (changetime >= 1)
                    {
                        RightStash();
                        changetime = 0;
                    }
                }
                else changetime = 0;
            }
            else if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer) && !stashing )
            {
                if (hit.transform.tag == "Flashlight"||hit.transform.tag =="Battery"|| hit.transform.tag == "P3kind" || hit.transform.tag == "保險絲" || hit.transform.tag == "Pad")
                {
                    changetime += Time.deltaTime;
                }
                else changetime = 0;
            }
            if (changetime >= 1)
            {
                RightChange();
            }
        }


        if (canmove == true)
        {
            
            Moveobj();
        }
        if (Invertcanmove == true)
        {
            Invertmoveobj();
        }
        if (SecInvertcanmove == true)
        {
            Invertmoveobj2();
        }
    }
    public void Invertmoveobj()
    {
        Inverttime += Time.deltaTime; //存入時間開始計算
        if (Inverthand.transform.parent != null&&NewBehaviourScript.Instance.hidingitemdrop!= true&& HadPlacedParent!=true) //強制解除「存入物件」與「手」的父子關係
        {
            /*if (obj.parent != null)
            {
                if (obj.parent.CompareTag("P3_StashPoint"))
                {
                    Inverthand.transform.SetParent(obj.parent);
                }
                Inverthand.transform.SetParent(null);
            }
            else
            {
                Inverthand.transform.SetParent(null);
            }*/
            Inverthand.transform.SetParent(null);
        }
        BoxCollider collider = Inverthand.GetComponent<BoxCollider>(); //抓取「存入物件」的BoxCollider組件
        collider.enabled = true; //停用BoxCollider以免誤觸發
        Onhandcheck onhandcheck = Inverthand.GetComponentInChildren<Onhandcheck>(); //抓取「存入物件」的Onhandcheck組件
        onhandcheck.Handcheck(3); //使物件Left何Right為false意味著此物品不在手中
        Centertobottom = Math.Abs(Inverthand.transform.position.y-Inverthand.transform.GetChild(0).transform.position.y); //計算「存入物件」中心座標以及底部的距離並用Math.Abs傳回絕對值
        if (Physics.Raycast(Inverttransformpos, Vector3.down, out hit, Mathf.Infinity,itemlayer)) //以「拾取物件」為原點向下進行偵測並指定特定Layer
        {
            if (hit.transform.gameObject != Inverthand && hit.transform.gameObject!=Inverthand) //排除偵測到自身
            {
                //Debug.Log(hit.transform.name);
                hitpoint = new Vector3(hit.point.x, hit.point.y + Centertobottom, hit.point.z); //紀錄偵測點的位置訊息並且讓點的Y軸加上「存入物件」的中心點和底部距離就是「存入物件」的存入位置
            }
        }
        if (Physics.Raycast(Inverttransformpos, Vector3.down, out hit, Mathf.Infinity, StashLayer))
        {
            
            if (hit.transform.tag == "Stash")
            {

                stashing = true;
            }
        }
        Inverthand.transform.position = Vector3.Lerp(Inverthand.transform.position, hitpoint, Inverttime / limittime); //透過Lerp平滑移動「存入物件」至存入位置
        objdistance = Vector3.Distance(Inverthand.transform.position, hitpoint); //計算「存入物件」和存入位置的距離
        if (objdistance <= 1e-05f) //如果距離<=0觸發
        {
            if (stashing)
            {
                Inverthand.transform.SetParent(Stashobj.transform);
                stashing = false;
            }
            else if (ApostleChange)
            {
                Inverthand.transform.SetParent(ApostleParent.transform);
                ApostleChange = false;
                Debug.Log("P3_StashPoint");
            }
            else if (HadPlacedParent != true && NewBehaviourScript.Instance.hidingitemdrop != true && !stashing)
            {

  
                    Inverthand.transform.SetParent(GameObject.Find("ChangedItemManager").transform);
                     HadPlacedParent = true;

            }
            Inverttime = 0; //存入時間重製
            Invertcanmove = false; //停止Invertmoveobj方法
            HadPlacedParent = false;
        }
        //設置物品存入的Quaternion 例如:if (obj.tag == "Flashlight"){ obj.localRotation = new Quaternion(-0.707106829f, 0, 0, 0.707106829f);
        #region 物件預設旋轉量
        if (Inverthand.tag == "Flashlight")
        {
            Inverthand.transform.rotation = new Quaternion(-0.707106829f, 0, 0, 0.707106829f);
        }
        else if (Inverthand.tag == "Battery")
        {
            Inverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        else if (Inverthand.tag == "P3kind")
        {
            Inverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        else if (Inverthand.tag == "保險絲")
        {
            Inverthand.transform.rotation = new Quaternion(0.707106829f, 0, 0, 0.707106829f);
            if (!obj.CompareTag("保險絲"))
            {
                NewBehaviourScript.Instance.有保險絲 = false;
            }
            else if (obj.parent != null)
            {
                if (obj.parent.CompareTag("stp"))
                {
                    NewBehaviourScript.Instance.有保險絲 = false;
                    if (Rightfull)
                    {
                        if (righthandobj.CompareTag("保險絲"))
                        {
                            NewBehaviourScript.Instance.有保險絲 = true;
                        }
                    }
                    else if (Leftfull)
                    {
                        if (lefthandobj.CompareTag("保險絲"))
                        {
                            NewBehaviourScript.Instance.有保險絲 = true;
                        }
                    }
                }
            }


        }
        else if (Inverthand.tag == "Pad")
        {
            Inverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        #endregion
    }
    public void Invertmoveobj2()
    {
        SecInvertime += Time.deltaTime; //存入時間開始計算
        if (secInverthand.transform.parent != null && NewBehaviourScript.Instance.hidingitemdrop != true) //強制解除「存入物件」與「手」的父子關係
        {
            secInverthand.transform.SetParent(null);
        }
        BoxCollider collider = secInverthand.GetComponent<BoxCollider>(); //抓取「存入物件」的BoxCollider組件
        collider.enabled = true; //停用BoxCollider以免誤觸發
        Onhandcheck onhandcheck = secInverthand.GetComponentInChildren<Onhandcheck>(); //抓取「存入物件」的Onhandcheck組件
        onhandcheck.Handcheck(3); //使物件Left何Right為false意味著此物品不在手中
        SecCentertobottom = Math.Abs(secInverthand.transform.position.y - secInverthand.transform.GetChild(0).transform.position.y); //計算「存入物件」中心座標以及底部的距離並用Math.Abs傳回絕對值
        if (Physics.Raycast(secInverttransformpos, Vector3.down, out hit, Mathf.Infinity, itemlayer)) //以「拾取物件」為原點向下進行偵測並指定特定Layer
        {
            if (hit.transform.gameObject != secInverthand && hit.transform.gameObject != Inverthand) //排除偵測到自身
            {
                //Debug.Log(hit.transform.name);
                sechitpoint = new Vector3(hit.point.x, hit.point.y + SecCentertobottom, hit.point.z); //紀錄偵測點的位置訊息並且讓點的Y軸加上「存入物件」的中心點和底部距離就是「存入物件」的存入位置
            }
        }
        secInverthand.transform.position = Vector3.Lerp(secInverthand.transform.position, sechitpoint, SecInvertime / limittime); //透過Lerp平滑移動「存入物件」至存入位置
        Secobjdistance = Vector3.Distance(secInverthand.transform.position, sechitpoint); //計算「存入物件」和存入位置的距離
        if (Secobjdistance <= 1e-05f) //如果距離<=0觸發
        {
            SecInvertime = 0; //存入時間重製
            SecInvertcanmove = false; //停止Invertmoveobj方法

        }
        //設置物品存入的Quaternion 例如:if (obj.tag == "Flashlight"){ obj.localRotation = new Quaternion(-0.707106829f, 0, 0, 0.707106829f);
        #region 物件預設旋轉量
        if (secInverthand.tag == "Flashlight")
        {
            secInverthand.transform.rotation = new Quaternion(-0.707106829f, 0, 0, 0.707106829f);
        }
        else if (secInverthand.tag == "Battery")
        {
            secInverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        else if (secInverthand.tag == "P3kind")
        {
            secInverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        else if (secInverthand.tag == "保險絲")
        {
            secInverthand.transform.rotation = new Quaternion(0.707106829f, 0, 0, 0.707106829f);
            NewBehaviourScript.Instance.有保險絲 = false;
        }
        else if (secInverthand.tag == "Pad")
        {
            secInverthand.transform.rotation = new Quaternion(0, 0, 0, 1);
        }
        #endregion
    }
    public void Moveobj()
    {

        if (!useed && obj.transform.parent!=null)
        {
            if (!obj.transform.parent.gameObject.CompareTag("Stash"))
            {
                stashcount = 0;
                foreach (GameObject ob in Stashpoint)
                {
                    if (ob.transform.childCount != 0)
                    {
                        stashcount++;
                    }

                }
                if (stashcount <= 10)
                {
                    stashfull = false;
                }
                useed = true;
            }
        }
        time += Time.deltaTime;
        if(obj!=null)
        {
            obj.position = Vector3.Lerp(obj.position, handpos.transform.position, time / limittime);
        }
        
        float objdistance = Vector3.Distance(obj.transform.position, handpos.transform.position); //計算拾取物件與目的地的距離
        if (objdistance <= 0.01f) //當距離<=0.01時觸發
        {
            


            obj.transform.SetParent(handpos.transform); //設置拾取物件的父物件使其跟隨手移動
            time = 0; //插值分子歸零
            canmove = false; //停止Moveobj方法
        }
        //設置物品在手中的Quaternion 例如:if (obj.tag == "Flashlight"){ obj.localRotation = new Quaternion(-0.707106829f, 0, 0, -0.707106829f);
        #region 物件預設旋轉量
        if (obj.tag == "Flashlight")
        {
            obj.localRotation = new Quaternion(-0.707106829f, 0, 0, -0.707106829f);
        } else if (obj.tag == "Battery")
        {
            obj.localRotation = new Quaternion(0, 0, 0, 1);
        }
        else if (obj.tag == "P3kind")
        {
            obj.localRotation = new Quaternion(0, 0, 0, 1);
        }
        else if (obj.tag == "保險絲")
        {
            obj.localRotation = new Quaternion(0, 0, 0, 1);
            NewBehaviourScript.Instance.有保險絲 = true;
        }
        else if (obj.tag == "Pad")
        {
            obj.localRotation = new Quaternion(0.5f, -0.5f, 0.5f, -0.5f);
        }
        #endregion 
    }
    public void LeftGrab()
    {
        //透過已玩家攝影機為原點的射線來判斷是否有打中物件 AvoidLayer迴避掉不希望擊中的Layer 
        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer))
        {
            //僅與預期目標交互
            if ((hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery" || hit.transform.tag == "P3kind" 
                || hit.transform.tag == "保險絲" || hit.transform.tag == "Pad") && canmove == false) //&& Invertcanmove == false 也可以加這段?
            {
                obj = hit.transform; //儲存目標物件transform訊息
                lefthandobj = hit.transform.gameObject; //儲存目標物件GameObject訊息
                canmove = true; //開始執行Moveobj方法
                Onhandcheck onhandcheck = obj.GetComponentInChildren<Onhandcheck>(); //抓取目標物件的Onhandcheck組件
                onhandcheck.Handcheck(0);//傳入0 目標物件Onhandcheck的bool Left 與 Right 分別為 true 與 false 傳入1則相反 傳入3全為false
                BoxCollider collider = obj.GetComponent<BoxCollider>(); //抓取目標物件的BoxCollider組件
                collider.enabled = false; //停用BoxCollider以免誤觸發
                try {
                    if (lefthandobj.GetComponent<Onhandcheck>().FirstGet ==false)
                    {
                        TakedCount++;
                        Debug.Log(TakedCount + "Had taked");
                        lefthandobj.GetComponent<Onhandcheck>().FirstGet = true;
                        if (lefthandobj.tag == "P3kind") {
                            TakedFlower++;
                        }
                    }
                }
                catch {
                    Debug.Log("There's Something wrong");
                }

                Leftfull = true; //左手已滿再空手前無法再次觸發LeftGrab方法
            }
            handpos = lefthand; //handpos為目標物件所指定的預期位置 由於是左手所以位置是lefthand  
        }
    }
    public void RightGrab()
    {
        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer))
        {
            if ((hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery"|| hit.transform.tag == "P3kind" ||
                hit.transform.tag == "保險絲" || hit.transform.tag == "Pad") && canmove == false && Invertcanmove == false)
            {
                obj = hit.transform;
                righthandobj = hit.transform.gameObject;
                canmove = true;
                Onhandcheck onhandcheck = obj.GetComponentInChildren<Onhandcheck>();
                onhandcheck.Handcheck(1);
                BoxCollider collider = obj.GetComponent<BoxCollider>();
                collider.enabled = false;
                try
                {
                    if (righthandobj.GetComponent<Onhandcheck>().FirstGet == false)
                    {
                        TakedCount++;
                        Debug.Log(TakedCount + "Had taked");
                        righthandobj.GetComponent<Onhandcheck>().FirstGet = true;
                        if (righthandobj.tag == "P3kind") {
                            TakedFlower++;
                        }
                    }
                }
                catch
                {
                    Debug.Log("There's Something wrong");
                }
                Rightfull = true;

            }
            //Inverthand = righthandobj;
            handpos = righthand;
        }
    }
    public void LeftChange()
    {
        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer))
        {
            if ((hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery" || hit.transform.tag == "P3kind" 
                || hit.transform.tag == "保險絲" || hit.transform.tag == "Pad") && canmove == false && Invertcanmove == false)
            {
                obj = hit.transform; //儲存拾取物件的Transform資料
                if (obj.transform.parent.parent != null)
                {   
                    if (obj.transform.parent.parent.tag != null)
                    {
                        if (obj.transform.parent.parent.CompareTag("P3_StashPoint"))
                        {
                            Apostleselect apostleselect = obj.transform.parent.parent.GetComponent<Apostleselect>();
                            if (lefthandobj.CompareTag("P3kind"))
                            {
                                if (obj.CompareTag("P3kind"))
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else if (apostleselect.GoodApsotle.transform.childCount == 0)
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else
                                {
                                    ApostleParent = obj.transform.parent.parent.gameObject;
                                    ApostleChange = true;
                                }
                            }
                            else
                            {
                                ApostleParent = obj.transform.parent.parent.gameObject;
                                ApostleChange = true;
                            }
                        }
                    }
                }
                 if (obj.transform.parent != null)
                { 
                    if (obj.transform.parent.tag != null)
                    {
                        if (obj.transform.parent.tag == "stp")
                        {
                            Stashobj = obj.transform.parent.gameObject;
                        }

                        else if (obj.transform.parent.CompareTag("P3_StashPoint"))
                        {
                            Apostleselect apostleselect = obj.transform.parent.GetComponent<Apostleselect>();
                            if (lefthandobj.CompareTag("P3kind"))
                            {
                                if (apostleselect.GoodApsotle.transform.childCount == 0)
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else
                                {
                                    ApostleParent = obj.transform.parent.gameObject;
                                    ApostleChange = true;
                                }
                            }
                            else
                            {
                                ApostleParent = obj.transform.parent.gameObject;
                                ApostleChange = true;
                            }
                        }
                    }
                }
                Inverttransformpos = obj.transform.position; //儲存拾取物件的Position 為「存入物件」待存入位置
                Inverthand = lefthandobj; //把目前手中的Gameobject資訊儲存至待會要進行存入動作的Gameobject資訊
                lefthandobj = hit.transform.gameObject; //把手中的Gameobject資料替換成拾取的Gameobject資料
                Invertcanmove = true; //進行存入
                canmove = true; //進行拾取
                Onhandcheck onhandcheck = obj.GetComponentInChildren<Onhandcheck>(); //抓取拾取物件的Onhandcheck組件
                onhandcheck.Handcheck(0); //拾取物件為左手拾取
                BoxCollider collider = obj.GetComponent<BoxCollider>(); //停用BoxCollider以免誤觸發
                collider.enabled = false; //停用BoxCollider以免誤觸發
                try
                {
                    if (lefthandobj.GetComponent<Onhandcheck>().FirstGet == false)
                    {
                        TakedCount++;
                        Debug.Log(TakedCount + "Had taked");
                        lefthandobj.GetComponent<Onhandcheck>().FirstGet = true;
                        if (lefthandobj.tag == "P3kind") {
                            TakedFlower++;
                        }
                    }
                }
                catch
                {
                    Debug.Log("There's Something wrong");
                }
                Leftfull = true; //左手已滿再空手前無法再次觸發LeftGrab方法
                changetime = 0; //重製滑鼠按住時間
            }     
            handpos = lefthand;  //儲存左手位置以供拾取物件進行位移
        }
    }

    public void RightChange()
    {
        if (Physics.Raycast(rayorigin, raydir, out hit, raycastdis, AvoidLayer))
        {
            if ((hit.transform.tag == "Flashlight" || hit.transform.tag == "Battery" 
                || hit.transform.tag == "P3kind" || hit.transform.tag == "保險絲" || hit.transform.tag == "Pad") 
                && canmove == false && Invertcanmove == false)
            {
                Debug.Log("Change");
                obj = hit.transform;
                if (obj.transform.parent.parent != null)
                {
                    if (obj.transform.parent.parent.tag != null)
                    {
                        if (obj.transform.parent.parent.CompareTag("P3_StashPoint"))
                        {
                            Apostleselect apostleselect = obj.transform.parent.parent.GetComponent<Apostleselect>();
                            if (righthandobj.CompareTag("P3kind"))
                            {
                                if (obj.CompareTag("P3kind"))
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else if (apostleselect.GoodApsotle.transform.childCount == 0)
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else
                                {
                                    ApostleParent = obj.transform.parent.parent.gameObject;
                                    ApostleChange = true;
                                }
                            }
                            else
                            {
                                ApostleParent = obj.transform.parent.parent.gameObject;
                                ApostleChange = true;
                            }
                        }
                    }
                }
                 if (obj.transform.parent != null)
                 {
                    if (obj.transform.parent.tag != null)
                    {
                        if (obj.transform.parent.tag == "stp")
                        {
                            Stashobj = obj.transform.parent.gameObject;
                        }

                        else if (obj.transform.parent.CompareTag("P3_StashPoint"))
                        {
                            Apostleselect apostleselect = obj.transform.parent.GetComponent<Apostleselect>();
                            if (righthandobj.CompareTag("P3kind"))
                            {
                                if (apostleselect.GoodApsotle.transform.childCount == 0)
                                {
                                    ApostleParent = apostleselect.GoodApsotle;
                                    ApostleChange = true;
                                }
                                else
                                {
                                    ApostleParent = obj.transform.parent.gameObject;
                                    ApostleChange = true;
                                }
                            }
                            else
                            {
                                ApostleParent = obj.transform.parent.gameObject;
                                ApostleChange = true;
                            }
                        }
                    }
                }
                //obj.localScale = new Vector3(0.1217f, 0.1217f, 0.1217f);
                Inverttransform = obj.transform;
                Inverttransformpos = obj.transform.position;
                //Inverttransformpos = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
                Inverthand = righthandobj;
                righthandobj = hit.transform.gameObject;
                Invertcanmove = true;
                canmove = true;
                Onhandcheck onhandcheck = obj.GetComponentInChildren<Onhandcheck>();
                onhandcheck.Handcheck(1);
                BoxCollider collider = obj.GetComponent<BoxCollider>();
                collider.enabled = false;
                // Debug.Log(hit.transform.name);
                try
                {
                    if (righthandobj.GetComponent<Onhandcheck>().FirstGet == false)
                    {
                        TakedCount++;
                        Debug.Log(TakedCount + "Had taked");
                        righthandobj.GetComponent<Onhandcheck>().FirstGet = true;
                        if (righthandobj.tag == "P3kind")
                        {
                            TakedFlower++;
                        }
                    }
                }
                catch
                {
                    Debug.Log("There's Something wrong");
                }
                Rightfull = true;
                changetime = 0;
            }

            handpos = righthand;

        }
    }
    void LeftStash()
    {
        stashcount = 0;
        foreach (GameObject ob in Stashpoint)
        {
            if (ob.transform.childCount == 0)
            {
                Inverthand = lefthandobj;
                Inverttransformpos = ob.transform.position;
                Invertcanmove = true;
                Leftfull = false;
                lefthandobj = null;
                Stashobj = ob;
                break;
            }
        }
        foreach (GameObject ob in Stashpoint)
        {
            if (ob.transform.childCount != 0)
            {
                stashcount++;
            }
                if ((stashcount+1)>= Stashpoint.Length)
            {
                stashfull = true;
            }
        }

    }
    void RightStash()
    {
        stashcount = 0;
        foreach (GameObject ob in Stashpoint)
        {
            if (ob.transform.childCount == 0)
            {
                Inverthand = righthandobj;
                Inverttransformpos = ob.transform.position;
                Invertcanmove = true;
                Rightfull = false;
                righthandobj = null;
                Stashobj = ob;
                break;
            }
        }
        foreach (GameObject ob in Stashpoint)
        {
            if (ob.transform.childCount != 0)
            {
                stashcount++;
            }
            if ((stashcount+1) >= Stashpoint.Length)
            {
                stashfull = true;
            }
        }
    }

}
