using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            { 
            rb.AddForce(Vector3.forward*5,ForceMode.Impulse);
            rb.constraints = RigidbodyConstraints.None;
        }
    }
}
