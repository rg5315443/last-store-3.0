using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Itemfunction : MonoBehaviour
{
    public Onhandcheck Handcheck;
    void Start()
    {
        Handcheck = GetComponent<Onhandcheck>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Handcheck.Left == true)
        {
            Destroy(this.gameObject);
        }
        if (Input.GetMouseButtonDown(1) && Handcheck.Right == true)
        {
            Destroy(this.gameObject);
        }
    }
}
