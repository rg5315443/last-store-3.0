using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using WMPLib;
using static UnityEngine.UIElements.UxmlAttributeDescription;

public class FUSEcontroller : MonoBehaviour, IDatapersistence
{
    public GameObject[] fuse;
    public int fusespawn;
    public float time;
    private static FUSEcontroller instance;
    bool hadgenerate;
    public static FUSEcontroller Instance
    {
        get
        {
            return instance;
        }



    }
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetInt("Turtorial") == 2 && hadgenerate != true)
        {
            for (int i = 0; i < fusespawn; i++)
            {
                int num = UnityEngine.Random.Range(0, fuse.Length);
                while (fuse[num].activeSelf == true)
                {
                    num = UnityEngine.Random.Range(0, fuse.Length);
                }
                fuse[num].SetActive(true);
            }
            hadgenerate = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void FinalSpawn()
    { 
        int ChildCoount = transform.childCount;

        for (int i = 0; i < ChildCoount; i++)
        {
            fuse[i] = transform.GetChild(i).gameObject; 
        }
        foreach (GameObject gameObject in fuse)
        {
            if (!gameObject.activeSelf)
            { 
                gameObject.SetActive(true);
                break;
            }
        }
    }
    /* public void fuseposchange(string fusename)
     {
         string fuseNAMEIN=fusename;
         int fusenumber = Random.Range(0,fuse.Length);

         if(fuse[fusenumber].name!=fusename && fuse[fusenumber].activeSelf == false)
         {
             StartCoroutine(count(fusenumber));

         }else fuseposchange(fuseNAMEIN);

     }
     IEnumerator count(int fusenumber)
     {
         yield return new WaitForSeconds(time);
         fuse[fusenumber].SetActive(true);
     }*/

   public void loaddata(Gamedata data)
    {

        if (data != null)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).gameObject.SetActive(data.insurancesavtive[i]);
                }
                catch { }
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).position = data.activeinsurancespos[i];
                }
                catch { }
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                try
                {
                    transform.GetChild(i).rotation = data.activeinsurancesros[i];
                }
                catch { }
            }
            hadgenerate = data.generatedfuse;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.insurancesavtive = new bool[transform.childCount];
        data.activeinsurancespos = new Vector3[transform.childCount];
        data.activeinsurancesros = new quaternion[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            data.insurancesavtive[i] = transform.GetChild(i).gameObject.activeSelf;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.activeinsurancespos[i] = transform.GetChild(i).transform.position;
        }
        for (int i = 0; i < transform.childCount; i++)
        {
            data.activeinsurancesros[i] = transform.GetChild(i).transform.rotation;
        }
        data.generatedfuse = hadgenerate;
    }
}
