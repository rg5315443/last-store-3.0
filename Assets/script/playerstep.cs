using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StarterAssets
{
public class playerstep : MonoBehaviour
{
    public AudioSource walk,run;
    public stamina stamina;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         if ((Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A)||  Input.GetKey(KeyCode.S)||  Input.GetKey(KeyCode.D)))&&!TraitManager.Instance.lame)
         {
            if(stamina.staminavalue>0)
            {
            run.enabled=true;
            walk.enabled=false;
            }
            else if(stamina.staminavalue<=0)
            {
             run.enabled=false;
            walk.enabled=true;
            }

         }
         else if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A)||  Input.GetKey(KeyCode.S)||  Input.GetKey(KeyCode.D))
         {
            run.enabled=false;
            walk.enabled=true;
         }
         else
         {
            run.enabled=false;
            walk.enabled=false;
         }
    }

        public void Run()
        {
            run.enabled = true;
            walk.enabled = false;
        }
        public void Walk()
        {
            run.enabled = false;
            walk.enabled = true;
        }
        public void Idel()
        {
                run.enabled = false;
                walk.enabled = false;
        }
    }
}