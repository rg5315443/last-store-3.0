using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class EndgameironDoor : MonoBehaviour
{
    public GameObject endgameobj;
    public Animator Endrollingdoorain;
    public AudioSource AudioSource;
    public AudioClip openbgm, stunbgm;
    public static bool Firstouch,repaired,BB_1;
    public FUSEcontroller FUSEcontroller;
    public TMP_Text FinalFixingText;
    int FixingTextCount;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (time.Instance.EndGame == true)
        {
            endgameobj.SetActive(true);
        }else endgameobj.SetActive(false);

        if (Firstouch == true && BB_1 == false)
        {
            AudioSource.PlayOneShot(openbgm);
            Endrollingdoorain.SetBool("firstopen", true);
            StartCoroutine("waitwaraing");
            BB_1 = true;

        }
        if (NewBehaviourScript.Instance.EndRepaired == true)
        {
            Endrollingdoorain.SetBool("Berepair", true);
        }
    }
    IEnumerator waitwaraing()
    {
        yield return new WaitForSeconds(6);
        FUSEcontroller.FinalSpawn();
        FinalFixingText.gameObject.SetActive(true);
        NewBehaviourScript.Instance.endwarning = true;
        NewBehaviourScript.Instance.EndRepair = true;
        NewBehaviourScript.Instance.fuseput = false;
        NewBehaviourScript.Instance.fuse.SetActive(false);
        InvokeRepeating("EnableAndDisableFixingText", 0f,1f);
    }
    void EnableAndDisableFixingText()
    {
        if (FinalFixingText.gameObject.activeSelf)
        {
            FinalFixingText.gameObject.SetActive(false);
        }
        else {
            FinalFixingText.gameObject.SetActive(true);
            FixingTextCount++;
        }
        if (FixingTextCount == 4)
        {
            FinalFixingText.gameObject.SetActive(false);
            CancelInvoke("EnableAndDisableFixingText");
        }
    }
}
