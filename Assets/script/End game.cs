using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class Endgame : MonoBehaviour
{
    public time time;
    public GameObject timegraph, endgame,endfade, endgamecg, endgamecgmask,sixammask,ScoreManager;
    public List<GameObject> dontkill = new List<GameObject>();
    public void Start()
    {
        StartCoroutine(theend());

    }
    public void Update()
    {
        Debug.Log(ScoreManager);
        ScoreManager = GameObject.Find("ScoreManager");
        if (dontkill.Contains(ScoreManager) == false)
        { 
        dontkill.Add(ScoreManager);
        }
    }


    IEnumerator theend()
    {
        //sixammask.SetActive(true);
        //yield return new WaitForSeconds(5f);
        GameObject[] allobj = FindObjectsOfType<GameObject>();
        List<GameObject> disableobj = new List<GameObject>(allobj);
        foreach (GameObject a in allobj) {
            foreach (GameObject b in dontkill) { 
            if(a.name == b.name)
                    disableobj.Remove(a);
                    disableobj.Remove(ScoreManager);
            }
        }
        foreach (GameObject a in disableobj)
        {
            a.SetActive(false);
        }

        for (int i = 0; i < timegraph.transform.childCount; i++)
        {
            timegraph.transform.GetChild(i).gameObject.SetActive(true);
        }
        endgame.SetActive(true);
        endfade.SetActive(true);
        timegraph.SetActive(true);
        yield return new WaitForSeconds(4f);
        timegraph.SetActive(false);
        endfade.SetActive(false);
        //endgame.SetActive(false);
       
        yield return new WaitForSeconds(1.5f);
        endgame.SetActive(false);
        endgamecg.SetActive(true);
        endgamecgmask.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadSceneAsync(0);

    }
}

