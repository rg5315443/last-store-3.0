using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class 首頁觸發 : MonoBehaviour
{
    public GameObject inanim,texts,savedgamebtn,settingbtn,settingmenu,Leaderboard_btn, Log_btn, desktop,startgamebtn,quitbtn,savebtn,SE,click,maincam,rawvideo;
    public Material[] materials;
    public Renderer rend;
    bool isClick=false;
    bool test;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SE.SetActive(false);
        click.SetActive(false);
       rend.enabled=true;
       rend.sharedMaterial=materials[0];
        inanim.SetActive(false);
        texts.SetActive(true);
        startgamebtn.SetActive(false);
        savebtn.SetActive(false);
        savedgamebtn.SetActive(false);
        settingbtn.SetActive(false);
        Leaderboard_btn.SetActive(false);
        Log_btn.SetActive(false);
        settingmenu.SetActive(false);
        desktop.SetActive(false);
        quitbtn.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && isClick == false && PlayerPrefs.GetInt("P5kill") == 0)
        {

            //RAW.SetActive(false);
            inanim.SetActive(true);
            texts.SetActive(false);
            isClick = true;
            StartCoroutine(DDD());
        }
        else if(Input.anyKeyDown && isClick == false && PlayerPrefs.GetInt("P5kill") == 1)
        {
            maincam.SetActive(false);
            texts.SetActive(false);
            rawvideo.SetActive(true);
            Invoke("offvideo", 10f);
        
        }
    }
    IEnumerator DDD()
    {
        yield return new WaitForSeconds(2.7f);
        rend.sharedMaterial=materials[1];
        SE.SetActive(true);
        yield return new WaitForSeconds(0.5f);
         SE.SetActive(false);
         click.SetActive(true);
        rend.sharedMaterial=materials[2];
        desktop.SetActive(true);
        savedgamebtn.SetActive(true);
        settingbtn.SetActive(true);
        startgamebtn.SetActive(true);
        quitbtn.SetActive(true);
        savebtn.SetActive(true);
        Leaderboard_btn.SetActive(true);
        Log_btn.SetActive(true);
    }
    private void OnDestroy()
    {
        PlayerPrefs.SetInt("P5kill", 0);
    }
    void offvideo() {
        maincam.SetActive(true);
        texts.SetActive(true);
        rawvideo.SetActive(false);
        PlayerPrefs.SetInt("P5kill", 0);
    }
}
