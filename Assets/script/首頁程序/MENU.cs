using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using static System.Net.Mime.MediaTypeNames;
using System.IO;
using System;

public class MENU : MonoBehaviour
{
    [Header("Menu Buttons")]
    [SerializeField] private Button newgame;
    [SerializeField] private Button savedgame;
    public GameObject Loadingbar,Traitmenu,TraitLoadGameError,BeKilledWindow;
    public UnityEngine.UI.Image bar;
    public TMP_Text text;
    public float vaule;
    public static bool BeKilled;
    public GameObject[] LoadingScene; 
    // Start is called before the first frame update
    private void Awake()
    {
        TraitSelectScript.Adrenaline = false;
        TraitSelectScript.AttractDown = false;
        TraitSelectScript.AttractUp = false;
        TraitSelectScript.Frugal = false;
        TraitSelectScript.lame = false;
        TraitSelectScript.BatteryCostDown = false;
        TraitSelectScript.BatteryCostUp = false;
        TraitSelectScript.OCD = false;
        TraitSelectScript.Psychopath = false;
        TraitSelectScript.RecoverSanityDown = false;
        TraitSelectScript.RecoverSanityUp = false;
        TraitSelectScript.ReduceSanityUp = false;
        TraitSelectScript.ReduceSanityDown = false;
        TraitSelectScript.Retreat = false;
        TraitSelectScript.RunSpeedDown = false;
        TraitSelectScript.RunSpeedUp = false;
        TraitSelectScript.StaminaCostDown = false;
        TraitSelectScript.StaminaCostUp = false;
        TraitSelectScript.WalkSpeedDown = false;
        TraitSelectScript.WalkSpeedUp = false;
        TraitSelectScript.WorkSpeedDown = false;
        TraitSelectScript.WorkSpeedUp = false;
        TraitSelectScript.TravelLight = false;
        TraitSelectScript.ScareAlone = false;
        TraitSelectScript.Nicevision = false;
        Loadingbar.SetActive(false);
        Resources.LoadAsync("START");
    }   
    public void onNewgameclick()
    {
        Loadingbar.SetActive(true);
        DisableMenuButtons();
        DataPersistence.instance.newgame();
        //SceneManager.LoadSceneAsync("START");
        StartCoroutine(LoadSceneasync("START"));
    }
    public void RandomLoadingScene()
    { 
        int random = UnityEngine.Random.Range(0, LoadingScene.Length);
        LoadingScene[random].SetActive(true);
    }
    public void onContinueclick()
    {
        if (!BeKilled)
        {
            if (TraitSelectScript.havetrait != true)
            {
                Loadingbar.SetActive(true);
                DisableMenuButtons();
                StartCoroutine(LoadSceneasync("START"));
            }
            else
            {
                TraitLoadGameError.SetActive(true);
            }
        }
        else
        {
            BeKilledWindow.SetActive(true);
        }
    }
    public void OffTraitLoadGameError() {
        TraitLoadGameError.SetActive(false);
    }
    public void onTraitclick()
    {
        Traitmenu.SetActive(true);
    }
    public void DisableMenuButtons() { 
    newgame.interactable = false;
    savedgame.interactable = false;
    }
    public void quitgame() {
        BeKilled = false;
        UnityEngine.Application.Quit();
    }


    private IEnumerator LoadSceneasync(string sceneName)
    {
        yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("START");

        // 等待場景加載完成
        while (!asyncLoad.isDone)
        {
            vaule = Mathf.Clamp01(asyncLoad.progress / 0.9f);
            
            text.text = Mathf.Round(vaule * 100).ToString()+"%"; 
            bar.fillAmount = vaule;
            yield return null;
        }
    }

}
