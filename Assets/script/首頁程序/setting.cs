using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class setting : MonoBehaviour
{
    public ����Ĳ�o menutrigger;
    public Animator setmenu;
    public AudioMixer mainaudio;
    Resolution[] resolutions;
    public Dropdown resdropdown;
    public float savevolume;
    public int resolutionindex;
    public int currentindex;
    public void Start()
    {
        resolutions = Screen.resolutions;
        resdropdown.ClearOptions();
        List<string> options = new List<string>();
        currentindex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        { 
        string option = resolutions[i].width + " X " + resolutions[i].height;
        options.Add(option);

        if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height) 
        { 
         currentindex = i;
        }
        }
        resdropdown.AddOptions(options);
        if (PlayerPrefs.GetInt("resolution") == currentindex)
        {
            resdropdown.value = currentindex;
        }
        else {
            resdropdown.value = PlayerPrefs.GetInt("resolution");
        }
        resdropdown.RefreshShownValue();
        setresolution(resdropdown.value);
    }
    public void setresolution(int resloutionindex) {
        if (gameObject.activeSelf)
        {
            if (PlayerPrefs.GetInt("resolution") == currentindex)
            {
                PlayerPrefs.SetInt("resolution", resloutionindex);
                Resolution resolution = resolutions[resloutionindex];
                Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
            }
            else
            {
                PlayerPrefs.SetInt("resolution", resloutionindex);
                resdropdown.GetComponent<Dropdown>().value = PlayerPrefs.GetInt("resolution");
                Resolution resolution = resolutions[PlayerPrefs.GetInt("resolution")];
                Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
            }
        }
    }
    public void Setting() {
     menutrigger.settingmenu.SetActive(true);
     StartCoroutine(animate());
    }
    IEnumerator animate() {
     yield return new WaitForSeconds(0.001f);
     setmenu.Play("setting");
    }
    public void Setvolume(float volume) {
        mainaudio.SetFloat("Volume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("mainaudio", volume);
    }
    public void offsetting() {
     setmenu.Play("off");
    }
    private void Update()
    {
        if (PlayerPrefs.GetFloat("mainaudio") != 0.0001) {
        Setvolume(PlayerPrefs.GetFloat("mainaudio"));
         savevolume = PlayerPrefs.GetFloat("mainaudio");
        GameObject.FindWithTag("Volume").GetComponent<UnityEngine.UI.Slider>().value = savevolume;

         UnityEngine.Debug.Log(PlayerPrefs.GetInt("resolution"));
        }
    }
}
