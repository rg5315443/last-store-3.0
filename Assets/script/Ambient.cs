using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ambient : MonoBehaviour
{
    public AudioClip inmusic;
    //public AudioClip outmusic;   
    AudioSource audioSource;
    public float fadeintime;
    public float vaulbe=0,maxvauble;
    public float time;
    bool inside;
    // Start is called before the first frame update
    void Start()
    {
        audioSource=GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(inside==false)
        {
            fadeintime+=Time.deltaTime;       
            vaulbe=Mathf.Lerp(vaulbe,0,fadeintime/ time);
         
            
            
            audioSource.volume=vaulbe;
        }
    }
    private void OnTriggerStay(Collider other) {
        if(other.tag=="Player")
        {
            inside=true;
            if(vaulbe<= maxvauble)
            {
             fadeintime+=Time.deltaTime;
         
             vaulbe=Mathf.Lerp(vaulbe, maxvauble, fadeintime/ time);
            
            //audioSource.PlayOneShot(inmusic);
            audioSource.volume=vaulbe;
            }
        }
    }
    private void OnTriggerExit(Collider other) {
        
       if(other.tag=="Player")
        {
            fadeintime=0;
            inside=false;

        }
    }

    private void OnTriggerEnter(Collider other) {
         if(other.tag=="Player")
        {
            fadeintime=0;
            audioSource.PlayOneShot(inmusic);

        }
    }

    }


        
            
       
    

