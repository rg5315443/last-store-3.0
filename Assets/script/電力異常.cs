using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class 電力異常 : MonoBehaviour, IDatapersistence
{
    public 官開燈控制[] device;
    public GameObject[] padwarnicon;
    public float 異常=0;
    public bool 解決=false;
     public float NeedProbability = 100f; 
    public float increaseRate = 0.01f;
    public float currentProbability;
    bool cancount = false;
    public bool triggered;
    public AudioClip audioClip;
    public AudioSource aud;
    public NewBehaviourScript newBehaviourScript;
    public static int FixedErrorLightCount;
    public static bool FixedLightGranted;
    // Start is called before the first frame update
    void Start()
    {

        aud = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (NewBehaviourScript.Instance.endwarning == false)
        {
            if (異常 < device.Length)
            {
                if (PlayerPrefs.GetInt("Turtorial") == 2)
                {
                    currentProbability += increaseRate * Time.deltaTime;
                }
                switch(NewBehaviourScript.Instance.warrning)
                {
                    case 0:
                        increaseRate = 1;
                        break;
                        case 1:
                        increaseRate = 1.5f;
                        break;
                        case 2:
                        increaseRate = 2;
                        break;
                        case 3:
                        increaseRate = 2.5f;
                        break;
                        case 4:
                        increaseRate = 3.5f;
                        break;
                }

                if (currentProbability >= NeedProbability)
                {
                    cancount = false;

                    int count = Random.Range(0, device.Length);
                    while (!cancount)
                    {
                        if (device[count].irregular == false)
                        {
                            device[count].irregular = true;
                            device[count].isLightOn = false;
                            padwarnicon[count].SetActive(true);
                            Debug.Log(count);
                            異常++;
                            cancount = true;
                        }
                        else
                        {
                            count = Random.Range(0, device.Length);
                        }
                    }
                    aud.PlayOneShot(audioClip);
                    currentProbability = 0f;
                }

                /*if ((Random.value + 1) * 10 < currentProbability)
                {

                    cancount = false;

                    int count = Random.Range(0, device.Length);
                    while (!cancount)
                    {
                        if (device[count].irregular == false)
                        {
                            device[count].irregular = true;
                            device[count].isLightOn = false;
                            padwarnicon[count].SetActive(true);
                            Debug.Log(count);
                            異常++;
                            cancount = true;
                        }
                        else
                        {
                            count = Random.Range(0, device.Length);
                        }

                    }
                    aud.PlayOneShot(audioClip);
                    currentProbability = 0f;
                }*/



            }
            if (異常 != 0 && !Input.GetMouseButton(0) && triggered != true)
            {
                GameObject.FindGameObjectWithTag("電閘").transform.localEulerAngles = new Vector3(0, 0, -15);
                newBehaviourScript.fuse.SetActive(false);
                newBehaviourScript.fuseput = false;
                triggered = true;
                newBehaviourScript.fixtriggered = false;
            }
            if (解決 == true)
            {

                for (int i = 0; i < device.Length; i++)
                {

                    device[i].isLightOn = true;
                    padwarnicon[i].SetActive(false);

                }
                StartCoroutine("closewarn");
                currentProbability = 0f;
                異常 = 0;
                /*if(coubtriv==device.Length)
                {
                    for(int i=0;i<device.Length;i++)
                    {
                        device[i].ifwarnning=false;
                    }
                   
                }*/
                解決 = false;
            }

        }
    }
       IEnumerator closewarn()
       {
        yield return new WaitForSeconds(0.5f);
         for(int i=0;i<device.Length;i++)
        {   
             
        
          device[i].irregular=false;
        }

       }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            異常 = data.corrouptcount;
            currentProbability = data.corrouptprob;
            for (int i = 0; i < 5; i++)
            {
                device[i].irregular = data.Lightcorrouptdata[i];
            }
        }
    }
    public void savedata(ref Gamedata data)
    {
        for (int i = 0; i < 5; i++)
        {
         data.Lightcorrouptdata[i] = device[i].irregular;
        }
        data.corrouptcount = 異常;
        data.corrouptprob = currentProbability;
    }
}
    

