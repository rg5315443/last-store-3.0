using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class Endingdoor : MonoBehaviour
{
    public GameObject goodend,player;
    public GameObject ScoreBG;
    public FinalScoreAnimation ScoreAnimation;
    private static Endingdoor instance;
    PlayableDirector goodendd;
    public static Endingdoor Instance
    {
        get
        {
            return instance;
        }
    }
    void Awake()
    {
        instance = this;
        goodendd = goodend.GetComponent<PlayableDirector>();
    }
    // Start is called before the first frame update
     void realending() {
        StartCoroutine("End");
        goodend.SetActive(true);
        
    }
    IEnumerator End()
    {
       
        yield return new WaitForSeconds(193);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        ScoreBG.SetActive(true);
        ScoreAnimation.Invoke("PlayFirstTMP", 1f);
        ScoreAnimation.Invoke("PlaySecondTMP", 2.5f);
        ScoreAnimation.Invoke("StartTick", 3.5f);
    }
}
