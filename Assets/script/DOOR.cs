using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DOOR : MonoBehaviour
{
    public Animator ani;
    public bool isopen;
    public bool canuse;
    public AudioClip openSE,closeSE;
    public AudioSource audioSource;
    private static DOOR instance;
    public GameObject PAD;

    public static DOOR Instance
    {
        get
        {
            return instance;
        }
    }
    void Start()
    {
        PAD = GameObject.FindGameObjectWithTag("Pad");
        Physics.IgnoreCollision(PAD.GetComponent<Collider>(), GetComponent<Collider>());
        canuse = true;
        audioSource = ani.GetComponent<AudioSource>();
    }

    public void door()
    {   
        
        if (canuse == true)
        {
            isopen = !isopen;
        }
        
        if(isopen==true&& canuse== true && NewBehaviourScript.Instance.lasttimeopendoor != true)
        {
            StartCoroutine(canuesewaittime());
            ani.Play("OPENDOOR");
            audioSource.PlayOneShot(openSE);

        }
        if (isopen==false&& canuse==true&& NewBehaviourScript.Instance.lasttimeopendoor != true)
        {
            StartCoroutine(canuesewaittime());
            ani.Play("CLOSEDOOR");
            Invoke("closeSEtime",0.9f);
            
        }
        if (isopen == true && canuse == true&&NewBehaviourScript.Instance.lasttimeopendoor==true)
        {
            canuse = false;
            ani.Play("OPENDOOR");
            audioSource.PlayOneShot(openSE);
            NewBehaviourScript.Instance.transform.parent.parent.GetComponent<CharacterController>().enabled = false;
            NewBehaviourScript.Instance.transform.parent.parent.GetComponent<FirstPersonController>().enabled = false;
            foreach (AudioSource pm in GOODENDTRIGGER.instance.preventmute)
            {
                pm.volume = 0;
            }
            Endingdoor.Instance.Invoke("realending", 3f);
        }
    }
    void closeSEtime()
    {
       
        audioSource.PlayOneShot(closeSE);
    }
    IEnumerator canuesewaittime()
    {
        canuse=false;
        yield return new WaitForSeconds(1);
        canuse = true;
    }

    GameObject[] FindGameObjectsWithLayer(int layer)
    {
        GameObject[] goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        List<GameObject> goList = new List<GameObject>();

        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }

        return goList.Count == 0 ? null : goList.ToArray();
    }
}
