using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class 鐵捲門 : MonoBehaviour
{
    public bool isopen,canuse;
    public float OPENcanusetime,CLOSEcanusetime;
    public Animator rollingdoorain;
    public AudioSource AudioSource;
    public AudioClip openbgm,closebgm;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isopen==false)
        {
           
           
        }
        if(isopen==true)
        {
            
            
        }
    }
   public IEnumerator opencount()
    {
        AudioSource.PlayOneShot(openbgm);
        rollingdoorain.SetBool("Open", true);
        canuse =false;
        yield return new WaitForSeconds(OPENcanusetime);
        canuse=true;
    }
    public IEnumerator closecount()
    { 
        AudioSource.PlayOneShot (closebgm);
        rollingdoorain.SetBool("Open", false);
        canuse =false;
        yield return new WaitForSeconds(CLOSEcanusetime);
        canuse=true;
    }
}
