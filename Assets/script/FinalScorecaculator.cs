using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScorecaculator : MonoBehaviour,IDatapersistence
{
    private static FinalScorecaculator instance;
    public int Score;
    public static int CaculatingScore;
    int EasterItemScore,HideScore;
    public int TraitScore;
    public static FinalScorecaculator Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
        TraitSelectScript.TraitPoints = 0;
    }
    void Start()
    {
        Score = 0;
        CaculatingScore = 0;
        Debug.Log("Call Again!");
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Current Dynamic Score : " + CaculatingScore);
        Debug.Log("Current Score : " + Score);
        TraitScore = TraitSelectScript.TraitPoints * 1000;
        EasterItemScore = NewBehaviourScript.Goodendcount * 1000;
        HideScore = NewBehaviourScript.HideCount * -100;
        if (Score >= 0)
        {
            Score = TraitScore + EasterItemScore + HideScore + CaculatingScore;
        }
        else
        {
            Score = 0;
        }
    }

    public void loaddata(Gamedata data)
    {
        if(data != null) 
        {
        CaculatingScore = data.CaculatingScore;
        HideScore = data.HideScore;
        }
    }

    public void savedata(ref Gamedata data)
    {
        data.CaculatingScore = CaculatingScore;
        data.HideScore = HideScore;
    }
}
