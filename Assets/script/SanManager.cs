using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SocialPlatforms;

public class SanManager : MonoBehaviour
{
    public float Sanity, SanityCostRate=1, SanityRecoverRate=1, OrgSanityCostRate;
    public float Lv1valve = 0.8f, Lv2valve = 0.5f,radius,Lv1Decress, Lv2Decress;
    public static bool Lv1,Lv2;
    private bool haventlv1,haventlv2;
    public float DarkTimer;
    public bool HadGranted;
    public static float orgrun, orgwalk;
    public Transform Dectecpos;
    public bool PlayerInLight,PlayerInDark;
    public LayerMask LightMask;
    public AudioSource SanitySound;
    public AnimationCurve curve,curveVisualversion,colorcurves;

    public Volume profile;
    private ColorAdjustments color;
    private Vignette vignette;
    private Fog fog;
    private VolumeParameter<float> saturation = new VolumeParameter<float>();
    private VolumeParameter<float> intensity = new VolumeParameter<float>();
    private VolumeParameter<float> roundness = new VolumeParameter<float>();
    private static Color VisualColor;
    private ColorParameter albedo;
    bool FlashLight_Opened, FlashLight_Triggered;
    public GameObject P6;
    void Start()
    {
        P6.SetActive(false);
        TraitRefresh();
        profile.profile.TryGet<ColorAdjustments>(out color);
        profile.profile.TryGet<Vignette>(out vignette);
        profile.profile.TryGet<Fog>(out fog);
        /* orgrun = FirstPersonController.Runrate;
         orgwalk = FirstPersonController.Walkrate;*/
        InvokeRepeating("LowSanScore",1.5f,3f);
}

    
    void Update()
    {
            PlayerInLight = Physics.CheckSphere(Dectecpos.position, radius, LightMask);

            SanitySound.volume = curve.Evaluate((1-(Sanity / 100f))+ 0.00001f);

            saturation.value = curveVisualversion.Evaluate((100-Sanity)/100f)*-100;
            color.saturation.SetValue(saturation);

            intensity.value = curveVisualversion.Evaluate((100 - Sanity) / 100f) * 0.3f + 0.3f;
            roundness.value = curveVisualversion.Evaluate((100 - Sanity) / 100f) * 0.08f + 0.87f;
            vignette.intensity.SetValue(intensity);
            vignette.roundness.SetValue(roundness);
            
            VisualColor = new Color(colorcurves.Evaluate((100 - Sanity) / 100f) * 0.604f + 0.396f, colorcurves.Evaluate((100 - Sanity) / 100f) * -0.376f + 0.376f, colorcurves.Evaluate((100 - Sanity) / 100f) * -0.376f + 0.376f);
            albedo = new ColorParameter(VisualColor);

            fog.albedo.SetValue(albedo);
          //  Debug.Log(fog.albedo.value);
             


        if (PlayerInLight)
        {
            if (Sanity < 100 && !NewBehaviourScript.Instance.hiding)
            {

                Sanity += Time.deltaTime* SanityRecoverRate;
            }
      
        }
        else
        {
            if (Sanity > 0)
            {
                Sanity -= Time.deltaTime * SanityCostRate;
                DarkTimer += Time.deltaTime;
            }
         
        }
        if (DarkTimer >= 1800 && HadGranted != true) {
            FinalScorecaculator.CaculatingScore += 1000;
            HadGranted = true;
        }
        if (Sanity <= Lv1valve&& !Lv1)
        {
            Lv1 = true;
            Lv1punish(ref FirstPersonController.Runrate, ref FirstPersonController.Walkrate, Lv1);
        }
        else if(Sanity > Lv1valve && Lv1)
        {
            Lv1 = false;
            Lv1punish(ref FirstPersonController.Runrate, ref FirstPersonController.Walkrate, Lv1);
        }

        if (Sanity <= Lv2valve&& !Lv2)
        {
            Lv2 = true;
            Lv2punish(ref FirstPersonController.Runrate, ref FirstPersonController.Walkrate, Lv2);
        }
        else if (Sanity > Lv2valve && Lv2) 
        { 
            Lv2 = false;
            Lv2punish(ref FirstPersonController.Runrate, ref FirstPersonController.Walkrate, Lv2);
        }
       // Debug.Log(FirstPersonController.Runrate);
        //Debug.Log(FirstPersonController.Walkrate);
    }

    public void FlashLightSan(bool FlashLight_Status)
    {
        
        SanityCostRate = FlashLight_Status == true ? SanityCostRate / 2 : OrgSanityCostRate;
    }
    void Lv1punish(ref float run, ref float walk, bool norevert) {
        if (norevert)
        {
            P6.SetActive(true);
            run *= Lv1Decress;
            walk *= Lv1Decress;
        }
        else {
            P6.SetActive(false);
            run = orgrun;
            walk = orgwalk;
        }
    }
    void Lv2punish(ref float run, ref float walk, bool norevert)
    {
        if (norevert)
        {
            P6.SetActive(true);
            run = orgrun;
            walk = orgwalk;
            run *= Lv2Decress;
            walk *= Lv2Decress;
        }
        else if (!norevert && Lv1)
        {
            P6.SetActive(true);
            run = orgrun;
            walk = orgwalk;
            run *= Lv1Decress;
            walk *= Lv1Decress;
        }
        else {
            P6.SetActive(false);
            run = orgrun;
            walk = orgwalk;
        }
    }
    void TraitRefresh()
    {
        if (TraitManager.Instance.RecoverSanityUp)
        {
            SanityRecoverRate += TraitManager.Instance.RecoverSanityUpPower/100;
        }
        if (TraitManager.Instance.RecoverSanityDown)
        {
            SanityRecoverRate -= TraitManager.Instance.RecoverSanityDownPower / 100;
        }
        if (TraitManager.Instance.ReduceSanityDown)
        {
            SanityCostRate -= TraitManager.Instance.ReduceSanityDownPower / 100;
            OrgSanityCostRate = SanityCostRate;
        }
        if (TraitManager.Instance.ReduceSanityUp)
        {
            SanityCostRate += TraitManager.Instance.ReduceSanityUpPower / 100;
            OrgSanityCostRate = SanityCostRate;
        }
        if (TraitManager.Instance.Psychopath)
        {
            Lv1Decress = 1.15f;
            Lv2Decress = 1.4f;
        }

    }
    void LowSanScore() {
        if (Sanity <= 30 && Sanity >= 20 && !IAMDEAD.GotKilled)
        {
            FinalScorecaculator.CaculatingScore += 30;
        }
        else if (Sanity < 20 && Sanity >= 10&&!IAMDEAD.GotKilled) {
            FinalScorecaculator.CaculatingScore += 60;
        }
        else if (Sanity < 10&& !IAMDEAD.GotKilled)
        {
            FinalScorecaculator.CaculatingScore += 90;
        }
    }
}
