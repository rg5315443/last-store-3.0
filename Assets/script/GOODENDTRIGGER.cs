using JetBrains.Annotations;
using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.ProBuilder.MeshOperations;
using UnityEngine.SceneManagement;
public class GOODENDTRIGGER : MonoBehaviour
{
    private static GOODENDTRIGGER Instance;
    public static GOODENDTRIGGER instance
    { 
        get { return Instance; }
    }
    public FirstPersonController characterController;
    public GameObject Director,Endroom;
    public GameObject[] allobj;
    public GameObject[] dontkillobj;
    public GameObject[] forcekillobj;
    public  bool Isend,canexit=false;
    public AudioSource[] AudioSource;
    public AudioSource[] preventmute;
    PlayableDirector PlayableDirector;
    SceneManager SceneManager;
    public float fadeouttime,voulum;
    public float time;
    BoxCollider boxCollider;
    public bool InEndingScean;
    public GameObject ScoreManager,ScoreBG;
    public FinalScoreAnimation ScoreAnimation;
    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;
        AudioSource = FindObjectsOfType<AudioSource>();
        
    }
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
     PlayableDirector = Director.GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Isend == true)
        {
            if (time <= fadeouttime)
            {
                time += Time.deltaTime;
            }
            else
            {
                time = fadeouttime;
            }
            for (int i = 0; i < AudioSource.Length; i++)
            {
                foreach (AudioSource pm in preventmute)
                {
                    AudioSource[i].volume = Mathf.Lerp(AudioSource[i].volume, 0f, (time/ AudioSource.Length) / fadeouttime);
                    pm.volume = 1;
                }
            }
            /*foreach (AudioSource d in AudioSource)
            {



                
                voulum = d.volume;
                
            }*/
            
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player"&& canexit)
        {
            characterController.MoveSpeed = 0;
            characterController.SprintSpeed = 0;
            //Director.SetActive(true);
            Debug.Log("GG");
            if (NewBehaviourScript.Goodendcount >= 8)
            {
                StartCoroutine("GoodEnd");
                boxCollider.enabled = false;
                InEndingScean = true;
            }
            else
            {
                StartCoroutine("BadEnd");
                boxCollider.enabled = false;
                InEndingScean = true;
            }

        }
        
    }
    IEnumerator BadEnd()
    {
        ScoreManager = GameObject.Find("ScoreManager");
        allobj = FindObjectsOfType<GameObject>();
        List<GameObject> killobj = new List<GameObject>(allobj);
        foreach (GameObject a in allobj)
        {
            foreach (GameObject b in dontkillobj)
            {
                if (a.name == b.name)
                    killobj.Remove(a);
                    killobj.Remove(ScoreManager);
                for (int i = 0; i < a.transform.childCount; i++)
                {
                    killobj.Remove(a.transform.GetChild(i).gameObject);
                }
            }
        }

        foreach (GameObject d in forcekillobj)
        {
            killobj.Add(d);
            foreach (GameObject a in killobj)
            {
                Destroy(a);
            }
        }
        Director.SetActive(true);
        yield return new WaitForSeconds(12f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        ScoreBG.SetActive(true);
        ScoreAnimation.Invoke("PlayFirstTMP", 1f);
        ScoreAnimation.Invoke("PlaySecondTMP", 2.5f);
        ScoreAnimation.Invoke("StartTick", 3.5f);
    }
    IEnumerator GoodEnd()
    {
        ScoreManager = GameObject.Find("ScoreManager");
        canexit = false;
        Isend = true;
        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(5.6f, 0f, 23.391f);
        // yield return new WaitForSeconds(15.0f);
        //GetComponent<AudioSource>().enabled = true;
        // yield return new WaitForSeconds(3.0f);
        allobj = FindObjectsOfType<GameObject>();
        List<GameObject> killobj = new List<GameObject>(allobj);
        foreach (GameObject a in allobj)
        {
            foreach (GameObject b in dontkillobj)
            {
                if (a.name == b.name)
                    killobj.Remove(a);
                    killobj.Remove(ScoreManager);
                for (int i = 0; i < a.transform.childCount; i++)
                {
                    killobj.Remove(a.transform.GetChild(i).gameObject);
                }
            }
        }

        foreach (GameObject d in forcekillobj)
        {
            killobj.Add(d);
            foreach (GameObject a in killobj)
            {
                Destroy(a);
            }
        }

        dontkillobj[1].transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        Isend = false;
        Endroom.gameObject.SetActive(true);
        Debug.Log("spawned endroom");
        characterController.MoveSpeed = 2.25f;
        characterController.SprintSpeed = 9;
        //SceneManager.LoadSceneAsync(0);
    }
}
