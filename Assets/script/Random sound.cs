using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WMPLib;

public class Randomsound : MonoBehaviour
{
    public AudioSource[] randomaudios;
    int randomchance;
    // Start is called before the first frame update
    void Start()
    {
        randomaudios = new AudioSource[gameObject.transform.childCount];
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            randomaudios[i] = gameObject.transform.GetChild(i).GetComponent<AudioSource>();
        }
        InvokeRepeating("randomdepend", 12f, 12f);
    }

    // Update is called once per frame
    void Update()
    {
        if (randomchance >= 95) {
            randomchance = 0;
            int randommusic,randomstereo;
            randommusic = Random.Range(0, randomaudios.Length);
            randomstereo = Random.Range(1, 100);
            if(randomstereo>50)randomaudios[randommusic].panStereo = 1;
            else randomaudios[randommusic].panStereo = -1;
            randomaudios[randommusic].Play();
        }
    }
    void randomdepend() {
     randomchance = Random.Range(1, 101);
    }
}
