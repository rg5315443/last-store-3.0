using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemyspawn : MonoBehaviour, IDatapersistence
{
    public int spawnhour, spawnminute , misuichour, misuicminute;
    public GameObject P2,P3,P4,P5;
    public Transform player;
    public LayerMask obs;
    public bool P2spawntriggered, P3spawntriggered, P4spawntriggered, P5spawntriggered ,P2cansee, P3cansee, P4cansee,
        P5cansee, Lv1misuicplayed, Lv2misuicplayed,AfterDay;
    Vector3 P2dir, P3dir, P4dir, P5dir;
    RaycastHit hit;
    AudioSource audioSource;
    public AudioClip Lv1,Lv2;
    private static Enemyspawn instance;
    bool stage2 = false;
    public GameObject P3ap;
    public static Enemyspawn Instance
    {
        get
        {
            return instance;
        }
    }
    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (!P2spawntriggered) 
        {   
            P2.SetActive(false);
        }
        if(!P3spawntriggered) 
        {
            P3.SetActive(false);
            P3ap.SetActive(false);
        }
        if (!P4spawntriggered)
        {
            P4.SetActive(false);
        }
        if (!P5spawntriggered) 
        {
            P5.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (time.Instance.hour >= misuichour && time.Instance.min >= misuicminute && !Lv1misuicplayed)
        {
            Lv1misuicplayed = true;

            audioSource.PlayOneShot(Lv1);
        }

        if (!P2spawntriggered)
        {
            P2dir = P2.transform.position - player.position;
            float P2distance = Vector3.Distance(P2.transform.position, player.position);
            if (!Physics.Raycast(player.position, P2dir, out hit, P2distance, obs))
            {
                P2cansee = true;
            }else P2cansee = false;
        }

        if (!P3spawntriggered)
        {
            P3dir = P3.transform.position - player.position;
            float P3distance = Vector3.Distance(P3.transform.position, player.position);
            if (!Physics.Raycast(player.position, P3dir, out hit, P3distance, obs))
            {
                P3cansee = true;
            } else P3cansee = false;
        }

        if (!P4spawntriggered)
        {
  
                P4cansee = false;
            
        }

        if (!P5spawntriggered)
        {
            P5dir = P5.transform.position - player.position;
            float P5distance = Vector3.Distance(P5.transform.position, player.position);
            if (!Physics.Raycast(player.position, P5dir, out hit, P5distance, obs))
            {
                P5cansee = true;
            }else P5cansee = false;
        }


        if (NewBehaviourScript.Instance.warrning >= 3)
        {
            P5.SetActive(true);
        }
        else {
            P5.SetActive(false);
        }
        if (time.Instance.hour >= spawnhour && time.Instance.min >= spawnminute && !P2spawntriggered && !P2cansee && Lv1misuicplayed && Lv2misuicplayed)
        {
            P2.SetActive(true);

            P2spawntriggered = true;
        }
        if (time.Instance.hour >= spawnhour && time.Instance.min >= spawnminute && !P3spawntriggered && !P3cansee && Lv1misuicplayed && Lv2misuicplayed)
        {
            P3.SetActive(true);
            P3ap.SetActive(true);
            P3spawntriggered = true;
        }
        if (time.Instance.hour >= spawnhour && time.Instance.min >= spawnminute && !P4spawntriggered && !P4cansee && Lv1misuicplayed && Lv2misuicplayed)
        {
            P4.SetActive(true);
            P4spawntriggered = true;
        }
        if (time.Instance.hour >= spawnhour && time.Instance.min >= spawnminute && time.Instance.day > 0 && Lv1misuicplayed && !Lv2misuicplayed)
        {
            
            audioSource.PlayOneShot(Lv2);
            Lv2misuicplayed = true;
        }

        /*if (time.Instance.hour >= spawnhour && time.Instance.min >= spawnminute && !P5spawntriggered && !P5cansee)
        {
            P5.SetActive(true);
            P5spawntriggered = true;
        }*/
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            Lv1misuicplayed = data.lv1music;
            Lv2misuicplayed = data.lv2music;
            stage2 = data.stage2;
            P2.SetActive(data.p2spawn);
            P3.SetActive(data.p3spawn);
            P4.SetActive(data.p4spawn);
            P5.SetActive(data.p5spawn);
        }
    }
    public void savedata(ref Gamedata data)
    {
        data.lv1music = Lv1misuicplayed;
        data.lv2music = Lv2misuicplayed;
        data.stage2 = this.stage2;
        data.p2spawn = P2.activeSelf;
        data.p3spawn = P3.activeSelf;
        data.p4spawn = P4.activeSelf;
        data.p5spawn = P5.activeSelf;
    }
}
