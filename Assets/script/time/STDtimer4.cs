using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class STDtimer4 : MonoBehaviour, IDatapersistence
{
    public time time;
    public Timer4 timer4;
    [SerializeField]
    private Sprite[] digits;
    [SerializeField]
    private Image[] chr;
    public int summin, sumhour,orgsumhour;
    int stdlastmin, stdlastmin2, stdlasthour, stdlasthour2;
    public void updatestdtime() {
        summin = timer4.min * 6 + time.min; //59 + 59 min 
        sumhour = timer4.hour * 6 + time.hour;
        orgsumhour = sumhour;
        if (summin >= 60)
        {
            sumhour = summin / 60 + orgsumhour;
            summin = summin % 60;
        }
        if (sumhour >= 24)
        {
            sumhour = sumhour % 24;
        }
    }
    public void Update()
    {
        try
        {
            stdlastmin = timer4.sec % 10;
            stdlastmin2 = timer4.sec / 10;
            stdlasthour = timer4.min % 10;
            stdlasthour2 = timer4.min / 10;

            chr[3].sprite = digits[stdlastmin];
            chr[2].sprite = digits[stdlastmin2];
            chr[1].sprite = digits[stdlasthour];
            chr[0].sprite = digits[stdlasthour2];
        }
        catch {
            chr[3].sprite = digits[0];
            chr[2].sprite = digits[0];
            chr[1].sprite = digits[0];
            chr[0].sprite = digits[0];
        }
    }
    public void loaddata(Gamedata data) {
        if (data != null)
        {
            this.summin = data.Cardsummin4;
            this.sumhour = data.Cardsumhour4;
        }
    }
    public void savedata(ref Gamedata data) {
        data.Cardsummin4 = this.summin;
        data.Cardsumhour4 = this.sumhour;
    }
}
