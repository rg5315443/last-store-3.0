using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer3 : MonoBehaviour, IDatapersistence //調整時間僅限min 跟 hour, sec不要調整 不然玩家看不到
{
    public NewBehaviourScript main;
    public int sec,min,hour,tsec,orgtsec,orghour,orgmin,orgsec;
    public void timerstart()
    {
        if (min == 0 && sec == 0 && main.warning3 != true)
        {
            hour = orghour;
            min = orgmin;
            sec = orgsec;
            tsec = orgtsec;
        }
        if (sec == 60)
        {
            min = 1;
            sec = 0;
        }
        else if (sec > 60)
        {
            min = sec / 60 + orgmin;
            sec -= (min - orgmin) * 60;
        }
        if (min == 60)
        {
            hour = 1;
            min = 0;
        }
        else if (min > 60)
        {
            hour = min / 60 + orghour;
            min -= (hour - orghour) * 60;
        }
        InvokeRepeating("timer", 1, 1);

    }
    public void cancelinvoke() {
        CancelInvoke("timerstart");
        CancelInvoke("timer");
        main.cardtick3 = false;
    }
   public void timer() {
        tsec = hour * 60 * 60 + min * 60 + sec;
        if (hour != 0 && tsec == orgtsec)
        {
            sec = 60;
            sec--;
        }
        else
        {
            sec--;
        }
        tsec--;
        if (tsec > 0)
        {

            if (min <= 0 && hour > 0)
            {
                hour--;
                min = 59;
            }
            else if (sec < 0 && min > 0)
            {
                min--;
                sec = 59;
            }
            else if (sec < 0 && min == 0 && hour == 0)
            {
             sec = 0;
            }
        }
        else
        {
         CancelInvoke("timer");
         main.warning3 = true;
         if (main.warning3 == true) //do waht u want here
         {
                GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).gameObject.SetActive(true);
                GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).GetComponent<Animator>().enabled = true;
                GameObject.FindGameObjectWithTag("warning3").transform.GetChild(0).GetComponent<AudioSource>().Play();
         }
        }
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            this.orgsec = data.timer3orgsec;
            this.orgmin = data.timer3orgmin;
            this.orghour = data.timer3orghour;
            this.sec = data.timer3sec;
            this.min = data.timer3min;
            this.hour = data.timer3hour;
            this.main.warning3 = data.warning3;
        }
    }
    public void savedata(ref Gamedata data)
    {
        data.timer3orgsec = this.orgsec;
        data.timer3orgmin = this.orgmin;
        data.timer3orghour = this.orghour;
        data.timer3sec = this.sec;
        data.timer3min = this.min;
        data.timer3hour = this.hour;
        data.warning3 = this.main.warning3;
    }
}
