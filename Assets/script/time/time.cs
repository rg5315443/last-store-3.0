using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class time : MonoBehaviour,IDatapersistence
{
    public NewBehaviourScript main;
    [SerializeField]
    private Sprite[] digits;
    [SerializeField]
    private Image[] chr;
    public int hour ,sec, min;
    public int day;
    public GameObject endcam;
    public  bool EndGame;
    private static time instance;
    public GameObject Exitgate_text;
    bool Tuched;
    public static time Instance
    {
        get {
            return instance;
        }
    }
    public TMP_Text text;
    private void Awake()
    {
        instance = this;
    }
    public void Start()
    {
        EndGame = false;
        InvokeRepeating("stdtimer", 1, 1f);
    }
    public void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.N))
            {
            hour = 4;
            min = 00;
        }
        
        if (hour >= 6 && min >= 0) {
            EndGame = true;
            CancelInvoke("stdtimer");
            if(!Tuched)
            {
                Exitgate_text.SetActive(true);
                StartCoroutine("Invoke");
                InvokeRepeating("TextFun", 1, 1f);
                Tuched = true;
            }

            //endcam.SetActive(true);
        }
        int stdlastmin = min % 10;
        int stdlastmin2 = min / 10;
        int stdlasthour = hour % 10;
        int stdlasthour2 = hour / 10;
        chr[3].sprite = digits[stdlastmin];
        chr[2].sprite = digits[stdlastmin2];
        chr[1].sprite = digits[stdlasthour];
        chr[0].sprite = digits[stdlasthour2];
        text.text = stdlasthour2.ToString() + stdlasthour.ToString() + ":"+stdlastmin2.ToString() + stdlastmin.ToString();
    }
    void TextFun()
    {
        if (Exitgate_text.activeSelf)
        {
            Exitgate_text.SetActive(false);
        }else Exitgate_text.SetActive(true);
    }
    IEnumerator Invoke()
    {
        yield return new WaitForSeconds(5);
        CancelInvoke("TextFun");
    }

    public void stdtimer()
    {
        sec = sec + 6;
        if (sec > 59 && min < 60)
        {
        min++;
        sec = 0;
        }
        if (min > 59)
        {
         min = 0;
         hour++;

        }
        if (hour > 23)
        {
            hour = 0;
            day++;
        }
    }
    public void loaddata(Gamedata data)
    {
        if (data != null)
        {
            min = data.Gamemin;
            hour = data.Gamehour;
            day = data.Gameday;
        }
    }
    public void savedata(ref Gamedata data)
    {
        data.Gamemin = this.min;
        data.Gamehour = this.hour;
        data.Gameday = day;
    }

}
