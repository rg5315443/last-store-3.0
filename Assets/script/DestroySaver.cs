using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroySaver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Move",1f,3f);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void Move()
    {
        Scene scene;
        scene = SceneManager.GetActiveScene();
        if (scene.buildIndex == 1)
        {
            SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
            CancelInvoke("Move");
        }
        Debug.Log("Invoking");
    }
}
