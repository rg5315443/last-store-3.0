using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.SocialPlatforms.Impl;

public class IAMDEAD : MonoBehaviour
{
    //public bool dead;
    // Start is called before the first frame update
   
    public GameObject deadscean,camroot;
    public GameObject[] dontkill;
    [SerializeField]
    List<GameObject> dontkilllist;
    [SerializeField]
    List<GameObject> killlist;
    GameObject childgam;
    public GameObject Player,counttime,P2,P3,P4,P5,P6;
    public float animationtime;
    public GameObject ScoreBG;
    public FinalScoreAnimation ScoreAnimation;
    public static bool GotKilled;
    CapsuleCollider cDapsule;
    public AudioSource[] AudioSource;
    public AudioSource[] ChildAudioSource;
    public AudioSource[] preventmute;
    NewBehaviourScript newBehaviour;
    float time;
    public float fadeouttime;
    public CharacterController characterController;
    public BoxCollider boxCollider;
    void Start()
    {
        deadscean.SetActive(false);
        Cursor.visible=false;
        Cursor.lockState=CursorLockMode.Locked;
        GameObject[] allobj = FindObjectsOfType<GameObject>();
        killlist = new List<GameObject>(allobj);
        dontkilllist = new List<GameObject>(dontkill);
         //cDapsule = Player.GetComponent<CapsuleCollider>();
         newBehaviour = FindObjectOfType<NewBehaviourScript>();
         AudioSource = FindObjectsOfType<AudioSource>();
        fadeouttime = 2f;
        GotKilled = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (!camroot.activeSelf)
        {
            if (time <= fadeouttime)
            {
                time += Time.deltaTime;
            }
            else
            {
                time = fadeouttime;
            }

            for (int i = 0; i < AudioSource.Length; i++)
            {
                AudioSource[i].volume = Mathf.Lerp(AudioSource[i].volume, 0f, time / fadeouttime);
                AudioSource[] childAudioSources = AudioSource[i].GetComponentsInChildren<AudioSource>(true);

                foreach (AudioSource childAudio in childAudioSources)
                {
                    if (!preventmute.Contains(childAudio))
                    {
                        childAudio.volume = Mathf.Lerp(childAudio.volume, 0f, time / fadeouttime);
                    }
                }
            }
            foreach (AudioSource pm in preventmute)
            {
                pm.volume = 1;
            }
        }
    }
    public void getover(float dead)
    {
        characterController.enabled = false;
        boxCollider.enabled = false;
        MENU.BeKilled = true;
        // cDapsule.enabled=false;
        P2.SetActive(false);
        P3.SetActive(false);
        P4.SetActive(false);
        P5.SetActive(false);
        P6.SetActive(false);
        // newBehaviour.enabled=false;
        counttime.SetActive(false);
        GotKilled = true;


        StartCoroutine(overcount(dead));
      
    }
    public IEnumerator overcount(float dead)
    {
        if (FinalScorecaculator.Instance == null)
        {
            Debug.LogError("FinalScorecaculator.Instance is null");
        }
        if (deadscean == null)
        {
            Debug.LogError("deadscean is null");
        }
        if (camroot == null)
        {
            Debug.LogError("camroot is null");
        }
        if (ScoreBG == null)
        {
            Debug.LogError("ScoreBG is null");
        }
        if (ScoreAnimation == null)
        {
            Debug.LogError("ScoreAnimation is null");
        }
        FinalScorecaculator.CaculatingScore -= FinalScorecaculator.Instance.TraitScore;
        yield return new WaitForSeconds(dead-1);
        deadscean.SetActive(true);
        yield return new WaitForSeconds(animationtime);
        camroot.SetActive(false);
        Cursor.visible=true;
        Cursor.lockState=CursorLockMode.None;
        //deadcrag.SetActive(true);
        ScoreBG.SetActive(true);
        ScoreAnimation.Invoke("PlayFirstTMP", 1f);
        ScoreAnimation.Invoke("PlaySecondTMP", 2.5f);
        ScoreAnimation.Invoke("StartTick", 3.5f);
    }
    
}
