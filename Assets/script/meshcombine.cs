using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshcombine : MonoBehaviour
{
    [SerializeField] private List<MeshFilter> sourcemesh;
    [SerializeField] private MeshFilter targetmesh;
    // Start is called before the first frame update
    [ContextMenu("combine")]
    private void combinemeshes()
    {
        var combine = new CombineInstance[sourcemesh.Count];
        for(var i = 0; i<sourcemesh.Count;i++)
        {
            combine[1].mesh=sourcemesh[i].sharedMesh;
            combine[1].transform=sourcemesh[i].transform.localToWorldMatrix;
        }

        var mesh = new Mesh();
        mesh.CombineMeshes(combine);
        targetmesh.mesh=mesh;
    }
}
