using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CHESSINGMUSIC : MonoBehaviour
{
    AudioSource _audio;
    public 追擊 P2AI;
    public p3ai p3Ai;
    bool isplaying;
    // Start is called before the first frame update
    void Start()
    {
        _audio=GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if((P2AI.chessing==true||p3Ai.ischessing==true) && isplaying==false)
        {
            
            isplaying=true;
            _audio.volume=0.3f;
            _audio.Play();
        }
        else if(P2AI.chessing==false&&p3Ai.ischessing == false && isplaying==true)
        {
            isplaying=false;
            StartCoroutine("fadeout");
            
        }
    }
    IEnumerator fadeout()
    {
        float timeToFade=0.25f;
        float timeElapsed=0f;
        while(timeElapsed<timeToFade)
        {
            _audio.volume=Mathf.Lerp(0.3f,0,timeElapsed/timeToFade);
            timeElapsed+=Time.deltaTime;
            yield return null;
        }
        _audio.Stop();
    }
}
