using System.Collections;
using Dan.Main;
using Dan.Models;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dan.Demo
{
    public class LeaderboardShowcase : MonoBehaviour
    {
        [Header("Leaderboard Essentials:")]
        [SerializeField] private TMP_InputField _playerUsernameInput;
        [SerializeField] private string UsernamePlusRandomTag;

        public void Submit()
        {
            int RandomTag;
            RandomTag = Random.Range(0, 100000);
            UsernamePlusRandomTag = RandomTag.ToString();
            Leaderboards.NewTest.UploadNewEntry(_playerUsernameInput.text + "#" + UsernamePlusRandomTag, FinalScorecaculator.Instance.Score, null, ErrorCallback);
            SceneManager.LoadSceneAsync(0);
        } 
        
        private void ErrorCallback(string error)
        {
            Debug.LogError(error);
        }
    }
}
