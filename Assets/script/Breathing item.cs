using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breathingitem : MonoBehaviour
{
    public float Emissiveintensity = 0;
    public float t = 0, maxt = 0.1f, tcache;
    public AnimationCurve AnimationCurve;
    Color EmissiveColor;
    public bool count;

    // Start is called before the first frame update
    void Start()
    {
        Emissiveintensity = -5;
        tcache = -5;
        count = true;
        EmissiveColor = new Color(Random.Range(1, 256)/255f, Random.Range(1, 256) / 255f, Random.Range(1, 256) / 255f);
    }

    // Update is called once per frame
    private void Update()
    {
        //Debug.Log(EmissiveColor);
        t += Time.deltaTime * 0.1f;
        // Debug.Log(Emissiveintensity);
        if (t >= maxt)
        {
            t = 0;
            count = !count;
        }

        /*if (Emissiveintensity == -2)
        {
            count = true;
        }
        else if (Emissiveintensity == 2)
        {
            count = false;
        }*/

        if (count)
        {
            Emissiveintensity = Mathf.Lerp(Emissiveintensity, 5, AnimationCurve.Evaluate(t));
            gameObject.GetComponent<Renderer>().material.SetColor("_EmissiveColor", EmissiveColor * Emissiveintensity);
        }
        else
        {
            Emissiveintensity = Mathf.Lerp(Emissiveintensity, -5, AnimationCurve.Evaluate(t));
            gameObject.GetComponent<Renderer>().material.SetColor("_EmissiveColor", EmissiveColor * Emissiveintensity);
        }
    }
}
