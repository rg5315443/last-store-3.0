using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLight : MonoBehaviour
{
    public GameObject G_L,G_D,R_L,R_D;
    public p3ai p3ai;
    public 電力異常 elec;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (NewBehaviourScript.Instance.endwarning == false)
        {

            if (NewBehaviourScript.Instance.warrning > 0|| elec.異常>0)
            {
                R_D.SetActive(true);
                G_L.SetActive(false);
                G_D.SetActive(false);
                R_L.SetActive(true);
            }
            else if (NewBehaviourScript.Instance.warrning <= 0|| elec.異常 <= 0)
            {
                R_D.SetActive(true);
                G_L.SetActive(true);
                G_D.SetActive(false);
                R_L.SetActive(false);
            }
        }
        else 
        {
            if (NewBehaviourScript.Instance.EndRepaired == false)
            {
                R_D.SetActive(true);
                G_L.SetActive(false);
                G_D.SetActive(false);
                R_L.SetActive(true);
            }
            else
            {
                GOODENDTRIGGER.instance.canexit = true;
                R_D.SetActive(false);
                G_L.SetActive(false);
                G_D.SetActive(true);
                R_L.SetActive(true);
            }

        }
    }
}
