using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class TraitLoreScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TextMeshProUGUI TraitLore;
    // Start is called before the first frame update
public void OnPointerEnter(PointerEventData eventData)
    {
        TraitLore.gameObject.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        TraitLore.gameObject.SetActive(false);
        
    }
    public void OnDisable()
    {
        TraitLore.gameObject.SetActive(false);
    }
}

