using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveSafeHouse : MonoBehaviour
{
    public GameObject[] Watcher;
    public GameObject Plane;
    // Start is called before the first frame update
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Leave");
            for (int i = 0; i < Watcher.Length; i++)
            {
                Plane.SetActive(false);
                NewBehaviourScript.Instance.WatcherCount = 0;
                Watcher[i].SetActive(false);
            }
        }
    }
}
