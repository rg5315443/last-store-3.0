using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Unity.Mathematics;
using UnityEngine;
[System.Serializable]
public class Gamedata
{
    public int Cardsummin, Cardsumhour, Cardsummin2, Cardsumhour2, Cardsummin3, Cardsumhour3, Cardsummin4, Cardsumhour4, Cardsummin5, Cardsumhour5, Gamemin, Gamehour, Gameday, timer1sec, timer1min, timer1hour, timer2sec, timer2min, timer2hour, timer3sec, timer3min, timer3hour, timer4sec, timer4min, timer4hour, timer5sec, timer5min, timer5hour;
    public int timer1orgsec, timer1orgmin, timer1orghour, timer2orgsec, timer2orgmin, timer2orghour, timer3orgsec, timer3orgmin, timer3orghour, timer4orgsec, timer4orgmin, timer4orghour, timer5orgsec, timer5orgmin, timer5orghour;
    public float stamina, currentleftele, currentrightele, corrouptcount, corrouptprob;
    public bool warning1, warning2, warning3, warning4, warning5, corrouptstatus;
    public bool[] Lightcorrouptdata = new bool[5];
    public Vector3 playerposition, P2postition, P3postition;
    public quaternion playerrotation, P2rotation, P3rotation;
    public bool gamestarted, turtorial;
    public Vector3 item1pos, item2pos, item3pos, item4pos, item5pos, item6pos, item7pos, item8pos;
    public quaternion item1rot, item2rot, item3rot, item4rot, item5rot, item6rot, item7rot, item8rot;
    public bool obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, lightcorroupt;
    public bool objplane1, objplane2, objplane3, objplane4, objplane5, objplane6, objplane7, objplane8;
    public bool pick1 = true, pick2 = true, pick3 = true, pick4 = true, pick5 = true, pick6 = true, pick7 = true, pick8 = true, fuseactive = true, fusecheckfun = true;
    public GameObject Lefthandobj, Righthandobj;
    public Transform Righthandobjparent, Lefthandobjparent;
    public Vector3 Lefthandobjpos, Righthandobjpos;
    public quaternion Lefthandobjrot, Righthandobjrot;
    public bool Lefthandbool, Righthandbool;
    public bool checkhandright, checkhandleft;
    public bool lv1music, lv2music, stage2, p2spawn, p3spawn, p4spawn, p5spawn;
    public float P5effect;

    public bool Endtriggercanexit, EndtriggerIsend;
    public int goodcount;

    public bool generatedfuse, generatedbatteries,generatedP3kids;
    public bool P3kidscontroller, insurancecontrol, batteriescontrol;
    public bool[] insurancesavtive, batteriesactive;
    public Vector3[] activeinsurancespos, activebatteriespos, P3kidspos, OnGroundItemPos;
    public quaternion[] activeinsurancesros, activebatteriesros, P3kidsrot, OnGroundItemRot;
    public GameObject[] P3childobjs,P3kidsparents;
    public GameObject[] OnGroundItems;
    public bool[] P3KidsAlreadyHaveItem;
    public Vector3 padpos,flashlightpos;
    public Quaternion padrot,flashlightrot;
    public bool onearmtrait,goodearstrait,goodeyestrait,badearstrait,badeyestrait;
    public GameObject[] stashobjs;
    public Vector3[] stashobjpos;
    public quaternion[] stashobjrot;
    public GameObject[] stashobjsparent;
    public GameObject[] hidedropobjs, hidedropobjs2, hidedropobjs3, hidedropobjs4;
    public Vector3[] hidedroponjspos, hidedroponjspos2, hidedroponjspos3, hidedroponjspos4;
    public quaternion[] hidedroprot, hidedroprot2, hidedroprot3, hidedroprot4;
    public Transform[] hidedropparent, hidedropparent2, hidedropparent3, hidedropparent4;

    public bool ScareAlone, lame, Nicevision, OCD, Adrenaline, Frugal, RunSpeedUp, RunSpeedDown, WalkSpeedUp, WalkSpeedDown, WorkSpeedUp, WorkSpeedDown, AttractDown, AttractUp, StaminaCostDown, StaminaCostUp, BatteryCostDown, BatteryCostUp, Retreat, RecoverSanityUp, RecoverSanityDown, ReduceSanityDown, ReduceSanityUp, TravelLight, Psychopath;

    public int HideScore,EasterItemScore, CaculatingScore;
    public Gamedata() {
    /*
    Lefthandobj = null;
    for (int i = 0; i< 5; i++) {
    Lightcorrouptdata[i] = false;
    }
    gamestarted = false;//�}�|�Q��
    turtorial = true;
    Gamemin = 50;
    Gamehour = 3;
    stamina = 15;
    warning1 = false;
    warning2 = false;
    warning3 = false;
    warning4 = false;
    warning5 = false;
    timer1orgsec = 0;
    timer1orgmin = 2;
    timer1orghour = 0;
    timer2orgsec = 0;
    timer2orgmin = 4;
    timer2orghour = 0;
    timer3orgsec = 0;
    timer3orgmin = 6;
    timer3orghour = 0;
    timer4orgsec = 0;
    timer4orgmin = 6;
    timer4orghour = 0;
    timer5orgsec = 0;
    timer5orgmin = 4;
    timer5orghour = 0;
    Cardsummin = 12;
    Cardsummin2 = 24;
    Cardsummin3 = 36;
    Cardsummin4 = 36;
    Cardsummin5 = 24;
    Cardsumhour = 0;
    Cardsumhour2 = 0;
    Cardsumhour3 = 0;
    Cardsumhour4 = 0;
    Cardsumhour5 = 1;
    timer1sec = 0;
    timer1min = 2;
    timer1hour = 0;
    timer2sec = 0;
    timer2min = 4;
    timer2hour = 0;
    timer3sec = 0;
    timer3min = 6;
    timer3hour = 0;
    timer4sec = 0;
    timer4min = 6;
    timer4hour = 0;
    timer5sec = 0;
    timer5min = 4;
    timer5hour = 0;
    objplane1 = true;
    objplane2 = true;
    objplane3 = true;
    objplane4 = true;
    objplane5 = true;
    objplane6 = true;
    objplane7 = true;
    objplane8 = true;
    obj1 = false;
    obj2 = false;
    obj3 = false;
    obj4 = false;
    obj5 = false;
    obj6 = false;
    obj7 = false;
    obj8 = false;
    P3kidscontroller = false;
    item1pos = new Vector3(0f, 0f, 0f);
    item1rot = new quaternion(0f, 0f, 0f, 0f);
    item2pos = new Vector3(0f, 0f, 0f);
    item2rot = new quaternion(0f, 0f, 0f, 0f);
    item3pos = new Vector3(0f, 0f, 0f);
    item3rot = new quaternion(0f, 0f, 0f, 0f);
    item4pos = new Vector3(0f, 0f, 0f);
    item4rot = new quaternion(0f, 0f, 0f, 0f);
    item5pos = new Vector3(0f, 0f, 0f);
    item5rot = new quaternion(0f, 0f, 0f, 0f);
    item6pos = new Vector3(0f, 0f, 0f);
    item6rot = new quaternion(0f, 0f, 0f, 0f);
    item7pos = new Vector3(0f, 0f, 0f);
    item7rot = new quaternion(0f, 0f, 0f, 0f);
    item8pos = new Vector3(0f, 0f, 0f);
    item8rot = new quaternion(0f, 0f, 0f, 0f);
    playerposition = new Vector3(9.31000042f, 0.0699999928f, 1.88f);
    playerrotation = new quaternion(0f, 0f, 0f,0f);
    P2postition = new Vector3(29.2743721f, 1.15999997f, -38.8164063f);
    P2rotation = new quaternion(0f, 0f, 0f, 0f);
    P3postition = new Vector3(0.0799999982f, 0.970000029f, 79.9199982f);
    P3rotation = new quaternion(0f, 0f, 0f, 0f);
    */
    }
}
