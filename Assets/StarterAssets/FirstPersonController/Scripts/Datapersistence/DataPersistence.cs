using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class DataPersistence : MonoBehaviour
{
    [Header("file stroage config")]
    [SerializeField] private string filename;
    private FileDataHandler dataHandler;
    private Gamedata gamedata;
    private List<IDatapersistence> datapersistenceobjects;
    public static DataPersistence instance { get; private set; }
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Found other persistence manager in the scean");
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        this.dataHandler = new FileDataHandler(Application.persistentDataPath, filename);

    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += Onsceanloaded;
    }
    /*
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= Onsceanloaded;
        SceneManager.sceneUnloaded -= Onsceanunloaded;
    }*/
    public void Onsceanloaded(Scene scene, LoadSceneMode mode)
    {
        this.datapersistenceobjects = FindAllDataPersistenceObjects();
        load();
    }
    /*
    public void Onsceanunloaded(Scene scene)
    {
       save();
    }*/
    public void newgame()
    {
        //this.gamedata = new Gamedata();
        dataHandler.newgame();
    }
    public void load()
    {
        this.datapersistenceobjects = FindAllDataPersistenceObjects();
        this.gamedata = dataHandler.load();
        /* if (this.gamedata == null)
         {
             Debug.Log("Can't find any save file. Init new data to default");

             newgame();
         }*/
        foreach (IDatapersistence datapersistenceobj in datapersistenceobjects)
        {
            datapersistenceobj.loaddata(gamedata);
        }
    }
    public void save()
    {
        this.datapersistenceobjects = FindAllDataPersistenceObjects();
        this.gamedata = new Gamedata();
        foreach (IDatapersistence datapersistenceobj in datapersistenceobjects)
        {
            datapersistenceobj.savedata(ref gamedata);
        }
        dataHandler.save(gamedata);
    }
    private List<IDatapersistence> FindAllDataPersistenceObjects()
    {
        IEnumerable<IDatapersistence> datapersistenceobjects = FindObjectsOfType<MonoBehaviour>()
            .OfType<IDatapersistence>();
        return new List<IDatapersistence>(datapersistenceobjects);
    }

    //just for test
    private void clicksave()
    {
        save();
    }

}
